DROP TABLE IF EXISTS A_Barra_Specific_Risk_HS300_before_regime;
CREATE TABLE `A_Barra_Specific_Risk_HS300_before_regime` (
	`Trading_Day` DATETIME NOT NULL,
	`SecuCode` VARCHAR(10) NOT NULL,
	`Specific_Risk` DECIMAL(20,6) NULL DEFAULT NULL,
	`Create_Time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`Trading_Day`,`SecuCode`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
