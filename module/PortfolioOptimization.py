# -*- coding: utf-8 -*-
"""
@author: Huang, Jingyang
@license: Copyright Reserved by Futu
@contact: jgyghuang@qq.com
@site: http://www.futu5.com
@software: PyCharm
@file: PortfolioOptimization.py
@time: 2018/12/17
"""


import pandas as pd
import numpy as np
from futuquant import *
import datetime
from sqlalchemy import create_engine, text
import json
import pymysql
from RiskModel import RiskModel

class PortOpt():
    # 当前只支持截面计算，且未将数据存入数据库
    def __init__(self, end_date, start_date = None, stock_list = [], cs = True, weight_last = pd.DataFrame([])):
        self.end_date = end_date #string
        self.start_date = start_date #string
        self.cs = cs
        self.stock_list = stock_list
        self.weight_last = weight_last # weight_last为上一期股票权重，dataframe格式，index为secucode
        self.sequence_factors = ["Country", "Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                 "Midcapitalization", "Residualvolatility", "Earningsyield",
                                 "ConstructionMaterials","Machinery","CommunicationEquipment","Financials",
                                 "RealEstate","IndustrialConglomerates", "MedicalHealth","NonferrousMetals",
                                 "Chemicals","HouseholdAppliances","FoodBeverage","Electronics",
                                 "Transportation","LightManufacturing","Utilities","Textiles","Agriculture",
                                 "CommercialTrade","MineralIndustry","FerrousMetals","CateringTourism",
                                 "Steel", "Leisure", "ConstructionFurnishings","ElectricalEquipment",
                                 "Defense","Automobiles","Communication","Media","NonBankFinancials",
                                 "TransportationEquipment","CommunicationServices"] # 1国家+10个风格+32个行业


    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str
    def get_data_return2(self, num_days_need):
        # num_days_need = 500
        num_days_need = int(num_days_need)
        if self.cs:
            start_date_temp_datetime = self.convert_string_to_datetime(self.end_date) - pd.DateOffset(days=2 * num_days_need)
            start_date_temp = self.convert_datetime_to_string(start_date_temp_datetime)

            trading_days = self.get_trading_days(start_date_temp, self.end_date)
            start_date = self.convert_datetime_to_string(trading_days[-num_days_need])  # 取500天前的交易日日期string
            end_date = self.end_date
            engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
            engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                                db="db_etf", charset="utf8mb4")
            Barra_Factor_Return_stmt = text(
                "select * from Barra_Factor_Return where Trading_Day >= Date('{start_date}') and Trading_Day <= Date('{end_date}')"
                " order by Trading_Day;".format(end_date=end_date, start_date=start_date))
            Barra_Exposure_stmt = text(
                "select * from Barra_Exposure where Trading_Day = Date('{end_date}')"
                " order by Trading_Day, SecuCode;".format(end_date=end_date))
            engine = create_engine(engine_str)
            Barra_Factor_Return = pd.read_sql(Barra_Factor_Return_stmt, engine)
            Barra_Factor_Return.drop(["Create_Time"], axis=1, inplace=True)
            Barra_Factor_Return = Barra_Factor_Return[["Trading_Day"] + self.sequence_factors]
            Barra_Factor_Return.iloc[:, 1:] = Barra_Factor_Return.iloc[:,1:].rolling(window=num_days_need).mean()
            Barra_Factor_Return_predict = Barra_Factor_Return.iloc[-1, :]

            Barra_Exposure = pd.read_sql(Barra_Exposure_stmt, engine)
            Barra_Exposure.drop(["Create_Time"], axis=1, inplace=True)
            Barra_Exposure = Barra_Exposure[["Trading_Day", "SecuCode", "StockReturn", "MV"] + self.sequence_factors]

        else:
            start_date_temp_datetime = self.convert_string_to_datetime(self.start_date) - pd.DateOffset(days=2 * num_days_need)
            start_date_temp = self.convert_datetime_to_string(start_date_temp_datetime)

            trading_days = self.get_trading_days(start_date_temp, self.start_date)
            start_date_factor_return = self.convert_datetime_to_string(trading_days[-num_days_need])  # 取500天前的交易日日期string
            end_date = self.end_date
            start_date = self.start_date
            engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
            engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                                db="db_etf", charset="utf8mb4")
            Barra_Factor_Return_stmt = text(
            "select * from Barra_Factor_Return where Trading_Day >= Date('{start_date_factor_return}')"
            " and Trading_Day <= Date('{end_date}') order by Trading_Day;".format(
             end_date = end_date, start_date_factor_return = start_date_factor_return))
            Barra_Exposure_stmt = text(
                "select * from Barra_Exposure where Trading_Day >= Date('{start_date}') and Trading_Day <= Date('{end_date}')"
                " order by Trading_Day, SecuCode;".format(end_date=end_date, start_date=start_date))
            engine = create_engine(engine_str)
            Barra_Factor_Return = pd.read_sql(Barra_Factor_Return_stmt, engine)
            Barra_Factor_Return.drop(["Create_Time"], axis=1, inplace=True)
            Barra_Factor_Return = Barra_Factor_Return[["Trading_Day"] + self.sequence_factors]
            Barra_Factor_Return.iloc[:, 1:] = Barra_Factor_Return.iloc[:, 1:].rolling(window=num_days_need).mean()
            Barra_Factor_Return_predict = Barra_Factor_Return[Barra_Factor_Return_predict["Trading_Day"]>=
                                                                      self.convert_string_to_datetime(self.start_date)]
            Barra_Factor_Return_predict.index = range(len(Barra_Factor_Return_predict))

            Barra_Exposure = pd.read_sql(Barra_Exposure_stmt, engine)
            Barra_Exposure.drop(["Create_Time"], axis=1, inplace=True)
            Barra_Exposure = Barra_Exposure[["Trading_Day", "SecuCode", "StockReturn", "MV"] + self.sequence_factors]
        if Barra_Factor_Return.empty or Barra_Factor_Return_predict.empty or Barra_Exposure.empty:
            # 待补充
            raise TypeError("Risk dataframe is empty.")
        return Barra_Exposure, Barra_Factor_Return_predict
    def get_data_risk2(self):
        if self.cs:
            engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
            engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6",
                                                port="13357",
                                                db="db_etf", charset="utf8mb4")
            end_date = self.end_date
            Barra_Covariance_stmt = text("select * from Barra_Covariance where Trading_Day = Date('{end_date}')".format(end_date=end_date))
            engine = create_engine(engine_str)
            Barra_Covariance = pd.read_sql(Barra_Covariance_stmt, engine)
            Barra_Covariance.drop(["Create_Time"], axis=1, inplace=True)
            Barra_Covariance = Barra_Covariance[["Trading_Day", "Factors"] + self.sequence_factors]
            Barra_Covariance.index = Barra_Covariance["Factors"]
            Barra_Covariance = Barra_Covariance.loc[self.sequence_factors]
            Barra_Covariance.index = range(len(Barra_Covariance))

            Barra_Specific_Risk_stmt = text(
                "select * from Barra_Specific_Risk where Trading_Day = Date('{end_date}')"
                " order by Trading_Day, SecuCode;".format(end_date=end_date))
            engine = create_engine(engine_str)
            Barra_Specific_Risk = pd.read_sql(Barra_Specific_Risk_stmt, engine)
            Barra_Specific_Risk.drop(["Create_Time"], axis=1, inplace=True)
            Barra_Specific_Risk.sort_values(by=["Trading_Day", "SecuCode"], inplace=True)
            Barra_Specific_Risk.index = range(len(Barra_Specific_Risk))

        else:
            engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
            engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6",
                                                port="13357",
                                                db="db_etf", charset="utf8mb4")
            start_date = self.start_date
            end_date = self.end_date
            Barra_Covariance_stmt = text(
                "select * from Barra_Covariance where Trading_Day >= Date('{start_date}') and Trading_Day <= Date('{end_date}')"
                " order by Trading_Day;".format(end_date=end_date, start_date=start_date))
            engine = create_engine(engine_str)
            Barra_Covariance = pd.read_sql(Barra_Covariance_stmt, engine)
            Barra_Covariance.drop(["Create_Time"], axis=1, inplace=True)
            Barra_Covariance = Barra_Covariance[["Trading_Day", "Factors"] + self.sequence_factors]
            Barra_Covariance.index = Barra_Covariance["Factors"]
            Barra_Covariance = Barra_Covariance.loc[self.sequence_factors].sort_values(by=["Trading_Day"])
            Barra_Covariance.index = range(len(Barra_Covariance))

            Barra_Specific_Risk_stmt = text(
                "select * from Barra_Specific_Risk where Trading_Day >= Date('{start_date}') and Trading_Day <= Date('{end_date}')"
                " order by Trading_Day, SecuCode;".format(end_date=end_date, start_date=start_date))
            engine = create_engine(engine_str)
            Barra_Specific_Risk = pd.read_sql(Barra_Specific_Risk_stmt, engine)
            Barra_Specific_Risk.drop(["Create_Time"], axis=1, inplace=True)
            Barra_Specific_Risk.sort_values(by=["Trading_Day", "SecuCode"], inplace=True)
            Barra_Specific_Risk.index = range(len(Barra_Covariance))
        if Barra_Covariance.empty or Barra_Specific_Risk.empty:
            # 待补充
            raise TypeError("Risk dataframe is empty.")
        return Barra_Covariance, Barra_Specific_Risk
    def get_data_benchmark(self, year):
        # HS权重
        BenchmarkWeight = pd.read_csv("D:\\rqalpha\\myfile_rqalpha\\MFM\\MultiFactorModel\\data\\data_HS\\" + str(year) + '.csv')
        return BenchmarkWeight
    def output(self):
        num_days_need = 500
        # num_days_need = 200
        if self.cs:
            year = self.end_date[:4]
            params_dict = self.params()
            BenchmarkWeight = self.get_data_benchmark(year)
            Barra_Exposure, Barra_Factor_Return_predict = self.get_data_return2(num_days_need)
            Barra_Covariance, Barra_Specific_Risk = self.get_data_risk2()
            df_weight = self.MeanVarOpt_cs(Barra_Factor_Return_predict, Barra_Exposure, Barra_Covariance, Barra_Specific_Risk, BenchmarkWeight, params_dict)

            col = self.convert_string_to_datetime(self.end_date)
            result = pd.DataFrame()
            stock_list = ["HK."+str(item).zfill(5) for item in df_weight.index[df_weight[col] > 0].tolist()]
            stock_name = self.get_stock_name("HK", stock_list)
            stock_weight = df_weight[df_weight[col] > 0].values
            result['stock_code'] = stock_list
            result['stock_name'] = stock_name
            result['weight'] = stock_weight
            result['select_time'] = self.convert_datetime_to_string(self.convert_string_to_datetime(self.end_date) + pd.DateOffset(days = 1))
        else:
            # 待补充
            raise TypeError("Portfolio Optimization by time series is not supported now.")
            pass
        # result.to_csv("./riskaver.csv", encoding='gb18030')
        return result

    def MeanVarOpt_cs(self, FactorR, exposure, covariance, specificrisk, BenchmarkWeight, params):
        '''
        f: factors return vector      K*1
        X: stocks exposure matrix     N*K
        cov: factor return covariance K*K
        Delta: specific risk          N*N
        StockRisk = XcovX' + Delta           N*N
        StockReturn = X*f                    N*1
        X_industry: exposure of industries - dummy           N*28
        X_industry_benchmark: exposure of industries in the benchmark - dummy   300*28
        W_benchmark: weights of stocks in the benchmark                  300*1
        X_size: exposure of size     N*1

        object: max R'W - W'(Lambda*V)W - C'|W - W0|
        constraints:
        权重之和为1         sum(W) = 1
        行业中性            W_industry = W_benchmarkindustry
        换手率约束          体现在成本函数系数中
        个股权重上下限约束   Wmin <= W <= Wmax
        风险因子暴露约束    |X_size'W| <= X_size_max
        个股数量约束        1'(W > 0) <= Nmax
        '''
        # 参数设定
        Lambda = params["Lambda"]
        coef_cost = params["coef_cost"]
        coef_rho = params["coef_rho"]
        coef_style = params["coef_style"]
        Wmax = params["Wmax"]
        Wmin = params["Wmin"]
        Nmax = params["Nmax"]
        tol = params["tol"]
        flag_enhancedindex = params["flag_enhancedindex"]
        flag_inindex = params["flag_inindex"]
        import cvxpy as cvx
        factorlist = covariance.columns.tolist()[2:]
        stylelist = factorlist[:17] # 17 style factors
        industrylist = factorlist[17: -1] # 11 industries
        # delta: 个股特异风险
        stocklist_orig = specificrisk["SecuCode"].tolist()
        # 指数成分股
        stocklist_index = BenchmarkWeight["code"].tolist()
        BenchmarkWeight.index = stocklist_index
        stocklist_inindex = sorted(list(set(stocklist_orig).intersection(set(stocklist_index))))
        print('intersection num', len(stocklist_inindex))
        if self.stock_list: # 如果有初始样本池
            # 设定初始样本池为最新一期港股通成分股，即AH_list
            stocklist_orig = list(set(self.stock_list).intersection(set(stocklist_orig)))
            print("stocklist_orig",len(stocklist_orig))
        if flag_enhancedindex or flag_inindex:  # 指数增强或指数成分股内选股
            stocklist = stocklist_inindex
        else:
            stocklist = stocklist_orig
        stocknum = len(stocklist)

        # X_industry_benchmark: 基准行业暴露度
        industry_benchmark = pd.get_dummies(BenchmarkWeight.industry)
        # industrylist_all = ['房地产','公用事业','金融','能源','通信服务','工业','信息技术','原材料','医疗保健','非日常生活消费品','日常消费品']
        industrylist_all = ['RealEstate', 'Utilities', 'Finance', 'Energy', 'Telecom', 'Industry',
                            'InformationTechnology', 'Materials', 'MedicalHealth', 'ConsumerDiscretionary', 'ConsumerStaples']
        industry_left = list(set(industrylist_all) - set(industry_benchmark.columns))
        if not industry_left:
            pass
        else:
            industry_benchmark = pd.concat([industry_benchmark, pd.DataFrame(0, columns=industry_left, index=industry_benchmark.index)],axis=1)

        # industry_benchmark.rename(columns={'房地产': 'RealEstate', '公用事业': 'Utilities', '金融': 'Finance', '能源': 'Energy', '通信服务': 'Telecom', '工业': 'Industry','信息技术': 'InformationTechnology', '原材料': 'Materials', '医疗保健': 'MedicalHealth', '非日常生活消费品': 'ConsumerDiscretionary', '日常消费品': 'ConsumerStaples'}, inplace=True)


        industry_benchmark = industry_benchmark[industrylist]
        industry_benchmark_sum = industry_benchmark.sum(axis = 0)
        industry_left_benchmark = industry_benchmark_sum.index[industry_benchmark_sum.values == 0].tolist()

        X_industry_benchmark = np.asmatrix(industry_benchmark)
        # W_benchmark：基准权重
        W_benchmark = np.asmatrix(BenchmarkWeight.weight / BenchmarkWeight.weight.sum()).T
        # X: 个股因子暴露度
        exposure.index = exposure["SecuCode"]
        X = exposure.loc[stocklist, factorlist]
        X_industry = X[industrylist]
        X_industry_sum = X_industry.sum(axis = 0)
        industry_left_port = X_industry_sum.index[X_industry_sum.values == 0].tolist()
        X_industry = np.asmatrix(X_industry)
        # X_style:17个风格因子暴露度
        X_style = np.asmatrix(X[stylelist]).T
        X = np.asmatrix(X)
        # f: 因子收益
        f = np.asmatrix(FactorR.values[1:]).T
        # delta: 个股特异风险
        specificrisk.index = specificrisk["SecuCode"]
        delta = np.diag(specificrisk.loc[stocklist, 'Specific_Risk']) / (10 ** 3)
        # cov: 因子收益协方差矩阵
        cov = np.asmatrix(covariance[factorlist]) / (10 ** 6)

        if self.weight_last.empty: # W0: 上一期个股权重
            W0 = pd.DataFrame(np.repeat(0.0, stocknum), index = stocklist)
        else:
            W0 = self.weight_last
            W0 = W0.loc[stocklist]
            W0 = W0.fillna(value=0.0)

        # 组合优化 - 第一次优化
        if flag_enhancedindex:  # 指数增强
            W_index = np.asmatrix(BenchmarkWeight.loc[stocklist_inindex, "weight"]/ 100).T
        else:  # 非指数增强
            W_index = 0
        W = cvx.Variable(stocknum)
        Wb = cvx.maximum(W - W0.values.reshape(stocknum), 0)
        Ws = cvx.maximum(W0.values.reshape(stocknum) - W, 0)
        #cost = coef_rho * np.repeat(coef_cost, stocknum).T * (Wb + Ws)
        ret = (X * f).T * (W - W_index)
        portfolio_exposure = X.T * (W - W_index)
        risk = cvx.quad_form(portfolio_exposure, cov) + cvx.quad_form((W - W_index), delta)
        #risk = cvx.quad_form(W, X.dot(cov).dot(X.T) + delta)

        # 组合一：无风格暴露约束、无行业中性约束
        # constraints = [cvx.sum(W) == 1.0, Wmax >= W, W >= Wmin]
        # 组合二：有风格暴露约束、无行业中性约束
        # constraints = [cvx.sum(W) == 1.0, Wmax >= W, W >= Wmin,
        #                coef_style[[0,1,2,4,8,10,12,14]] >= cvx.abs(X_style * (W - W_index))[[0,1,2,4,8,10,12,14]]]
        #  组合三：风格暴露约束、无行业中性约束
        constraints = [cvx.sum(W) == 1.0, Wmax >= W, W >= Wmin,
                       coef_style[[0,1,2,4,8,10,12,14]] >= cvx.abs(X_style * (W - W_index))[[0,1,2,4,8,10,12,14]]]
        #, X_industry.T*W == np.array(X_industry_benchmark.T*W_benchmark).flatten() 仅限制size因子暴露:coef_style[2] >= cvx.abs(X_style * (W - W_index))[2]，仅限制前20个行业保持行业中性:(X_industry.T*W)[:20] == (X_industry_benchmark.T*W_benchmark)[:20]
        '''
        if (not industry_left_port) or (set(industry_left_port).intersection(set(industry_left_benchmark)) == set(industry_left_port)):
            print('a')
            print('industryleft_benchmark:', industry_left_benchmark)
            print('industryleft_port:', industry_left_port)
            pass
        else:
            print('b')
            print('industryleft_benchmark:', industry_left_benchmark)
            print('industryleft_port:', industry_left_port)
            constraints = constraints[:-1]
        '''
        #obj = cvx.Maximize(ret - Lambda * risk - cost) # 考虑成本
        obj = cvx.Maximize(ret - Lambda * risk) # 不考虑成本
        prob = cvx.Problem(obj, constraints)
        prob.solve(solver=cvx.ECOS_BB)

        # 第一次优化结果
        weight_W = pd.DataFrame(W.value, columns=['weight_temp'])
        weight_W['weight_index'] = W_index
        weight_W['weight_temp_active'] = weight_W['weight_temp'] - weight_W['weight_index']
        weight_W['stocks'] = stocklist
        weight_n1 = weight_W[weight_W['weight_temp'] >= tol]
        n1 = len(weight_n1)
        if n1 <= Nmax:  # 如果已经满足股票数量约束
            weight_date = pd.DataFrame(weight_W['weight_temp'].values, index=stocklist, columns=[self.convert_string_to_datetime(self.end_date)])
            weight_date[weight_date < tol] = 0
            # 增加n1只股票的权重，至权重值和为1
            leftweight = 1.0 - float(weight_date.sum())
            weight_date[weight_date != 0] += leftweight / n1

        else:  # 如果n1超出股票数量约束
            # n2为强制保留的股票
            n2 = int(Nmax * 0.5)
            weight_n1 = weight_n1.sort_values(by='weight_temp', ascending=False)
            weight_keep = weight_n1.iloc[:n2, :]
            weight_keep_index = weight_n1.index[:n2]
            # 剩余n1-n2只股票中选出(Nmax-n2)只股票
            weight_left_index = weight_n1.index[n2:]
            weight_left_n1minusn2 = weight_n1.iloc[n2:, :] # length = n1-n2=stocknum2
            leftweight = 1.0 - float(weight_keep['weight_temp'].sum())
            X2 = X[weight_left_index, :]
            X_style2 = X_style[:, weight_left_index]
            # X_industry2 = X_industry[weight_left_index, :]
            delta2 = np.diag(delta[weight_left_index, weight_left_index])
            W02 = W0.iloc[weight_left_index, :]
            # 已有style暴露度
            removestocks_index = list(set(weight_W.index) - set(weight_n1.index))
            keepstocks_coef_style = X_style[:, weight_keep_index] * np.asmatrix(weight_keep['weight_temp_active']).T + \
                                    X_style[:, removestocks_index] * np.asmatrix(weight_W.loc[removestocks_index, 'weight_temp_active']).T
            # 剩余行业权重
            # leftstocks_industry = X_industry_benchmark.T*W_benchmark - X_industry[weight_keep_index, :].T*np.asmatrix(weight_keep['weight_temp']).T

            # 第二次优化
            if flag_enhancedindex:  # 指数增强
                W_index2 = np.asmatrix(weight_n1.weight_index[weight_left_index]).T
            else:  # 非指数增强
                W_index2 = 0
            stocknum2 = n1 - n2
            W2 = cvx.Variable(stocknum2)
            Wb2 = cvx.maximum(W2 - W02.values.reshape(stocknum2), 0)
            Ws2 = cvx.maximum(W02.values.reshape(stocknum2) - W2, 0)
            eta = cvx.Variable(stocknum2, boolean=True)
            #cost2 = coef_rho * np.repeat(coef_cost, stocknum2).T * (Wb2 + Ws2)
            ret2 = (X2 * f).T * (W2 - W_index2)
            portfolio_exposure2 = X2.T * (W2 - W_index2)
            risk2 = cvx.quad_form(portfolio_exposure2, cov) + cvx.quad_form((W2 - W_index2), delta2)

            # 组合一：无风格暴露约束、无行业中性约束
            # constraints = [cvx.sum(W2) == leftweight, cvx.sum(eta) <= Nmax - n2, W2 <= eta, W2 >= Wmin, W2 <= Wmax]
            # 组合二：有风格暴露约束、无行业中性约束
            # constraints = [cvx.sum(W2) == leftweight, cvx.sum(eta) <= Nmax - n2,
            #                W2 <= eta, W2 >= Wmin, W2 <= Wmax,
            #                cvx.abs(X_style2*(W2-W_index2) + np.array(keepstocks_coef_style).flatten())[[0,1,2,4,8,10,12,14]]
            #                <= coef_style[[0,1,2,4,8,10,12,14]]]
            # 组合三：有风格暴露约束、无行业中性约束
            constraints = [cvx.sum(W2) == leftweight, cvx.sum(eta) <= Nmax - n2, W2 <= eta, W2 >= Wmin, W2 <= Wmax,
                           cvx.abs(X_style2*(W2-W_index2) + np.array(keepstocks_coef_style).flatten())[[0,1,2,4,8,10,12,14]]
                           <= coef_style[[0,1,2,4,8,10,12,14]]]
            # W2 + weight_left_n1minusn2['weight_temp'].values.T >= Wmin, W2 + weight_left_n1minusn2['weight_temp'].values.T <= Wmax,, X_industry2.T*W2 <= leftstocks_industry
            #obj2 = cvx.Maximize(ret2 - Lambda * risk2 - cost2) # 考虑成本
            obj2 = cvx.Maximize(ret2 - Lambda * risk2) # 不考虑成本
            prob2 = cvx.Problem(obj2, constraints)
            prob2.solve(solver=cvx.ECOS_BB, mi_max_iters=100)
            weight_left = pd.DataFrame(W2.value, index=W02.index.tolist())
            weight_left[weight_left < tol] = 0

            weight_date = pd.DataFrame(index=stocklist, columns=[self.convert_string_to_datetime(self.end_date)])
            weight_date.loc[weight_keep['stocks']] = np.array([weight_keep['weight_temp'].values]).T
            weight_date.loc[weight_left.index] = np.array(weight_left.values)
            weight_date = weight_date.fillna(value=0)
        return weight_date

    def params(self):
        params_dict = {}
        params_dict["Lambda"] = 1.0 # 风险厌恶系数
        params_dict["coef_cost"] = 3.0 / 10000 # 交易成本
        params_dict["coef_rho"] = 0.5 # 交易成本惩罚项
        # 组合一：17个风格因子暴露度约束，0为没有约束
        # params_dict["coef_style"] = np.array([0.0]*17)
        # 组合二：17个风格因子暴露度约束
        # params_dict["coef_style"] = np.array([0.5]*3 + [0.0] + [0.5] + [0.0]*3 + [0.5] +
        #                                       [0.0] + [0.5] + [0.0] + [0.5] + [0.0] + [0.5] + [0.0]*2)
        # 组合三：17个风格因子暴露度约束
        params_dict["coef_style"] = np.array([0.15]*3 + [0.0] + [0.15] + [0.0]*3 + [0.15] +
                                             [0.0] + [0.15] + [0.0] + [0.15] + [0.0] + [0.15] + [0.0]*2)
        params_dict["Wmax"] = 0.03 # 个股权重上限
        params_dict["Wmin"] = 0.0 # 个股权重下限
        params_dict["Nmax"] = 50 # 最大标的股票个数
        params_dict["tol"] = 10 ** (-3)
        params_dict["flag_enhancedindex"] = 0 # 是否指数增强
        params_dict["flag_inindex"] = 0 # 是否基准成分股内选股
        return params_dict
    def get_trading_days(self, start, end):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fsi_check", pwd="fsi_check@", host="172.28.249.24", port="13358",
                                            db="hk_finance", charset="utf8mb4")
        sql_string = """select TradingDay from QT_OSIndexQuote where IndexCode='1001098' and
                            TradingDay >= DATE('{start}') and TradingDay <= DATE('{end}') 
                            order by TradingDay;""".format(start=start, end=end)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_list = sorted(trading_day_df["TradingDay"].tolist())
        return trading_day_list
    def get_stock_name(self, market = "HK", stock_list=None):
        """
        得到股票代码
        :param market: 市场HK,A,US
        :param stock_list: 股票列表
        :return: stockname_list
        """
        try:
            quote_ctx = OpenQuoteContext(host='172.24.31.139', port=11114)
            if stock_list:
                if market.upper() == 'HK':
                    stockinfo_df = quote_ctx.get_stock_basicinfo(Market.HK, SecurityType.STOCK, stock_list)[1]
                if market.upper() == 'US':
                    stockinfo_df = quote_ctx.get_stock_basicinfo(Market.US, SecurityType.STOCK, stock_list)[1]
                else:
                    stockinfo_df = quote_ctx.get_stock_basicinfo(Market.SH, SecurityType.STOCK, stock_list)[1]
                stock_name_list = stockinfo_df.name.tolist()
            else:
                stock_name_list = []
        except Exception as err:
            print(str(err))
        finally:
            quote_ctx.close()
        return stock_name_list


if __name__ == "__main__":
    # 最新一期沪港通名单，作为初始样本池
    AH_list = pd.read_csv("D:\\rqalpha\\myfile_rqalpha\\MFM\\MultiFactorModel\\data\\data_AH\\20181001.csv", usecols = [2])
    stock_list = [int(item.split(".")[0]) for item in AH_list['wind_code'].tolist()]
    # 输入的end_date需为前一个交易日，如2019/01/30有量价数据之后（晚上五点左右），结合2019/01/29的因子数据，可以计算至2019/01/29的相关数据，
    # 则2019/01/30晚五点之后，可以输出end_date = “2019-01-29”的组合优化结果。
    # 理论上应该在2019/01/30晚上输出新的仓位，以2019/01/31开盘价等价格成交
    end_date = "2019-03-28"
    portopt = PortOpt(end_date = end_date, stock_list = stock_list, cs = True)
    df_weight, stockpool_df = portopt.output()
    print(stockpool_df)