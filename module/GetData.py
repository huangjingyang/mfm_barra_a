# -*- coding: utf-8 -*-
"""
@author: Huang, Jingyang
@license: Copyright Reserved by Futu
@contact: jgyghuang@qq.com
@site: http://www.futu5.com
@software: PyCharm
@file: GetData.py
@time: 2019/04/03
"""
import pandas as pd
import numpy as np
from futualpha import *
import warnings
from WindPy import *
import datetime

class GetData():
    def __init__(self, trading_day, sample):
        self.trading_day = trading_day # string
        self.sample = sample

    def cal_stock_return_nextmonth(self, trading_day, secucode_list):
        """
        计算个股下个月月度收益率
        """
        crosssection = CrossSection()
        factor_type_kline = CNFactorsType.QFQ_Kline
        df_data_kline_start = crosssection.get_data_cross_section(secucode_list, factor_type_kline, ["ClosePrice"],
                                                                  trading_day)
        df_data_kline_start.rename(columns={"ClosePrice": "ClosePrice_start"}, inplace=True)
        end_datetime_temp = self.convert_string_to_datetime(trading_day) + pd.DateOffset(months=2)
        end_datetime_temp = end_datetime_temp.replace(day=1)
        end_datetime = end_datetime_temp - pd.DateOffset(days=1)
        end = self.get_trading_day(start=None, end=self.convert_datetime_to_string(end_datetime))[-1]
        end = self.convert_datetime_to_string(end)
        df_data_kline_end = crosssection.get_data_cross_section(secucode_list, factor_type_kline, ["ClosePrice"], end)
        df_data_kline_end.rename(columns={"ClosePrice": "ClosePrice_end"}, inplace=True)
        df_data_kline = pd.merge(df_data_kline_start, df_data_kline_end, on=["InnerCode", "SecuCode"], how="left")
        df_data_kline["StockReturn"] = df_data_kline["ClosePrice_end"] / df_data_kline["ClosePrice_start"] - 1
        if df_data_kline["StockReturn"].dropna(how="all").empty:
            return pd.DataFrame()
        df_data_return = df_data_kline.drop(["ClosePrice_start", "ClosePrice_end"], axis=1)
        df_data_return.index = range(len(df_data_return))
        return df_data_return

    def cal_stock_return_nextday(self, secucode_list):
        """
        计算个股下个交易日收益率
        """
        start = self.trading_day
        start_datetime = self.convert_string_to_datetime(start)
        end_datetime = self.get_trading_day(start, end = None)[1]
        end = self.convert_datetime_to_string(end_datetime)
        ts = TimeSeries()
        factor_type_kline = CNFactorsType.QFQ_Kline
        df_data_kline = ts.get_data_time_series(secucode_list, factor_type_kline, ["ClosePrice"], start, end)
        df_data_kline_pivot = df_data_kline.pivot(index="TradingDay", columns="SecuCode", values="ClosePrice")
        df_data_kline_pivot.fillna(method="pad", inplace=True)
        df_data_kline_pivot_return = df_data_kline_pivot.pct_change().T
        df_data_kline_pivot_return = df_data_kline_pivot_return.assign(SecuCode = df_data_kline_pivot_return.index)
        df_data_kline_pivot_return.rename(columns = {end_datetime: "StockReturn"}, inplace = True)
        df_data_kline_pivot_return.index = range(len(df_data_kline_pivot_return))
        df_data_return = pd.merge(df_data_kline[df_data_kline["TradingDay"]==start_datetime], df_data_kline_pivot_return, on=["SecuCode"], how="left")
        df_data_return = df_data_return[["InnerCode", "SecuCode", "StockReturn"]]
        df_data_return["StockReturn"] = df_data_return["StockReturn"].fillna(0)
        df_data_return.index = range(len(df_data_return))
        return df_data_return

    def data_qa_factors(self, factor_list_common, factor_list_profitability, factor_list_debtpayingability):
        """
        从futu_factors_2.0获取因子数据，通常另secucode_list = []取全部数据，再从wind取当天非ST、上市满一年、可交易股票池
        """
        crosssection = CrossSection()
        timeseries = TimeSeries()
        stock_list = []
        # Volumn_Price_Common因子
        factor_type_common = CNFactorsType.Volumn_Price_Common
        df_data_common = crosssection.get_data_cross_section(stock_list, factor_type_common, factor_list_common, self.trading_day)
        if df_data_common.empty: # 当天不是交易日，或futualpha中无数据
            warnings.warn("Futualpha info is empty.", UserWarning)
            return pd.DataFrame([])
        df_data_common = df_data_common[['InnerCode', 'SecuCode'] + factor_list_common]
        # Profitability因子
        factor_type_debtpayingability = CNFactorsType.Debtpayingability
        df_data_debtpayingability = timeseries.get_data_time_series(stock_list, factor_type_debtpayingability,
                                      factor_list_debtpayingability, self.trading_day, self.trading_day)
        df_data_debtpayingability = df_data_debtpayingability[['InnerCode', 'SecuCode']+factor_list_debtpayingability]
        # Debtpayingability因子
        factor_type_profitability = CNFactorsType.Profitability
        df_data_profitability = timeseries.get_data_time_series(stock_list, factor_type_profitability,
                                    factor_list_profitability, self.trading_day, self.trading_day)
        df_data_profitability = df_data_profitability[['InnerCode', 'SecuCode'] + factor_list_profitability]
        # 个股未来一个月收益率
        # df_data_return = self.cal_stock_return_nextmonth(self.trading_day, stock_list)
        # 个股未来一天收益率
        df_data_return = self.cal_stock_return_nextday(stock_list)
        result = self.dataframe_merge([df_data_common, df_data_debtpayingability, df_data_profitability, df_data_return], ["InnerCode", "SecuCode"], "outer")
        result.rename(columns = {"MV_total": "MV", "SW_Industry_name": "Industry"}, inplace = True)
        result.insert(loc=0, column='Trading_Day', value=self.convert_string_to_datetime(self.trading_day))
        return result

    def get_predict_data(self):
        """
        从EXCEL导入分析师预测数据
        """
        path = "D:\\rqalpha\\myfile_rqalpha\\BarraProject\\MFM_Barra_A\\data\\predictdata\\"
        filename_datetime_list = []
        for year in range(2010, 2019):
            for month in [1, 7]:
                filename_datetime_list.append(pd.Timestamp(year, month, 1))
        filename_datetime_list = np.array(filename_datetime_list)
        trading_day_datetime = self.convert_string_to_datetime(self.trading_day)
        loc1 = np.where(trading_day_datetime >= filename_datetime_list)
        if not loc1:
            return pd.DataFrame()
        else:
            date = filename_datetime_list[loc1[0][-1]]
            filename = str(date.year).zfill(4) + str(date.month).zfill(2) + "01"
            file = pd.read_csv(path + filename + ".csv")
            file = file.assign(EPFWD = file["PNI_12M"]/file["MV"])
            file.rename(columns = {"PNI_12M_growth": "EGRSF", "PNI_36M_growth": "EGRLF", "code": "SecuCode"}, inplace = True)
            file["SecuCode"] = file["SecuCode"].apply(lambda x: str(x).zfill(6))
            file = file.drop(["MV", "PNI_24M_growth", "PNI_12M"], axis = 1)
            file.drop_duplicates(subset = "SecuCode", keep = "first", inplace = True)
        return file




    def data_sample_stocks_list(self):
        """
        从Wind获取trading_day当天的剔除ST、上市满一年、交易状态为交易的全部A股
        或者HS300/ZZ500成分股，仅剔除当日无法交易股票
        本函数之后更改为获取self.trading_day之前最近月末交易日的股票清单，并数据储存在GetData_data文件夹中
        """
        trading_day = self.trading_day
        trading_day_datetime = self.convert_string_to_datetime(trading_day)
        trading_day_monthend_datetime = self.get_trading_day_last_monthend(trading_day)
        trading_day_monthend = self.convert_datetime_to_string(trading_day_monthend_datetime)
        path = "D:\\rqalpha\\myfile_rqalpha\\BarraProject\\MFM_Barra_A\\data\\GetData_data\\"
        if self.sample == "A":
            if trading_day_monthend_datetime == trading_day_datetime:  # 月末交易日，从wind取数
                trading_day_format_wind = str(trading_day_datetime.year).zfill(4) + str(
                        trading_day_datetime.month).zfill(2) + str(trading_day_datetime.day).zfill(2)
                w.start()
                sectorid = "a001010f00000000"
                wind_data_not_st = w.wset("sectorconstituent", "date = " + trading_day + "; sectorid = " + sectorid)
                # 剔除ST之后的全部A股
                wind_data_not_st_df = pd.DataFrame(wind_data_not_st.Data, index = wind_data_not_st.Fields)
                # 储存
                wind_data_not_st_df.to_csv(path + "A_notST_" + trading_day + ".csv", encoding='gb18030')
                # 无法交易的股票
                secucode_not_st_temp = wind_data_not_st_df.loc["wind_code"].tolist()
                secucode_not_st = [item[:6] for item in secucode_not_st_temp]
                secucode_not_st_temp_string = ""
                for item in secucode_not_st_temp:
                    secucode_not_st_temp_string = secucode_not_st_temp_string + item + ","
                secucode_not_st_temp_string = secucode_not_st_temp_string[:-1]
                wind_data_trade_status = w.wss(secucode_not_st_temp_string, "trade_status",
                                               "tradeDate = " + trading_day_format_wind)
                wind_data_trade_status_df = pd.DataFrame(wind_data_trade_status.Data,
                                                         index=wind_data_trade_status.Fields,
                                                         columns=wind_data_trade_status.Codes)
                # 储存
                wind_data_trade_status_df.to_csv(path + "A_tradingstatus_" + trading_day + ".csv", encoding='gb18030')
                w.close()
            else:
                # 从文件夹中获取剔除ST之后的全部A股
                wind_data_not_st_df = pd.read_csv(path + "A_notST_" + trading_day_monthend + ".csv", encoding='gb18030')
                wind_data_not_st_df.set_index("Unnamed: 0", inplace=True)
                secucode_not_st_temp = wind_data_not_st_df.loc["wind_code"].tolist()
                secucode_not_st = [item[:6] for item in secucode_not_st_temp]
                # 从文件夹中获取无法交易的股票
                wind_data_trade_status_df = pd.read_csv(path + "A_tradingstatus_" + trading_day_monthend + ".csv", encoding='gb18030')
                wind_data_trade_status_df.set_index("Unnamed: 0", inplace=True)
            secucode_not_trade_temp = wind_data_trade_status_df.columns[wind_data_trade_status_df.loc["TRADE_STATUS"] != "交易"].tolist()
            secucode_not_trade = [item[:6] for item in secucode_not_trade_temp]
            # 上市日期不满一年的股票
            ListedDate = self.data_listed_date()
            secucode_ipo_less = ListedDate["SecuCode"][
                                    ListedDate["ListedDate"] > trading_day_datetime - pd.DateOffset(years=1)].tolist()
            # 剔除ST、无法交易、上市日期不满一年之后的全部A股
            secucode_list = list(set(secucode_not_st) - set(secucode_not_trade) - set(secucode_ipo_less))

        else:
            if self.sample == "HS300":
                sectorid = "1000000090000000"
            elif self.sample == "ZZ500":
                sectorid = "1000008491000000"
            if trading_day_monthend_datetime == trading_day_datetime:  # 月末交易日，从wind取数
                trading_day_format_wind = str(trading_day_datetime.year).zfill(4) + str(
                    trading_day_datetime.month).zfill(2) + str(trading_day_datetime.day).zfill(2)
                w.start()
                # 全部指数成分股
                wind_data_constituent = w.wset("sectorconstituent", "date = " + trading_day + "; sectorid = " + sectorid)
                wind_data_constituent_df = pd.DataFrame(wind_data_constituent.Data, index = wind_data_constituent.Fields)
                # 储存
                wind_data_constituent_df.to_csv(path + self.sample + "_notST_" + trading_day + ".csv", encoding='gb18030')
                # 无法交易的股票
                secucode_constituent_temp = wind_data_constituent_df.loc["wind_code"].tolist()
                secucode_constituent = [item[:6] for item in secucode_constituent_temp]
                secucode_constituent_temp_string = ""
                for item in secucode_constituent_temp:
                    secucode_constituent_temp_string = secucode_constituent_temp_string + item + ","
                secucode_constituent_temp_string = secucode_constituent_temp_string[:-1]
                wind_data_trade_status = w.wss(secucode_constituent_temp_string, "trade_status",
                                               "tradeDate = " + trading_day_format_wind)
                wind_data_trade_status_df = pd.DataFrame(wind_data_trade_status.Data, index=wind_data_trade_status.Fields,
                                                         columns=wind_data_trade_status.Codes)
                # 无法交易的股票
                wind_data_trade_status_df.to_csv(path + self.sample + "_tradingstatus_" + trading_day + ".csv", encoding='gb18030')
                w.close()
            else:
                # 从文件夹中获取剔除ST之后的HS300成分股
                wind_data_constituent_df = pd.read_csv(path + self.sample + "_notST_" + trading_day_monthend + ".csv", encoding='gb18030')
                wind_data_constituent_df.set_index("Unnamed: 0", inplace = True)
                secucode_constituent_temp = wind_data_constituent_df.loc["wind_code"].tolist()
                secucode_constituent = [item[:6] for item in secucode_constituent_temp]
                # 从文件夹中获取无法交易的股票
                wind_data_trade_status_df = pd.read_csv(path + self.sample + "_tradingstatus_" + trading_day_monthend + ".csv", encoding='gb18030')
                wind_data_trade_status_df.set_index("Unnamed: 0", inplace=True)
            secucode_not_trade_temp = wind_data_trade_status_df.columns[
                                            wind_data_trade_status_df.loc["TRADE_STATUS"] != "交易"].tolist()
            secucode_not_trade = [item[:6] for item in secucode_not_trade_temp]
            # 剔除ST、无法交易之后的全部A股
            secucode_list = list(set(secucode_constituent) - set(secucode_not_trade))
        return secucode_list

    def data_sample_stocks_list_from_deletetable_temp(self):
        """
        从delete table获取trading_day之前最近一个月月末交易日的股票清单：剔除ST、上市满一年、交易状态为交易
        本函数为临时使用，之后删除
        """
        trading_day = self.trading_day
        trading_day_monthend = self.get_trading_day_last_monthend(trading_day)
        trading_day_monthend = self.convert_datetime_to_string(trading_day_monthend)
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6",
                                            port="13357",
                                            db="db_etf", charset="utf8mb4")
        if self.sample == "A":
            table_name = "A_Barra_Exposure_all_temp_delete"
            # table_name = "A_Barra_Exposure_all_delete"
        elif self.sample == "HS300":
            table_name = "A_Barra_Exposure_HS300_delete"
        elif self.sample == "ZZ500":
            table_name = "A_Barra_Exposure_ZZ500_delete"
        sql_string = "select SecuCode from " + table_name + " where Trading_Day = Date('{trading_day_monthend}')".format(
                    trading_day_monthend=trading_day_monthend)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        data = pd.read_sql(sql_stmt, engine)
        secucode_list = data["SecuCode"].tolist()
        return secucode_list

    def data_listed_date(self):
        """
        从聚源获取上市日期，判断上市是否满一年
        """
        trading_day = self.trading_day
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_stmt = text("SELECT InnerCode, CompanyCode, SecuCode, ListedDate  FROM SecuMain " \
                        "where SecuCategory = 1 and ListedSector in (1,2,6) and ListedDate <= Date('{trading_day}');".format(
                         trading_day=trading_day))
        engine = create_engine(engine_str)
        df_result = pd.read_sql(sql_stmt, engine)
        return df_result

    def output_data(self, factor_list_common, factor_list_profitability, factor_list_debtpayingability):
        df_data_qa = self.data_qa_factors(factor_list_common, factor_list_profitability, factor_list_debtpayingability)
        if df_data_qa.empty:
            raise TypeError("No qa Data.")
        df_data_predict = self.get_predict_data()
        if df_data_predict.empty:
            df_data_qa = df_data_qa.assign(EPFWD = np.nan)
            df_data_qa = df_data_qa.assign(EGRSF=np.nan)
            df_data_qa = df_data_qa.assign(EGRLF=np.nan)
        else:
            df_data_qa = pd.merge(df_data_qa, df_data_predict, on = ["SecuCode"], how = "outer")
        style_factor = df_data_qa.columns.tolist()[4:]
        style_factor.remove("Industry")
        style_factor.remove("StockReturn")
        columns_list = ["Trading_Day", "InnerCode", "SecuCode", "StockReturn", "MV"] + style_factor + ["Industry"]
        df_data_qa = df_data_qa.reindex(columns_list, axis=1)
        stock_list = self.data_sample_stocks_list()  # 从data_sample_stocks_list中获取股票清单
        # stock_list = self.data_sample_stocks_list_from_deletetable_temp() # 暂时从delete table中获取股票清单
        df_data_qa.index = df_data_qa["SecuCode"]
        result = df_data_qa.loc[stock_list]
        result.index = range(len(result))
        result = result.dropna(how="all")  # 删除全部为nan的个股
        result = result[~result["SecuCode"].isna()]  # 删除secucode缺失的个股
        result = result[~result["Industry"].isna()]  # 删除行业缺失的个股
        result.drop_duplicates(subset="SecuCode", keep="first", inplace=True) # 去重
        result["StockReturn"] = result["StockReturn"].fillna(0)  # 收益率填充为0
        result = result.sort_values(by = ["SecuCode"])
        result.index = range(len(result))
        if result.dropna(how="all").empty:
            raise TypeError("No Data.")
        return result



    def dataframe_merge(self, df_list, merge_key, how):
        result = df_list[0]
        for df in df_list[1:]:
            result = pd.merge(result, df, on = merge_key, how = how)
        return result
    def dataframe_adjust(self, df1, df2):
        index1 = df1.index.tolist()
        index2 = df2.index.tolist()
        index1 = sorted(index1)
        index2 = sorted(index2)
        index_union = list(sorted(set(index1).union(set(index2))))
        df2 = df2[~df2.index.duplicated()]
        df2 = df2.reindex(index_union)
        df2 = df2.fillna(method="pad")
        df2_new = df2.reindex(index1)
        return df2_new
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str
    def get_trading_day(self, start, end):
        """
        获取交易日
        :param start: 开始时间string
        :param end: 结束时间string
        :return: 交易日列表，日期为pd.Timestamp
        """
        if start == None:
            start_datetime = self.convert_string_to_datetime(end) - pd.DateOffset(years=1)
            start = self.convert_datetime_to_string(start_datetime)
        if end == None:
            end_datetime = self.convert_string_to_datetime(start) + pd.DateOffset(years=1)
            end = self.convert_datetime_to_string(end_datetime)
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate  from QT_TradingDayNew where SecuMarket = 83 and IfTradingDay  = 1 and
                            TradingDate >= DATE('{start}') and TradingDate <= DATE('{end}') order by TradingDate;""".format(
            start=start, end=end)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.rename(columns = {"TradingDate": "TradingDay"}, inplace = True)
        trading_day_list = sorted(trading_day_df["TradingDay"].tolist())
        return trading_day_list
    def get_trading_day_last_monthend(self, date = None):
        """
        获取date之前的最近一个月末交易日
        """
        if date == None:
            date = self.trading_day
        end = date
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select max(TradingDate) from QT_TradingDayNew where SecuMarket = 83 and IfMonthEnd  = 1 and
                                   TradingDate <= DATE('{end}');""".format(end=end)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_monthend = trading_day_df["max(TradingDate)"][0]
        return trading_day_monthend



if __name__ == "__main__":
    trading_day = "2010-01-01"
    # sample = "A"
    sample = "HS300"
    # sample = "ZZ500"
    factor_list_common = ["MV_total", "Lncap", "Hbeta",  "Rstr", "Dastd", "Cmra",  "Hsigma", "Midcap", "Btop", "Stom",
                          "Stoq", "Stoa", "Cetop", "Etop",  "Mlev", "Blev", "SW_Industry_name"]
    factor_list_profitability = ["Egro", "Sgro"]
    factor_list_debtpayingability = ["Dtoa"]
    getdata_cs = GetData(trading_day = trading_day, sample = sample)
    data_cs = getdata_cs.output_data(factor_list_common, factor_list_profitability, factor_list_debtpayingability)

