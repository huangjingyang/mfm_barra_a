# -*- coding: utf-8 -*-
"""
@author: Huang, Jingyang
@license: Copyright Reserved by Futu
@contact: jgyghuang@qq.com
@site: http://www.futu5.com
@software: PyCharm
@file: ReturnModel.py
@time: 2018/11/29
"""
import pandas as pd
import numpy as np
import statsmodels.api as sm
import datetime
from sqlalchemy import create_engine, text
import matplotlib.pyplot as plt
import numba

class ReturnModel():
    def __init__(self, start_date, end_date, sample):
        self.start_date = start_date #string
        self.end_date = end_date #string
        self.sample = sample
        self.style_sequence_factors = ["Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                 "Midcapitalization", "Residualvolatility", "Earningsyield"]#10个风格
        self.industry_sequence_factors1 = ["FerrousMetals", "CateringTourism","ConstructionMaterials",
                                           "Machinery","TransportationEquipment","CommunicationEquipment",
                                           "CommunicationServices","Financials","RealEstate","IndustrialConglomerates",
                                           "MedicalHealth","NonferrousMetals","Chemicals","HouseholdAppliances",
                                           "FoodBeverage","Electronics","Transportation","LightManufacturing",
                                           "Utilities","Textiles","Agriculture","CommercialTrade",
                                           "MineralIndustry"]#2014年之前，23个行业
        self.industry_sequence_factors1_Chinese = ["黑色金属","餐饮旅游","建筑建材","机械设备","交运设备","信息设备","信息服务",
                                                   "金融服务","房地产","综合","医药生物","有色金属","化工","家用电器","食品饮料",
                                                    "电子","交通运输","轻工制造","公用事业","纺织服装","农林牧渔",
                                                   "商业贸易","采掘"]#2014年之前，23个行业
        self.industry_sequence_factors2 = ["Steel", "Leisure", "ConstructionMaterials","ConstructionFurnishings",
                                           "Machinery","ElectricalEquipment","Defense","Automobiles",
                                           "CommunicationEquipment","Communication","Media","Financials",
                                           "NonBankFinancials","RealEstate","IndustrialConglomerates",
                                           "MedicalHealth","NonferrousMetals","Chemicals","HouseholdAppliances",
                                           "FoodBeverage","Electronics","Transportation","LightManufacturing",
                                           "Utilities","Textiles","Agriculture","CommercialTrade",
                                           "MineralIndustry"]#2014年之后，28个行业
        self.industry_sequence_factors2_Chinese = ["钢铁","休闲服务","建筑材料","建筑装饰","机械设备","电气设备",
                                                    "国防军工","汽车","计算机","通信","传媒","银行","非银金融",
                                                    "房地产","综合","医药生物","有色金属","化工","家用电器",
                                                    "食品饮料","电子","交通运输","轻工制造","公用事业",
                                                    "纺织服装","农林牧渔","商业贸易","采掘"]#2014年之后，28个行业
        self.industry_sequence_factors = ["ConstructionMaterials","Machinery","CommunicationEquipment","Financials",
                                 "RealEstate","IndustrialConglomerates", "MedicalHealth","NonferrousMetals",
                                 "Chemicals","HouseholdAppliances","FoodBeverage","Electronics",
                                 "Transportation","LightManufacturing","Utilities","Textiles","Agriculture",
                                 "CommercialTrade","MineralIndustry","FerrousMetals","CateringTourism",
                                 "Steel", "Leisure", "ConstructionFurnishings","ElectricalEquipment",
                                 "Defense","Automobiles","Communication","Media","NonBankFinancials",
                                 "TransportationEquipment","CommunicationServices"] # 32个行业名称
        self.sequence_factors = ["Country", "Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                 "Midcapitalization", "Residualvolatility", "Earningsyield",
                                 "ConstructionMaterials","Machinery","CommunicationEquipment","Financials",
                                 "RealEstate","IndustrialConglomerates", "MedicalHealth","NonferrousMetals",
                                 "Chemicals","HouseholdAppliances","FoodBeverage","Electronics",
                                 "Transportation","LightManufacturing","Utilities","Textiles","Agriculture",
                                 "CommercialTrade","MineralIndustry","FerrousMetals","CateringTourism",
                                 "Steel", "Leisure", "ConstructionFurnishings","ElectricalEquipment",
                                 "Defense","Automobiles","Communication","Media","NonBankFinancials",
                                 "TransportationEquipment","CommunicationServices"] # 1国家+10个风格+32个行业




    def get_processd_data(self, trading_day):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="db_etf", charset="utf8mb4")
        if self.sample == "A":
            table_name = "A_Barra_Exposure_all"
        elif self.sample == "HS300":
            table_name = "A_Barra_Exposure_HS300"
        elif self.sample == "ZZ500":
            table_name = "A_Barra_Exposure_ZZ500"
        sql_string = "select * from " + table_name + " where Trading_Day = Date('{trading_day}')".format(
                    trading_day=trading_day)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        data = pd.read_sql(sql_stmt, engine)
        return data

    @numba.jit
    def Return_CS(self, df_exposure, trading_day, regression_method):
        """
        :param df_exposure: 个股大类因子暴露度等信息
        :return factor_return: 因子收益率
        :type factor_return: dataframe, 1*23,
                        columns = 1 datetime + 10 style factors + 28 industry factors + 1 country factor
        :return residual_return: 个股残差收益
        :type residual_return: dataframe, N*3, columns = datetime + secucode + residual return
        """
        rf = 0.03 # 年化无风险利率
        rf_day = rf/252 # 日度无风险利率
        trading_day_datetime = self.convert_string_to_datetime(trading_day)
        mv_total = df_exposure["MV"].sum()
        df_exposure = df_exposure.set_index(["SecuCode"])
        df_industry = pd.get_dummies(df_exposure["Industry"])
        industry_sequence_factors = df_industry.columns.tolist()
        weight_industry_list = []
        for industry in industry_sequence_factors:
            secucode_industry = df_industry.index[df_industry[industry] == 1]
            mv_industry = df_exposure["MV"].loc[secucode_industry].sum()
            weight_industry = mv_industry/mv_total
            weight_industry_list.append(weight_industry)
        c_list = weight_industry_list/weight_industry_list[0]*(-1)
        column_name_0 = industry_sequence_factors[0]
        df_industry_new = pd.DataFrame(index = df_industry.index, columns = industry_sequence_factors[1:])
        for i in range(1, len(industry_sequence_factors)):
            column_name = industry_sequence_factors[i]
            c = c_list[i]
            df_industry_new[column_name] = df_industry[column_name] + c*df_industry[column_name_0]

        df_country_style_industry = pd.merge(df_exposure[["Country"] + self.style_sequence_factors], df_industry_new,
                                             left_index=True, right_index=True, how='left')
        sequence_factors_dropone = df_country_style_industry.columns.tolist()
        Y = df_exposure["StockReturn"].values.astype(np.float32) - rf_day
        X = df_country_style_industry.values.astype(np.float32)
        # W = (1 / np.sqrt(df_exposure["MV"])) / np.sum(1 / np.sqrt(df_exposure["MV"]))
        W = 1 / df_exposure["MV"]
        if regression_method == "OLS":
            results = sm.OLS(Y, X)
            fit = results.fit()
            # r-squared
            r_squared = fit.rsquared_adj
        elif regression_method == "WLS":
            results = sm.WLS(Y, X, W)
            fit = results.fit()
            # r-squared
            r_squared = fit.rsquared_adj
        elif regression_method == "RLM":
            results = sm.RLM(Y, X, M = sm.robust.norms.HuberT())
            fit = results.fit()
            # r-squared
            r_squared = None
        # 因子收益率
        factor_return = pd.DataFrame(fit.params, index = sequence_factors_dropone).T
        industry_beta_0 = (c_list[1:]*factor_return[industry_sequence_factors[1:]].values).sum()
        factor_return.insert(loc = 0, column=column_name_0, value=industry_beta_0)
        factor_return.insert(loc = 0, column = 'Trading_Day', value = trading_day_datetime)
        # 个股残差收益
        residual_return = pd.DataFrame(fit.resid, columns = ["Residual_Return"])
        residual_return = residual_return.assign(Trading_Day = trading_day_datetime)
        residual_return = residual_return.assign(SecuCode = df_exposure.index)
        # t值
        factor_tvalues = pd.DataFrame(fit.tvalues, index=sequence_factors_dropone).T # 缺少一个行业
        # bsr
        factor_bse = pd.DataFrame(fit.bse, index=sequence_factors_dropone).T # 缺少一个行业
        # Breusch-Pagan
        Breusch_Pagan_p = sm.stats.diagnostic.het_breushpagan(fit.resid, exog_het=fit.model.exog)[1]
        # # 残差和拟合值散点图
        # plt.rcParams['font.sans-serif'] = ['SimHei']
        # plt.rcParams['axes.unicode_minus'] = False
        # plt.scatter(fit.predict(), fit.resid)
        # plt.xlabel('拟合值')
        # plt.ylabel('残差')
        # plt.axhline(y=fit.resid.mean(), color='darkgreen', linewidth=2)
        # plt.show()
        # # qqplot
        # resid = fit.resid
        # fig = sm.qqplot(resid)
        # plt.show()
        return factor_return, residual_return, factor_tvalues, factor_bse, r_squared, Breusch_Pagan_p

    @numba.jit
    def output_timeseries(self, regression_method):
        """
        计算一段时间内每个时间截面上的回归结果，用于检验残差异方差、计算因子收益等。
        储存数据时运行output，output为仅计算一个时间截面的回归结果，在SaveDB中每计算一个截面则储存一次。
        """
        # trading_day_df = self.get_trading_days_monthend() # 仅取月末交易日
        trading_day_df = self.get_trading_days_days() # 日度
        trading_day_series = trading_day_df["TradingDate"]
        r_squared = []
        Breusch_Pagan_p = []
        factor_return = pd.DataFrame()
        factor_tvalues = pd.DataFrame()
        factor_bse = pd.DataFrame()
        residual_return = pd.DataFrame()
        for i in range(len(trading_day_df)):
            date_datetime = trading_day_series[i]
            date = self.convert_datetime_to_string(date_datetime)
            print(date)
            factors_data = self.get_processd_data(date)
            if factors_data.empty:
                raise TypeError("No processd data. Go back to DataProcess and SaveDB.")
            factor_return_date, residual_return_date, factor_tvalues_date, factor_bse_date, r_squared_date, Breusch_Pagan_p_date =\
                self.Return_CS(factors_data, date, regression_method)
            factor_return = pd.concat([factor_return, factor_return_date], sort=False)
            factor_tvalues = pd.concat([factor_tvalues, factor_tvalues_date], sort=False)
            factor_bse = pd.concat([factor_bse, factor_bse_date], sort=False)
            residual_return = pd.concat([residual_return, residual_return_date], sort=False)
            r_squared.append(r_squared_date)
            Breusch_Pagan_p.append(Breusch_Pagan_p_date)
        if regression_method != "RLM":
            r_squared = pd.DataFrame(r_squared, index = trading_day_series)
            self.plot_adjusted_rsquared(r_squared)
            print(r_squared.mean())
        self.plot_factor_return(factor_return)
        print("mean of bse: ", factor_bse.mean())
        print("mean of factor return: ", factor_return.drop(["Trading_Day"], axis = 1).mean()*100)
        return
    def output(self, regression_method, date):
        """
        在储存数据SaveDB时运行output，output为仅计算一个时间截面的回归结果，regression_method仅为HS300或ZZ500。
        factor_return_date按顺序排列后存入数据库A_Barra_Factor_Return_HS300/A_Barra_Factor_Return_ZZ500
        trading_day + 1 country + 10 style + 32 industry(2014年前后行业分类有变化) + create_time
        :return factor_return: 一个时间截面上的因子收益率
        :return residual_return_matrix: 一个时间截面上的残差收益率
        """
        date_datetime = self.convert_string_to_datetime(date)
        if self.sample not in ("HS300", "ZZ500"):
            raise TypeError("Only HS300/ZZ500 components will be saved.")
        if regression_method != "WLS":
            raise TypeError("Only WLS is accepted.")
        factors_data = self.get_processd_data(date)
        if factors_data.empty:
            raise TypeError("No processd data. Go back to DataProcess and SaveDB.")
        factor_return_date, residual_return_date, factor_tvalues_date, factor_bse_date, r_squared_date, Breusch_Pagan_p_date =\
            self.Return_CS(factors_data, date, regression_method)

        factor_exist = factor_return_date.columns.tolist()
        factor_industry_exist = list(set(factor_exist)-set(self.style_sequence_factors)-set(["Trading_Day", "Country"]))
        if date_datetime < pd.Timestamp(2014,1,1): #2014年之前，23个行业
            factor_industry_left = list(set(self.industry_sequence_factors1_Chinese) - set(factor_industry_exist))
            if factor_industry_left:
                factor_return = pd.concat([factor_return_date, pd.DataFrame(np.nan, columns=factor_industry_left,
                                                                             index=factor_return_date.index)], axis=1)
            else:
                factor_return = factor_return_date.copy()
            factor_return.rename(columns={"黑色金属": "FerrousMetals", "餐饮旅游": "CateringTourism",
                                        "建筑建材": "ConstructionMaterials", "机械设备": "Machinery",
                                        "交运设备": "TransportationEquipment", "信息设备": "CommunicationEquipment",
                                        "信息服务": "CommunicationServices", "金融服务": "Financials",
                                        "房地产": "RealEstate", "综合": "IndustrialConglomerates", "医药生物": "MedicalHealth",
                                        "有色金属": "NonferrousMetals", "化工": "Chemicals", "家用电器": "HouseholdAppliances",
                                        "食品饮料": "FoodBeverage", "电子": "Electronics", "交通运输": "Transportation",
                                        "轻工制造": "LightManufacturing", "公用事业": "Utilities", "纺织服装": "Textiles",
                                        "农林牧渔": "Agriculture", "商业贸易": "CommercialTrade", "采掘": "MineralIndustry"},
                               inplace=True)
            factor_industry_left2 = list(set(self.industry_sequence_factors) - set(self.industry_sequence_factors1))
            factor_return = pd.concat([factor_return, pd.DataFrame(np.nan, columns=factor_industry_left2,
                                                                        index=factor_return.index)], axis=1)
            factor_return = factor_return[["Trading_Day"]+self.sequence_factors]

        else: # 2014年之后，28个行业，包括2014年
            factor_industry_left = list(set(self.industry_sequence_factors2_Chinese) - set(factor_industry_exist))
            if factor_industry_left:
                factor_return = pd.concat([factor_return_date, pd.DataFrame(np.nan, columns=factor_industry_left, index=factor_return_date.index)], axis=1)
            else:
                factor_return = factor_return_date.copy()
            factor_return.rename(columns={"钢铁": "Steel", "休闲服务": "Leisure",
                                        "建筑材料": "ConstructionMaterials", "建筑装饰": "ConstructionFurnishings",
                                        "机械设备": "Machinery", "电气设备": "ElectricalEquipment", "国防军工": "Defense",
                                        "汽车": "Automobiles", "计算机": "CommunicationEquipment", "通信": "Communication",
                                        "传媒": "Media", "银行": "Financials", "非银金融": "NonBankFinancials",
                                        "房地产": "RealEstate", "综合": "IndustrialConglomerates", "医药生物": "MedicalHealth",
                                        "有色金属": "NonferrousMetals", "化工": "Chemicals", "家用电器": "HouseholdAppliances",
                                        "食品饮料": "FoodBeverage", "电子": "Electronics", "交通运输": "Transportation",
                                        "轻工制造": "LightManufacturing", "公用事业": "Utilities", "纺织服装": "Textiles",
                                        "农林牧渔": "Agriculture", "商业贸易": "CommercialTrade", "采掘": "MineralIndustry"},
                               inplace=True)
            factor_industry_left2 = list(set(self.industry_sequence_factors) - set(self.industry_sequence_factors2))
            factor_return = pd.concat([factor_return, pd.DataFrame(np.nan, columns=factor_industry_left2,
                                                               index=factor_return.index)], axis=1)
            factor_return = factor_return[["Trading_Day"]+self.sequence_factors]
        return factor_return, residual_return_date
    def get_trading_days_monthend(self):
        """
        获取月末交易日，该时间与SaveDB中的日期一致
        """
        start_date = self.start_date
        end_date = self.end_date
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate from QT_TradingDayNew where SecuMarket = 83 and IfMonthEnd  = 1 and
                                        TradingDate >= DATE('{start_date}') and TradingDate <= DATE('{end_date}') order by  
                                        TradingDate;""".format(start_date=start_date, end_date=end_date)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.index = range(len(trading_day_df))
        return trading_day_df
    def get_trading_days_days(self):
        """
        获取日度交易日，该时间与SaveDB中的日期一致
        """
        start_date = self.start_date
        end_date = self.end_date
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate  from QT_TradingDayNew where SecuMarket = 83 and IfTradingDay  = 1 and
                                        TradingDate >= DATE('{start_date}') and TradingDate <= DATE('{end_date}') order by 
                                        TradingDate;""".format(start_date=start_date, end_date=end_date)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.index = range(len(trading_day_df))
        return trading_day_df
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str
    def plot_adjusted_rsquared(self, r_squared):
        r_squared_mean = r_squared.rolling(21).mean()[20:]
        plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
        plt.rcParams['axes.unicode_minus'] = False
        plt.figure('fig', figsize=(8, 2.5))
        plt.plot(r_squared_mean.index, r_squared_mean.values)
        plt.xlabel('时间')
        plt.ylabel('Mean of Adjusted R-squared')
        plt.title("Mean of Adjusted R-squared")
        plt.show()
    def plot_factor_return(self, factor_return):
        factor_return = factor_return.set_index(["Trading_Day"])
        plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
        plt.rcParams['axes.unicode_minus'] = False
        plt.figure('fig', figsize=(10, 5))
        for factor_name in self.style_sequence_factors:
            plt.plot(factor_return.index, factor_return[factor_name].cumsum(), label=factor_name)
        plt.xlabel('时间')
        plt.ylabel('因子收益率')
        plt.title("因子收益率")
        plt.legend(loc='lower left')
        plt.show()


if __name__ == "__main__":
    start_date = "2014-01-30"
    end_date = "2018-12-28"
    sample = "A"
    # sample = "HS300"
    # sample = "ZZ500"
    returnmodel = ReturnModel(start_date, end_date, sample)
    # returnmodel.output_timeseries("OLS")
    # returnmodel.output_timeseries("WLS")
    returnmodel.output_timeseries("RLM")

    # start_date = None
    # end_date = None
    # sample = "HS300"
    # returnmodel = ReturnModel(start_date, end_date, sample)
    # returnmodel.output("WLS", "2014-01-30")



