# -*- coding: utf-8 -*-
"""
@author: Huang, Jingyang
@license: Copyright Reserved by Futu
@contact: jgyghuang@qq.com
@site: http://www.futu5.com
@software: PyCharm
@file: DataProcess.py
@time: 2019/04/03
"""
import pandas as pd
import numpy as np
from GetData import GetData
from sklearn import linear_model
import datetime
import warnings
from sqlalchemy import create_engine, text


class DataProcess():
    def __init__(self, trading_day, sample):
        self.trading_day = trading_day # string
        self.sample = sample
        self.sequence_factors = ['Size', 'Beta', 'Momentum', 'Btop', 'Growth', 'Liquidity',  'Leverage',
                                 'Midcapitalization', 'Residualvolatility', 'Earningsyield',
                                 'Industry', 'Country']

    def get_data(self):
        "获取原始数据"
        factor_list_common = ["MV_total", "Lncap", "Hbeta", "Rstr", "Dastd", "Cmra", "Hsigma", "Midcap", "Btop", "Stom",
                              "Stoq", "Stoa", "Cetop", "Etop", "Mlev", "Blev", "SW_Industry_name"]
        factor_list_profitability = ["Egro", "Sgro"]
        factor_list_debtpayingability = ["Dtoa"]
        getdata = GetData(self.trading_day, self.sample)
        factor_raw = getdata.output_data(factor_list_common, factor_list_profitability, factor_list_debtpayingability)
        return factor_raw

    def process_temp1(self, data):
        """
        对当期的因子暴露度做缺失值填充、去极值、标准化、正交化、合并等操作
        将未进行斯密特正交化的因子暴露度储存进A_Barra_Exposure_all_temp，之后删除
        """
        trading_day = self.trading_day
        trading_day_datetime = self.convert_string_to_datetime(trading_day)
        data_raw = data.copy()
        # data_raw.index = data_raw["SecuCode"].astype(int)
        data_raw.index = data_raw["SecuCode"]
        data_raw.sort_index(inplace = True)
        # 27风格因子+1列行业
        style_industry_raw = data_raw.drop(["Trading_Day", "InnerCode", "SecuCode", "StockReturn", "MV"], axis = 1)
        # 27个风格因子
        factors_raw = style_industry_raw.drop(["Industry"], axis = 1)
        # 个股未来收益率
        Return = data_raw["StockReturn"].to_frame()
        # 市值
        MarketValue = data_raw["MV"].to_frame()
        # 行业
        Industry = data_raw["Industry"].to_frame()
        # 行业哑变量
        Industryfactor = pd.get_dummies(style_industry_raw["Industry"])
        # Industryfactor.rename(columns={'房地产': 'RealEstate', '公用事业': 'Utilities', '金融': 'Finance', '能源': 'Energy',
        #                                '电信业务': 'Telecom', '工业': 'Industry', '信息技术': 'InformationTechnology',
        #                                '原材料': 'Materials', '医疗保健': 'MedicalHealth',
        #                                '非日常生活消费品': 'ConsumerDiscretionary', '日常消费品': 'ConsumerStaples'}, inplace=True)
        # 27个风格因子均为nan的股票用行业均值填充
        emptyfactor = style_industry_raw[factors_raw.isna().all(axis=1)]
        if emptyfactor.empty:
            factordata = style_industry_raw.copy()
        else:
            notemptyfactor = style_industry_raw[factors_raw.notnull().any(axis=1)]
            factordatafillna = pd.DataFrame()
            industry_list = list(set(emptyfactor["Industry"]))
            for item in industry_list: #沿行业循环，用行业中位数填充有缺失的个股
                fill = emptyfactor[emptyfactor['Industry'] == item].fillna(style_industry_raw[style_industry_raw['Industry'] == item].median())
                factordatafillna = factordatafillna.append(fill)
            factordata = notemptyfactor.append(factordatafillna)
            factordata.sort_index(inplace = True)

        # 去极值、标准化等处理
        factordata_standard = self.StdInEachIndustry(factordata, MarketValue)
        # 合成大类因子
        factordata_style = self.StyleFactor_output(factordata_standard)
        # 17个风格因子名称
        style_name_list = factordata_style.columns.tolist()
        # 将行业哑变量合并
        factordata_style_industry = pd.merge(factordata_style, Industryfactor, left_index=True, right_index=True)
        # 暴露度缺失补填
        factordata_Schmidt = self.Factor_fillna(factordata_style_industry)[style_name_list]


        # 暂时储存上面步骤的结果，全市场A股，表格命名带temp
        # 交易日、股票代码、未来一个月收益率、市值、大类因子暴露度、行业（一列）、国家因子
        # 2015年~2018年，48期结果
        # 为了运算斯密特正交化之前的IC的相关性、VIF

        # 风格因子暴露和行业因子暴露合并
        exposure_style_industry_country = pd.merge(factordata_Schmidt, Industry, left_index = True, right_index = True, how = 'left')
        # 加入个股收益率
        exposure_style_industry_country = pd.merge(exposure_style_industry_country, Return, left_index = True, right_index = True, how = 'left')
        # 加入个股市值
        exposure_style_industry_country = pd.merge(exposure_style_industry_country, MarketValue, left_index = True, right_index = True, how='left')
        # 加入截距项
        exposure_style_industry_country = exposure_style_industry_country.assign(Country = 1)
        # 调整顺序
        exposure_style_industry_country = exposure_style_industry_country[["MV", "StockReturn"] + self.sequence_factors]
        # 加入时间
        exposure_style_industry_country.insert(loc = 0, column = 'Trading_Day', value = self.convert_string_to_datetime(trading_day))
        # 加入个股代码
        exposure_style_industry_country.insert(loc = 0, column = 'SecuCode', value = exposure_style_industry_country.index)
        exposure_style_industry_country.index = range(len(exposure_style_industry_country))
        exposure_style_industry_country.sort_values(by = "SecuCode", inplace = True)
        exposure_style_industry_country.index = range(len(exposure_style_industry_country))
        return exposure_style_industry_country

    def process_temp2(self):
        """
        对当期的因子暴露度做缺失值填充、去极值、标准化、正交化、合并等操作
        将处理完善后的因子暴露度储存进A_Barra_Exposure_all，与process_temp产生的结果一致
        只是数据源为从A_Barra_Exposure_all_temp取得，2014年之前（不含）和2019年之后（包含）的运算应该运行process_temp
        """
        # 从A_Barra_Exposure_all_temp表中获取数据再运算
        # 储存为正式的表格：A_Barra_Exposure_all
        # 交易日、股票代码、未来一个月收益率、市值、大类因子暴露度、行业（一列）、国家因子
        trading_day = self.trading_day
        style_name_list = self.sequence_factors.copy()
        style_name_list.remove("Country")
        style_name_list.remove("Industry")
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="db_etf", charset="utf8mb4")
        sql_string = "select * from A_Barra_Exposure_all_temp where Trading_Day = Date('{trading_day}')".format(
                        trading_day=trading_day)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        data_temp = pd.read_sql(sql_stmt, engine)
        data_temp.set_index("SecuCode", inplace = True)
        factordata_Schmidt = data_temp[style_name_list]
        Industry = data_temp["Industry"].to_frame()
        MarketValue = data_temp["MV"].to_frame()
        Return = data_temp["StockReturn"].to_frame()

        # 斯密特正交化
        factordata_Schmidt['Midcapitalization'] = self.Schmidt(factordata_Schmidt
                                                            [['Size', 'Midcapitalization']])['Midcapitalization']
        factordata_Schmidt['Residualvolatility'] = self.Schmidt(factordata_Schmidt
                                                            [['Beta', 'Residualvolatility']])['Residualvolatility']
        # 以市值进一步标准化
        factordata_secondstandard = self.SecondStandard_MV(MarketValue, factordata_Schmidt)

        # 风格因子暴露和行业因子暴露合并
        exposure_style_industry_country = pd.merge(factordata_secondstandard, Industry, left_index = True, right_index = True, how = 'left')
        # 加入个股收益率
        exposure_style_industry_country = pd.merge(exposure_style_industry_country, Return, left_index = True, right_index = True, how = 'left')
        # 加入个股市值
        exposure_style_industry_country = pd.merge(exposure_style_industry_country, MarketValue, left_index = True, right_index = True, how='left')
        # 加入截距项
        exposure_style_industry_country = exposure_style_industry_country.assign(Country = 1)
        # 调整顺序
        exposure_style_industry_country = exposure_style_industry_country[["MV", "StockReturn"] + self.sequence_factors]
        # 加入时间
        exposure_style_industry_country.insert(loc = 0, column = 'Trading_Day', value = self.convert_string_to_datetime(trading_day))
        # 加入个股代码
        exposure_style_industry_country.insert(loc = 0, column = 'SecuCode', value = exposure_style_industry_country.index)
        exposure_style_industry_country.index = range(len(exposure_style_industry_country))
        exposure_style_industry_country.sort_values(by = "SecuCode", inplace = True)
        exposure_style_industry_country.index = range(len(exposure_style_industry_country))
        return exposure_style_industry_country


    def process(self, data):
        """
        对当期的因子暴露度做缺失值填充、去极值、标准化、正交化、合并等操作
        """
        trading_day = self.trading_day
        trading_day_datetime = self.convert_string_to_datetime(trading_day)
        data_raw = data.copy()
        # data_raw.index = data_raw["SecuCode"].astype(int)
        data_raw.index = data_raw["SecuCode"]
        data_raw.sort_index(inplace = True)
        # 27风格因子+1列行业
        style_industry_raw = data_raw.drop(["Trading_Day", "InnerCode", "SecuCode", "StockReturn", "MV"], axis = 1)
        # 27个风格因子
        factors_raw = style_industry_raw.drop(["Industry"], axis = 1)
        # 个股未来收益率
        Return = data_raw["StockReturn"].to_frame()
        # 市值
        MarketValue = data_raw["MV"].to_frame()
        # 行业
        Industry = data_raw["Industry"].to_frame()
        # 行业哑变量
        Industryfactor = pd.get_dummies(style_industry_raw["Industry"])
        # Industryfactor.rename(columns={'房地产': 'RealEstate', '公用事业': 'Utilities', '金融': 'Finance', '能源': 'Energy',
        #                                '电信业务': 'Telecom', '工业': 'Industry', '信息技术': 'InformationTechnology',
        #                                '原材料': 'Materials', '医疗保健': 'MedicalHealth',
        #                                '非日常生活消费品': 'ConsumerDiscretionary', '日常消费品': 'ConsumerStaples'}, inplace=True)
        # 27个风格因子均为nan的股票用行业均值填充
        emptyfactor = style_industry_raw[factors_raw.isna().all(axis=1)]
        if emptyfactor.empty:
            factordata = style_industry_raw.copy()
        else:
            notemptyfactor = style_industry_raw[factors_raw.notnull().any(axis=1)]
            factordatafillna = pd.DataFrame()
            industry_list = list(set(emptyfactor["Industry"]))
            for item in industry_list: #沿行业循环，用行业中位数填充有缺失的个股
                fill = emptyfactor[emptyfactor['Industry'] == item].fillna(style_industry_raw[style_industry_raw['Industry'] == item].median())
                factordatafillna = factordatafillna.append(fill)
            factordata = notemptyfactor.append(factordatafillna)
            factordata.sort_index(inplace = True)

        # 去极值、标准化等处理
        factordata_standard = self.StdInEachIndustry(factordata, MarketValue)
        # 合成大类因子
        factordata_style = self.StyleFactor_output(factordata_standard)
        # 17个风格因子名称
        style_name_list = factordata_style.columns.tolist()
        # 将行业哑变量合并
        factordata_style_industry = pd.merge(factordata_style, Industryfactor, left_index=True, right_index=True)
        # 暴露度缺失补填
        factordata_Schmidt = self.Factor_fillna(factordata_style_industry)[style_name_list]

        # 斯密特正交化
        factordata_Schmidt['Midcapitalization'] = self.Schmidt(factordata_Schmidt
                                                            [['Size', 'Midcapitalization']])['Midcapitalization']
        factordata_Schmidt['Residualvolatility'] = self.Schmidt(factordata_Schmidt
                                                            [['Beta', 'Residualvolatility']])['Residualvolatility']
        # 以市值进一步标准化
        factordata_secondstandard = self.SecondStandard_MV(MarketValue, factordata_Schmidt)

        # 风格因子暴露和行业因子暴露合并
        exposure_style_industry_country = pd.merge(factordata_secondstandard, Industry, left_index = True, right_index = True, how = 'left')
        # 加入个股收益率
        exposure_style_industry_country = pd.merge(exposure_style_industry_country, Return, left_index = True, right_index = True, how = 'left')
        # 加入个股市值
        exposure_style_industry_country = pd.merge(exposure_style_industry_country, MarketValue, left_index = True, right_index = True, how='left')
        # 加入截距项
        exposure_style_industry_country = exposure_style_industry_country.assign(Country = 1)
        # 调整顺序
        exposure_style_industry_country = exposure_style_industry_country[["MV", "StockReturn"] + self.sequence_factors]
        # 加入时间
        exposure_style_industry_country.insert(loc = 0, column = 'Trading_Day', value = self.convert_string_to_datetime(trading_day))
        # 加入个股代码
        exposure_style_industry_country.insert(loc = 0, column = 'SecuCode', value = exposure_style_industry_country.index)
        exposure_style_industry_country.index = range(len(exposure_style_industry_country))
        exposure_style_industry_country.sort_values(by = "SecuCode", inplace = True)
        exposure_style_industry_country.index = range(len(exposure_style_industry_country))
        return exposure_style_industry_country

    def StdInEachIndustry(self, factordata, MarketValue):
        # 对各因子（每列非NaN值）进行去极值、标准化处理
        factordata = factordata.drop(["Industry"], axis = 1)
        factorname = factordata.columns.tolist()  # 因子名称
        for x in factorname:
            temp = factordata[x][factordata[x].notnull()]  # x因子非NaN值
            temp1 = temp.copy()
            length = int(np.ceil(len(temp) * 0.01))
            for i in range(length):
                temp[temp.idxmax()] = np.NAN
                temp[temp.idxmin()] = np.NAN
                temp = temp.dropna()
                Mean = temp.mean()  # 平均mean
                Std = temp.std()  # 平均加权std
                temp1[temp1 > (Mean + 3 * Std)] = Mean + 3 * Std # 中位数去极值
                temp1[temp1 < (Mean - 3 * Std)] = Mean - 3 * Std # 中位数去极值
                temp_Cap = MarketValue['MV'][temp1.index]
                if x == 'Lncap':
                    CapWeightedMean = temp1.mean()
                else:
                    CapWeightedMean = (temp_Cap * temp1).sum() / temp_Cap.sum()  # 基于行业市值的加权平均mean
                EqualWeightedStd = temp1.std()  # 平均加权的std
                factordata[x][factordata[x].notnull()] = (temp1 - CapWeightedMean) / EqualWeightedStd
        factordata.sort_index(axis = 0, inplace=True)
        return factordata
    def Schmidt(self, data):
        output = pd.DataFrame()
        mat = np.mat(data)
        output[0] = np.array(mat[:,0].reshape(len(data),))[0]
        for i in range(1,data.shape[1]):
            tmp = np.zeros(len(data))
            for j in range(i):
                up = np.array((mat[:,i].reshape(1,len(data)))*(np.mat(output[j]).reshape(len(data),1)))[0][0]
                down = np.array((np.mat(output[j]).reshape(1,len(data)))*(np.mat(output[j]).reshape(len(data),1)))[0][0]
                tmp = tmp+up*1.0/down*(np.array(output[j]))
            output[i] = np.array(mat[:,i].reshape(len(data),))[0]-np.array(tmp)
        output.index = data.index
        output.columns = data.columns
        return output
    def StyleFactor(self, components_data, weight, style_name):
        """
        小类因子合成大类因子，放在因子标准化之后
        :param components_data: components exposure after standardization, dataframe
        :param weight: weight for each components, list
        :param style_name: style factor name, string
        :return output: style factor exposure, dataframe
        """
        df = components_data.copy()
        components_name = df.columns.tolist()
        if len(components_name) == 1:
            output = df.rename({components_name[0]: style_name})
            return output
        else:
            output = pd.DataFrame(index = df.index, columns = [style_name])
            index1 = df.index[~df.isna().any(axis = 1)] #全部非nan
            output[style_name][index1] = (df.reindex(index1)*weight).sum(axis = 1)
            index2 = df.index[df.isna().all(axis = 1)] #全部为nan
            index3 = list(set(df.index) - set(index1) - set(index2)) #至少有一个为nan, 但不是全部为nan
            temp = df.reindex(index3)*weight
            temp_notnan = (~temp.isna()).astype(int)
            weight_loc = temp_notnan*weight
            weight_left = 1 - weight_loc.sum(axis = 1)
            weight_allocation = weight_left/temp_notnan.sum(axis = 1)
            weight_allocation_new = np.tile(weight_allocation, (np.shape(temp)[1], 1)).T
            output[style_name][index3] = (df.reindex(index3)*weight_allocation_new + temp).sum(axis = 1)
        return output
    def StyleFactor_output(self, factordata_standard):
        factordata_style = pd.DataFrame()
        for single_name in ["Lncap", "Hbeta", "Rstr", "Midcap", "Btop"]:
            single_style = self.StyleFactor(factordata_standard[single_name].to_frame(), [1.0], single_name)
            factordata_style = pd.concat([factordata_style, single_style], axis=1)

        factordata_style = pd.concat([factordata_style, self.StyleFactor(factordata_standard[["Etop", "Cetop", "EPFWD"]],
                                                                         [0.11, 0.21, 0.68],  "Earningsyield")], axis=1)
        factordata_style = pd.concat([factordata_style, self.StyleFactor(factordata_standard[["Egro", "Sgro", "EGRLF", "EGRSF"]],
                                                                         [0.24, 0.47, 0.18, 0.11], "Growth")], axis=1)
        factordata_style = pd.concat([factordata_style, self.StyleFactor(factordata_standard[["Mlev", "Blev", "Dtoa"]],
                                                                         [0.38, 0.35, 0.27], "Leverage")], axis=1)
        factordata_style = pd.concat([factordata_style, self.StyleFactor(factordata_standard[["Stom", "Stoq", "Stoa"]],
                                                                         [0.35, 0.35, 0.3], "Liquidity")], axis=1)
        factordata_style = pd.concat([factordata_style, self.StyleFactor(factordata_standard[["Dastd", "Cmra", "Hsigma"]],
                                                                         [0.74, 0.16, 0.1], "Residualvolatility")], axis=1)
        factordata_style.rename(columns = {"Hbeta": "Beta", "Lncap": "Size", "Rstr": "Momentum", "Midcap": "Midcapitalization"}, inplace=True)
        return factordata_style
    def Factor_fillna(self, factordata_style_industry):
        warnings.simplefilter("ignore")
        # factordata_style_industry为经过去极值、标准化、合成后的大类风格因子+行业哑变量
        factordata_base = factordata_style_industry.dropna()
        factordata_missing = factordata_style_industry[factordata_style_industry.isna().any(axis=1)]
        for i, row in factordata_missing.iterrows():
            # 这一row中为nan的个数
            counter = row.isna().sum()
            # 定位这一row中是nan的因子名称
            missingfactorlocation = row[row.isna()].index.tolist()
            for j in range(counter):
                regr = linear_model.LinearRegression(fit_intercept=True)
                #regr.fit(factordata_base.loc[:, row[row.notnull()].index], factordata_base.loc[:, missingfactorlocation[j]])
                regr.fit(factordata_base[row[row.notnull()].index], factordata_base[missingfactorlocation[j]])
                factordata_missing.loc[i, missingfactorlocation[j]] = sum(regr.coef_ * row[row.notnull()]) + regr.intercept_
                #factordata_missing[missingfactorlocation[j]][i] = sum(regr.coef_ * row[row.notnull()]) + regr.intercept_
        output = factordata_base.append(factordata_missing).sort_index()
        return output
    def SecondStandard_MV(self, MarketValue, style_factor):
        mv = (MarketValue['MV'][style_factor.index])
        multiply = style_factor * np.tile(mv,(np.shape(style_factor)[1], 1)).T
        CapWeightedMean = multiply.sum() / mv.sum()
        EqualWeightedStd = style_factor.std()
        output = (style_factor - CapWeightedMean) / EqualWeightedStd
        output.sort_index(inplace = True)
        return output
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str
    def output(self):
        # output = self.process_temp2()  # A_Barra_Exposure_all



        data_all = self.get_data()
        if data_all.empty:
            return pd.DataFrame([])
        # 储存进A_Barra_Exposure_HS300/A_Barra_Exposure_ZZ500/2014年之前或2018年之后的A_Barra_Exposure_all
        output = self.process(data_all)

        # output = self.process_temp1(data_all) # 储存进A_Barra_Exposure_all_temp
        return output


if __name__ == "__main__":
    trading_day = "2018-05-02"
    sample = "A"
    dataprocess = DataProcess(trading_day = trading_day, sample = sample)
    output = dataprocess.output()
    print(output)