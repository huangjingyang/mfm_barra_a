# -*- coding: utf-8 -*-
"""
@author: Huang, Jingyang
@contact: jgyghuang@qq.com
@software: PyCharm
@file: FactorTest.py
@time: 2019/04/04
"""

from sqlalchemy import create_engine, text
import statsmodels.api as sm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import datetime
import seaborn as sns

class FactorTest():
    def __init__(self, start, end, sample):
        self.start = start
        self.end = end
        self.sample = sample

    def regression_ic_cs(self, data):
        """
        回归法有效性检验、IC值
        本函数只进行一个横截面的回归和IC值计算
        """
        factor_name_list = ['Size', 'Beta', 'Momentum', 'Btop', 'Growth', 'Liquidity',  'Leverage',
                                 'Midcapitalization', 'Residualvolatility', 'Earningsyield'] # A股十大风格因子
        result_dict = dict()
        for factor_name in factor_name_list:
            col = list(set(["StockReturn", "Size", "MV", "Industry"] + [factor_name]))
            source_data_df = data[col]
            source_data_df = source_data_df.dropna()
            endog = source_data_df[factor_name]
            second_endog = source_data_df['StockReturn'].to_frame()
            weights = 1 / source_data_df['MV']
            # df_industry = pd.get_dummies(source_data_df["Industry"])
            # if factor_name == "Size":
            #     exog = df_industry
            # else:
            #     exog = df_industry.join(source_data_df["Size"])
            # first_regression = sm.WLS(endog, exog, weights=weights).fit()
            # second_regression = sm.WLS(second_endog, first_regression.resid, weights = weights).fit()
            # coef = second_regression.params.values[0]
            # t = second_regression.tvalues.values[0]
            # ic_df = second_endog.assign(resid=first_regression.resid.values)
            # ic_value = ic_df.corr(method="spearman").values[0, 1]
            second_regression = sm.WLS(second_endog, sm.add_constant(endog), weights=weights).fit()
            coef = second_regression.params.values[1]
            t = second_regression.tvalues.values[1]
            ic_value = source_data_df[['StockReturn', factor_name]].corr(method="spearman").values[0, 1]
            result_dict[factor_name + "_coef"] = coef
            result_dict[factor_name + "_t"] = t
            result_dict[factor_name + "_ic"] = ic_value
        return result_dict
    def layer_cs(self, data, layer):
        """
        分层法
        本函数只进行一个横截面的分层计算
        """
        factor_name_list = ['Size', 'Beta', 'Momentum', 'Btop', 'Growth', 'Liquidity', 'Leverage',
                            'Midcapitalization', 'Residualvolatility', 'Earningsyield']  # A股十大风格因子
        result_df = data[["SecuCode", "MV", "Industry", "StockReturn"]]
        result_df.index = result_df["SecuCode"]
        for factor_name in factor_name_list:
            source_data_df = data[['Industry'] + [factor_name]]
            source_data_df.index = data["SecuCode"]
            source_data_df = source_data_df.replace([np.inf, -np.inf], np.nan)
            source_data_df = source_data_df.dropna()
            if source_data_df.empty:
                return pd.DataFrame([])
            # 对不同行业进行分层，再以基准行业权重加权收益率
            source_data_df = source_data_df.groupby(["Industry"]).apply(self.LayerFunction, factor_name, layer)
            # 直接对全部股票分层，每一层内收益率以市值加权，或者等权平均
            # source_data_df = self.LayerFunction(source_data_df, factor_name, layer)
            result_df = pd.concat([result_df, source_data_df[[factor_name, "layer_" + factor_name]]], axis = 1)
        result_df.index = range(len(result_df))
        return result_df
    def LayerFunction(self, df, factorname, L):
        df['layer_'+factorname] = np.nan
        for l in range(1, L):
            n = round(1 - (l - 1) / L, 1)
            m = round(1 - l / L, 1)
            df['layer_'+factorname][(df[factorname] > df[factorname].quantile(m, interpolation = "midpoint"))
                                    & (df[factorname] <= df[factorname].quantile(n, interpolation = "midpoint"))] = l
        df['layer_'+factorname][df[factorname] <= df[factorname].quantile(1 / L, interpolation = "midpoint")] = L
        return df
    def cal_VIF(self, style_exposure):
        """
        计算一个截面的所有风格因子的方差扩大因子
        """
        factor_name_list = style_exposure.columns.tolist()
        vif = pd.DataFrame(index = [0], columns = factor_name_list)
        for y_name in factor_name_list:
            x_name = factor_name_list.copy()
            x_name.remove(y_name)
            x = style_exposure[x_name]
            y = style_exposure[y_name]
            reg = sm.OLS(y, x).fit()
            vif_factor = 1/(1 - reg.rsquared)
            vif[y_name] = vif_factor
        return vif

    def calculate_timeseries(self, layer):
        """
        计算所有（月）交易日上的回归结果、IC结果、分层结果
        按时间循环，全部结果输入factor_estimate_and_show_plot中
        """
        factor_name_list = ['Size', 'Beta', 'Momentum', 'Btop', 'Growth', 'Liquidity', 'Leverage',
                            'Midcapitalization', 'Residualvolatility', 'Earningsyield']  # A股十大风格因子
        trading_day_monthend = self.get_trading_days_monthend()
        date_list = sorted(trading_day_monthend["TradingDate"].tolist())
        result_df_regression = pd.DataFrame()
        result_df_layer = pd.DataFrame()
        result_df_vif = pd.DataFrame()
        for i in range(len(date_list)):
            trading_day_datetime = date_list[i]
            trading_day = self.convert_datetime_to_string(trading_day_datetime)
            print(trading_day)
            # data = self.get_data_from_barra_temp(trading_day)
            data = self.get_data_from_barra(trading_day)
            if data.empty:
                date_list.remove(trading_day_datetime)
            else:
                # VIF
                vif_date = self.cal_VIF(data[factor_name_list])
                result_df_vif = pd.concat([result_df_vif, vif_date], sort = False)

                # 分层法
                result_df_layer_date = self.layer_cs(data, layer)
                result_df_layer_date = result_df_layer_date.assign(TradingDay = self.convert_string_to_datetime(trading_day))
                result_df_layer = pd.concat([result_df_layer, result_df_layer_date], sort = False)

                # 回归、IC
                result_dict_regression = self.regression_ic_cs(data)
                result_df_regression = pd.concat([result_df_regression, pd.DataFrame.from_dict(result_dict_regression,
                                                                                      orient = 'index').T], sort = False)
        result_df_regression['TradingDay'] = date_list
        result_df_vif["TradingDay"] = date_list
        return result_df_regression,  result_df_layer, result_df_vif



    def cal_layer_return(self, df_source_df, benchmark_name):
        """
        计算所有时间段的所有因子的分层收益率
        """
        factor_name_list = ['Size', 'Beta', 'Momentum', 'Btop', 'Growth', 'Liquidity', 'Leverage',
                            'Midcapitalization', 'Residualvolatility', 'Earningsyield']  # A股十大风格因子
        trading_days_list = sorted(list(set(df_source_df["TradingDay"])))
        result_df = pd.DataFrame()
        for i in range(len(trading_days_list)):
            start = trading_days_list[i]
            # 在对不同行业分层的情况下需用到
            year = start.year
            BenchmarkWeight = pd.read_csv("D:\\rqalpha\\myfile_rqalpha\\BarraProject\\MFM_Barra_A\\data\\benchmark\\" + benchmark_name + "Weight_" + str(year) + ".csv", encoding = "gb2312")
            BenchmarkWeight["code"] = BenchmarkWeight["code"].apply(lambda x: str(x).zfill(6))
            BenchmarkWeight_sum = BenchmarkWeight.groupby(["industry"])["weight"].sum()
            BenchmarkWeight_sum = BenchmarkWeight_sum / BenchmarkWeight_sum.sum()
            BenchmarkWeight_sum = BenchmarkWeight_sum.to_frame(name = "Industry_weight")
            BenchmarkWeight_sum["Industry"] = BenchmarkWeight_sum.index
            df_source_df_date = df_source_df[df_source_df["TradingDay"] == start]
            df_source_df_date = df_source_df_date.dropna()
            df_source_df_date.rename(columns = {"StockReturn": start}, inplace = True)
            return_factor = pd.DataFrame()
            for factor_name in factor_name_list:

                # 对不同行业进行分层，再以基准行业权重对收益率加权平均
                # 计算一个时间段内一个因子的收益率（市值加权）
                return_factor_industry = pd.DataFrame()
                for industry in df_source_df_date["Industry"].unique():
                    # 计算一个时间段内一个因子的一个行业内的收益率（市值加权）
                    df_factor_industry = df_source_df_date[df_source_df_date["Industry"] == industry]
                    temp = df_factor_industry.groupby(["layer_" + factor_name]).apply(
                        lambda x: np.average(x[start], weights=x["MV"], axis=0))
                    temp = temp.to_frame(name = start)
                    temp = temp.assign(Industry = industry)
                    return_factor_industry = pd.concat([return_factor_industry, temp])

                return_factor_industry["layer_" + factor_name] = return_factor_industry.index
                return_factor_industry = pd.merge(return_factor_industry, BenchmarkWeight_sum, on = ["Industry"], how = "left")
                return_factor_industry["Industry_weight"].fillna(0, inplace = True)
                temp_2 = return_factor_industry.groupby(["layer_"+factor_name]).apply(
                        lambda x: np.average(x[start], weights=x["Industry_weight"], axis=0))
                temp_2 = temp_2.to_frame(name = start)
                temp_2 = temp_2.assign(Factor = factor_name)
                return_factor = pd.concat([return_factor, temp_2], sort=False)
                return_factor["layer"] = return_factor.index
            if i == 0:
                result_df = return_factor
            else:
                result_df = pd.merge(result_df, return_factor, on = ["Factor", "layer"], how = "left")

            '''
                # 不对不同行业进行分层，对全部样本直接分层，以市值对收益率加权平均或等权平均
                # 计算一个时间段内一个因子的收益率（等权平均）
                temp = df_source_df_date.groupby(["layer_" + factor_name]).apply(lambda x: np.mean(x[start])) # 等权平均
                temp = temp.to_frame(name = start)
                temp = temp.assign(Factor=factor_name)
                return_factor = pd.concat([return_factor, temp])
            return_factor["layer"] = return_factor.index
            if i == 0:
                result_df = return_factor
            else:
                result_df = pd.merge(result_df, return_factor, on = ["Factor", "layer"], how = "left")
            '''

        return result_df
    def MaxDrawdown(self, df):
        """
        计算最大回撤
        """
        maxdrawdown = pd.Series(index=df.columns)
        net_value = df.add(1)  # 组合净值时间序列
        for i in df.columns: #range(1, df.shape[1] + 1):
            begin = np.argmax(np.maximum.accumulate(net_value[i]) - net_value[i])  # 回撤结束时间点
            end = np.argmax(net_value[i][: begin])  # 回撤开始的时间点
            maxdrawdown.loc[i] = format((net_value[i][end] - net_value[i][begin]) / net_value[i][begin], '.2%')
        return maxdrawdown
    def cal_rankic(self, df_factor):
        rankic_list = []
        factor_rankic_df = pd.DataFrame(index = [0], columns=["rankic_mean", "rankic_pct"])
        date_list = df_factor.index
        for date in date_list:
            temp = df_factor.loc[date].to_frame()
            temp = temp.assign(layer = temp.index)
            temp.index = range(len(temp))
            rankic_date = temp.corr(method = "spearman").values[0,1]
            rankic_list.append(rankic_date)
        rankic_abs_mean = np.nanmean(np.abs(rankic_list))
        rankic_mean = np.nanmean(rankic_list)
        rankic_pct = (np.abs(rankic_list) >= 0.5).sum() / len(rankic_list)
        factor_rankic_df["rankic_abs_mean"] = rankic_abs_mean
        factor_rankic_df["rankic_mean"] = rankic_mean
        factor_rankic_df["rankic_pct"] = rankic_pct
        return factor_rankic_df
    def factor_estimate_and_show_plot_layer(self, df_source_df):
        plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
        plt.rcParams['axes.unicode_minus'] = False
        if self.sample == "A" or self.sample == "HS300":
            benchmark_name = "HS300"
        elif self.sample == "ZZ500":
            benchmark_name = "ZZ500"
        layer_return = self.cal_layer_return(df_source_df, benchmark_name)
        col_name = layer_return.columns.tolist()
        col_name.remove("Factor")
        col_name.remove("layer")
        rf = 0.02  # 无风险年利率
        factor_name_list = ['Size', 'Beta', 'Momentum', 'Btop', 'Growth', 'Liquidity', 'Leverage',
                            'Midcapitalization', 'Residualvolatility', 'Earningsyield']  # A股十大风格因子
        performance = pd.DataFrame()
        for factor_name in factor_name_list:
            df_factor = layer_return[layer_return["Factor"] == factor_name]
            df_factor.index = df_factor["layer"]
            df_factor.sort_index(inplace = True)
            df_factor = df_factor[col_name].T #columns为层数,index为时间序列

            excess_return = df_factor.sub(rf / 12)
            performance_factor = pd.DataFrame(index=df_factor.columns)
            performance_factor['年化收益率'] = df_factor.mean().mul(12).apply(lambda x: format(x, '.2%'))
            performance_factor['年化波动率'] = df_factor.std().mul(math.sqrt(12)).apply(lambda x: format(x, '.2%'))
            performance_factor['夏普比率'] = df_factor.mean().mul(12).sub(rf).div(
                                    df_factor.std().mul(math.sqrt(12))).apply(lambda x: format(x, '.3'))
            performance_factor['最大回测'] = self.MaxDrawdown(df_factor)
            performance_factor['年化超额收益率'] = df_factor.mean().mul(12).sub(rf).apply(lambda x: format(x, '.2%'))
            performance_factor['年化超额收益波动率'] = excess_return.std().mul(math.sqrt(12)).apply(lambda x: format(x, '.2%'))
            # print(factor_name)
            # print(performance_factor)

            # 分层曲线图
            plt.figure('fig', figsize=(9, 4))
            for k in df_factor.columns:
                plt.plot(df_factor.index, df_factor[k].cumsum(), label= int(k) )
            plt.legend(loc='upper left')
            plt.xlabel("时间")
            plt.ylabel("分层收益率累计")
            plt.title(factor_name + ': 分层组合收益率累计曲线')
            plt.show()

            # rankic和多空组合
            performance_temp = self.cal_rankic(df_factor)
            k_min = df_factor.columns.min()
            k_max = df_factor.columns.max()
            long_short_port = df_factor[k_min] - df_factor[k_max]
            performance_temp["多空组合年化收益率"] = long_short_port.mean()*12
            performance_temp["多空组合年化波动率"] = long_short_port.std()*math.sqrt(12)
            performance_temp["多空组合夏普比率"] = (long_short_port.mean()*12-rf)/(long_short_port.std()*math.sqrt(12))
            performance_temp.index = [factor_name]
            performance = pd.concat([performance, performance_temp], sort=False)

            plt.figure('fig', figsize=(9, 4))
            ax1 = plt.subplot(111)
            ax1.bar(long_short_port.index, long_short_port.values, width=16, color='b')
            ax11 = ax1.twinx()
            ax11.plot(long_short_port.index, long_short_port.cumsum(), color='r')
            plt.title(factor_name + ' : 多空组合月收益率和收益率累计曲线（右轴）')
            plt.xlabel('time')
            ax1.set_xlabel("时间")
            ax1.set_ylabel('月收益率')
            ax11.set_ylabel('收益率累计')
            plt.show()
        print(performance[["rankic_abs_mean", "rankic_pct", "多空组合年化收益率"]])
        return performance
    def factor_estimate_and_show_plot_regression(self, data_source_df):
        # 因子有效性评价以及作图
        factor_name_list = ['Size', 'Beta', 'Momentum', 'Btop', 'Growth', 'Liquidity', 'Leverage',
                            'Midcapitalization', 'Residualvolatility', 'Earningsyield']  # A股十大风格因子
        data_source_df.index = data_source_df['TradingDay']

        estimate_df = pd.DataFrame()
        for factor_name in factor_name_list:
            # 因子名称
            temp_dict = dict()
            df_t = data_source_df[factor_name + '_t']
            df_coef = data_source_df[factor_name + '_coef']
            df_ic = data_source_df[factor_name + '_ic']
            temp_dict['因子'] = factor_name

            # T值绝对值均值
            factor_t_mean_abs = np.mean(np.abs(df_t))
            temp_dict['T值绝对值均值'] = factor_t_mean_abs
            # t值标准差
            factor_t_std = np.std(df_t)
            temp_dict['T值标准差'] = factor_t_std
            # 比值
            factor_t_ir = np.mean(np.abs(df_t)) / df_t.std()
            temp_dict['T均值/T值标准差'] = factor_t_ir
            # t值绝对值大于2占比
            factor_t_percent = np.nansum([df_t >= 2.0, df_t <= -2.0]) / len(df_t)
            temp_dict['T值绝对值大于2占比'] = factor_t_percent
            # # 因子年化收益率
            factor_mean_coef = df_coef.mul(12).mean()
            temp_dict['因子年化收益率'] = factor_mean_coef
            # 因子收益率序列t检验
            factor_retrun_series_t = df_coef.mean() * np.sqrt(len(df_coef) - 1) / df_coef.std()
            temp_dict['因子收益率序列t检验'] = factor_retrun_series_t
            # 因子IC均值
            factor_ic_mean = np.mean(df_ic)
            # 因子IC标准差
            factor_ic_std = np.std(df_ic)
            # 因子IR
            factor_ir = np.mean(df_ic)/np.std(df_ic)
            # 因子IC胜率
            factor_ic_win = (df_ic > 0).sum() / df_ic.count()
            temp_dict['IC均值'] = factor_ic_mean
            temp_dict['IC标准差'] = factor_ic_std
            temp_dict['IR比率'] = factor_ir
            temp_dict['IC胜率'] = factor_ic_win
            temp_dict['IC序列'] = df_ic.tolist()
            estimate_df = estimate_df.append(temp_dict, ignore_index=True)

            # 绘图
            # plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
            # plt.rcParams['axes.unicode_minus'] = False
            # plt.figure('fig', figsize=(9, 4))
            # ax1 = plt.subplot(111)
            # ax1.bar(df_coef.index, df_coef.values, width=16, color='b')
            # ax11 = ax1.twinx()
            # ax11.plot(df_coef.index, df_coef.cumsum(), color='r')
            # title = factor_name + ' : 因子月收益率和收益率累计曲线（右轴）'
            # plt.title(title)
            # ax1.set_xlabel("时间")
            # ax1.set_ylabel('月收益率')
            # ax11.set_ylabel('收益率累计')
            # plt.show()
        estimate_df = estimate_df.set_index("因子")
        print(estimate_df[["T值绝对值均值", "T值绝对值大于2占比", "IC均值", "IC胜率"]])
        return estimate_df




    def output2(self):
        """
        计算因子稳定系数，只需要t期和t+1期的因子暴露度
        """
        factor_name_list = ['Size', 'Beta', 'Momentum', 'Btop', 'Growth', 'Liquidity', 'Leverage',
                            'Midcapitalization', 'Residualvolatility', 'Earningsyield']  # A股十大风格因子
        trading_day_monthend = self.get_trading_days_monthend()
        date_list = sorted(trading_day_monthend["TradingDate"].tolist())
        data_t0 = pd.DataFrame()
        data_t1 = pd.DataFrame()
        df_factor_stab_coef = pd.DataFrame(index = date_list[1:], columns = factor_name_list)
        for i in range(len(date_list) - 1):
            trading_day_t0_datetime = date_list[i]
            trading_day_t0 = self.convert_datetime_to_string(trading_day_t0_datetime)
            trading_day_t1_datetime = date_list[i+1]
            trading_day_t1 = self.convert_datetime_to_string(trading_day_t1_datetime)
            if data_t1.empty:
                data_t0 = self.get_data_from_barra(trading_day_t0)
            else:
                data_t0 = data_t1.copy()
            data_t1 = self.get_data_from_barra(trading_day_t1)
            secuCode_list_t0 = data_t0["SecuCode"].tolist()
            secuCode_list_t1 = data_t1["SecuCode"].tolist()
            secuCode_list = list(set(secuCode_list_t0).intersection(set(secuCode_list_t1)))
            data_t0_copy = data_t0.copy()
            data_t1_copy = data_t1.copy()
            data_t0_copy = data_t0_copy.set_index("SecuCode")
            data_t1_copy = data_t1_copy.set_index("SecuCode")
            for factor_name in factor_name_list:
                x_t0 = data_t0_copy[factor_name].loc[secuCode_list]
                x_t1 = data_t1_copy[factor_name].loc[secuCode_list]
                factor_stab_coef = np.cov(x_t0, x_t1)[1,0]/(np.std(x_t0)*np.std(x_t1))
                df_factor_stab_coef[factor_name][trading_day_t1_datetime] = factor_stab_coef
        print(df_factor_stab_coef.mean())
        return df_factor_stab_coef
    def output(self, layer):
        data_source_df_reg, data_source_df_layer, data_source_df_vif = self.calculate_timeseries(layer)
        result_layer = self.factor_estimate_and_show_plot_layer(data_source_df_layer)
        result_regression = self.factor_estimate_and_show_plot_regression(data_source_df_reg)
        if self.sample == "A":
            result_ic_corr = self.cal_ic_corr(result_regression["IC序列"].to_frame())
            print("VIF")
            print(data_source_df_vif.mean())
    def cal_ic_corr(self, ic_df):
        plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
        plt.rcParams['axes.unicode_minus'] = False
        ic = []
        for factor_name in ic_df.index:
            ic.append(ic_df["IC序列"][factor_name])
        ic = pd.DataFrame(ic, index=ic_df.index).T
        result = ic.corr(method="spearman")
        fig, ax = plt.subplots(figsize=(10, 10))
        sns.heatmap(result, annot = True, vmax = 1, square = True, cmap = "Blues", linewidths = .5, ax = ax)
        plt.show()
        return result
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str
    def get_trading_days_monthend(self):
        # 获取交易时间
        start = self.start
        end = self.end
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate from QT_TradingDayNew where SecuMarket = 83 and IfMonthEnd  = 1 and
                                        TradingDate >= DATE('{start}') and TradingDate <= DATE('{end}') order by  
                                        TradingDate;""".format(start=start, end=end)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.index = range(len(trading_day_df))
        return trading_day_df
    def get_trading_day(self, start, end):
        """
        获取交易日
        :param start: 开始时间string
        :param end: 结束时间string
        :return: 交易日列表，日期为pd.Timestamp
        """
        if start == None:
            start_datetime = self.convert_string_to_datetime(end) - pd.DateOffset(years=1)
            start = self.convert_datetime_to_string(start_datetime)
        if end == None:
            end_datetime = self.convert_string_to_datetime(start) + pd.DateOffset(years=1)
            end = self.convert_datetime_to_string(end_datetime)
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate  from QT_TradingDayNew where SecuMarket = 83 and IfTradingDay  = 1 and
                            TradingDate >= DATE('{start}') and TradingDate <= DATE('{end}') order by TradingDate;""".format(
            start=start, end=end)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.rename(columns = {"TradingDate": "TradingDay"}, inplace = True)
        trading_day_list = sorted(trading_day_df["TradingDay"].tolist())
        return trading_day_list
    def get_trading_day_last_monthend(self, date = None):
        """
        获取date之前的最近一个月末交易日
        """
        end = date
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select max(TradingDate) from QT_TradingDayNew where SecuMarket = 83 and IfMonthEnd  = 1 and
                                   TradingDate <= DATE('{end}');""".format(end=end)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_monthend = trading_day_df["max(TradingDate)"][0]
        return trading_day_monthend
    def get_trading_day_next_monthend(self, date = None):
        """
        获取date之后的最近一个月末交易日
        """
        start = date
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select min(TradingDate) from QT_TradingDayNew where SecuMarket = 83 and IfMonthEnd  = 1 and
                                   TradingDate >= DATE('{start}');""".format(start=start)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_monthend = trading_day_df["min(TradingDate)"][0]
        return trading_day_monthend
    def get_data_from_barra_temp(self, trading_day):
        """
        从db_etf.A_Barra_Exposure_all_temp中获取交易日、股票代码（6位数string）、未来一个月收益率、市值、Barra10个大类因子、行业（一列）、国家因子
        因子暴露度数据为未进行正交化数据，使用一次后不再使用该函数，改用get_data_from_barra
        """
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="db_etf", charset="utf8mb4")
        # sql_string = "select * from A_Barra_Exposure_all_temp where Trading_Day = Date('{trading_day}')".format(
        #               trading_day=trading_day)
        sql_string = "select * from A_Barra_Exposure_all_temp_delete where Trading_Day = Date('{trading_day}')".format(
            trading_day=trading_day) # delete表中的StockReturn为月度数据，而未带delete的表中的StockReturn为日度数据
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        data_barra = pd.read_sql(sql_stmt, engine)
        data_barra.rename(columns={"Trading_Day": "TradingDay"}, inplace=True)
        data_barra.index = range(len(data_barra))
        if data_barra.empty:
            return pd.DataFrame()
        return data_barra
    def get_data_from_barra(self, trading_day):
        """
        根据self.sample从相关表中获取交易日、股票代码（6位数string）、未来一个月收益率、市值、Barra10个大类因子、行业（一列）、国家因子
            self.sample == "A" --> 全部市场 --> A_Barra_Exposure_all
            self.sample == "HS300" --> HS300成分股 --> A_Barra_Exposure_HS300
            self.sample == "ZZ500" --> 中证500 --> A_Barra_Exposure_ZZ500
        """
        if self.sample == "A":
            # table_name = "A_Barra_Exposure_all"
            table_name = "A_Barra_Exposure_all_delete"
        elif self.sample == "HS300":
            # table_name = "A_Barra_Exposure_HS300"
            table_name = "A_Barra_Exposure_HS300_delete"
        elif self.sample == "ZZ500":
            # table_name = "A_Barra_Exposure_ZZ500"
            table_name = "A_Barra_Exposure_ZZ500_delete"
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="db_etf", charset="utf8mb4")
        sql_string = "select * from " +  table_name + " where Trading_Day = Date('{trading_day}')".format(
                      trading_day=trading_day)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        data_barra = pd.read_sql(sql_stmt, engine)
        data_barra.rename(columns={"Trading_Day": "TradingDay"}, inplace=True)
        data_barra.index = range(len(data_barra))
        if data_barra.empty:
            return pd.DataFrame()
        return data_barra
    def convert_dailyreturn_to_monthreturn(self, date):
        """
        由于新的暴露度的table中的StockReturn为日度数据，但是测试因子显著性和计算IC值时需要用月度收益率，计算未来一个月的月度收益率
        """
        pass



if __name__=="__main__":
    start = "2014-01-01"
    end = "2018-12-31"
    # sample = "A"
    sample = "HS300"
    # sample = "ZZ500"
    obj = FactorTest(start, end, sample)
    layer = 5
    obj.output(layer)
    # obj.output2()

