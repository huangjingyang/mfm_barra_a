#-*- coding: utf-8 -*-
"""
@author: Huang, Jingyang
@license: Copyright Reserved by Futu
@contact: jgyghuang@qq.com
@site: http://www.futu5.com
@software: PyCharm
@file: SaveDB.py
@time: 2019/04/04
"""
import pandas as pd
import numpy as np
import math
import pymysql.cursors
import configparser
from ReturnModel import ReturnModel
from DataProcess import DataProcess
import datetime
from sqlalchemy import create_engine, text

class SaveDB():
    # 保存处理后的因子暴露度、因子收益率、个股残差收益到数据库
    def __init__(self):
        pass

    def save_data_to_db(self, table_name, data_df):
        # 决定存储的DB
        config = configparser.ConfigParser()
        save_db = "db_etf"
        connection = pymysql.connect(host='172.28.249.6',
                                     port=13357,
                                     user='stock_writer',
                                     password='stock_writer@',
                                     db=save_db,
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
        try:
            with connection.cursor() as cursor:
                for k in range(data_df.shape[0]):
                    data_df = data_df.round(6)
                    columns = data_df.columns
                    sql = "REPLACE INTO {table_name} (`".format(table_name=table_name)
                    sql += '`,`'.join(columns.values) + "`) " + "VALUES" + " ("
                    for i in range(len(columns)):
                        column_name = columns[i]
                        value = data_df[column_name][k]
                        if column_name == "Trading_Day":
                            sql += "Date('" + str(value) + "')"
                            sql += ","
                        elif column_name == "SecuCode":
                            sql += "'" + str(value) + "'"
                            sql += ","
                        elif column_name == "Factors":
                            sql += "'" + str(value) + "'"
                            sql += ","
                        elif column_name == "Industry":
                            sql += "'" + str(value) + "'"
                            sql += ","
                        else:
                            if value == None or value == np.nan or math.isnan(float(value)):
                                sql += 'NULL'
                                sql += ","
                            else:
                                if isinstance(value, int) or isinstance(value, float):
                                    value = round(float(value),6)
                                sql += "'"+str(value) +  "'"
                                sql += ","
                    sql = sql.rstrip(',')
                    sql += ");"
                    #print(sql)
                    cursor.execute(sql)
                connection.commit()
        except Exception as err:
            print(str(err))
            raise
        finally:
            connection.close()


    def save(self, table_name, date):
        if table_name == "A_Barra_Exposure_all_temp":
            # dataprocess.output()中位process_temp1
            sample = "A"
            dataprocess = DataProcess(date, sample)
            data_df = dataprocess.output()
            self.save_data_to_db(table_name, data_df)
        elif table_name == "A_Barra_Exposure_all":
            # dataprocess.output()中位process_temp2
            # 2014年之前、2018年之后为正常的process
            sample = None
            dataprocess = DataProcess(date, sample)
            data_df = dataprocess.output()
            self.save_data_to_db(table_name, data_df)
        elif table_name == "A_Barra_Exposure_HS300":
            sample = "HS300"
            dataprocess = DataProcess(date, sample)
            data_df = dataprocess.output()
            self.save_data_to_db(table_name, data_df)
        elif table_name == "A_Barra_Exposure_ZZ500":
            sample = "ZZ500"
            dataprocess = DataProcess(date, sample)
            data_df = dataprocess.output()
            self.save_data_to_db(table_name, data_df)
        elif table_name in ("A_Barra_Factor_Return_HS300", "A_Barra_Residual_Return_HS300"):
            sample = "HS300"
            returnmodel = ReturnModel(None, None, sample)
            factor_return, residual_return = returnmodel.output("WLS", date)
            self.save_data_to_db("A_Barra_Factor_Return_HS300", factor_return)
            self.save_data_to_db("A_Barra_Residual_Return_HS300", residual_return)
        elif table_name in ("A_Barra_Factor_Return_ZZ500", "A_Barra_Residual_Return_ZZ500"):
            sample = "ZZ500"
            returnmodel = ReturnModel(None, None, sample)
            factor_return, residual_return = returnmodel.output("WLS", date)
            self.save_data_to_db("A_Barra_Factor_Return_ZZ500", factor_return)
            self.save_data_to_db("A_Barra_Residual_Return_ZZ500", residual_return)






if __name__=="__main__":
    start_date = "2010-02-02"
    end_date = "2014-01-29"
    engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
    engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                        db="jydb", charset="utf8mb4")
    # 月末交易日
    # sql_string = """select TradingDate from QT_TradingDayNew where SecuMarket = 83 and IfMonthEnd  = 1 and
    #                             TradingDate >= DATE('{start_date}') and TradingDate <= DATE('{end_date}') order by
    #                             TradingDate;""".format(start_date=start_date, end_date=end_date)

    # 交易日
    sql_string = """select TradingDate  from QT_TradingDayNew where SecuMarket = 83 and IfTradingDay  = 1 and
                                TradingDate >= DATE('{start_date}') and TradingDate <= DATE('{end_date}') order by 
                                TradingDate;""".format(start_date=start_date, end_date=end_date)
    sql_stmt = text(sql_string)
    engine = create_engine(engine_str)
    trading_day_df = pd.read_sql(sql_stmt, engine)
    trading_day_df.index = range(len(trading_day_df))
    trading_day_series = trading_day_df["TradingDate"]
    for i in range(len(trading_day_df)):
        date_datetime = trading_day_series[i]
        date = date_datetime.strftime('%Y-%m-%d')
        print(date)

        # 储存因子暴露度
        # 注意需要修改DataProcess中的output的相关代码
        # table_name = "A_Barra_Exposure_all_temp"
        # table_name = "A_Barra_Exposure_all"
        # table_name = "A_Barra_Exposure_HS300"
        # table_name = "A_Barra_Exposure_ZZ500"
        # obj = SaveDB()
        # obj.save(table_name, date)

        # 储存因子收益率和个股残差收益
        # table_name = "A_Barra_Factor_Return_HS300"
        # table_name = "A_Barra_Factor_Return_ZZ500"
        # obj = SaveDB()
        # obj.save(table_name, date)




