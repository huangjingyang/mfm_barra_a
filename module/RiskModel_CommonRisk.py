# -*- coding: utf-8 -*-
"""
@author: Huang, Jingyang
@license: Copyright Reserved by Futu
@contact: jgyghuang@qq.com
@site: http://www.futu5.com
@software: PyCharm
@file: RiskModel.py
@time: 2019/05/01
"""
import pandas as pd
import numpy as np
from numpy import linalg
import math
import datetime
from sqlalchemy import create_engine, text
from sklearn import linear_model
import json
import pymysql
import numba


class RiskModel_CommonRisk():
    # 当前只支持截面计算
    def __init__(self, start_date, end_date, sample):
        self.start_date = start_date
        self.end_date = end_date
        self.sample = sample
        self.sequence_factors = ["Country", "Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                 "Midcapitalization", "Residualvolatility", "Earningsyield",
                                 "ConstructionMaterials", "Machinery", "CommunicationEquipment", "Financials",
                                 "RealEstate", "IndustrialConglomerates", "MedicalHealth", "NonferrousMetals",
                                 "Chemicals", "HouseholdAppliances", "FoodBeverage", "Electronics",
                                 "Transportation", "LightManufacturing", "Utilities", "Textiles", "Agriculture",
                                 "CommercialTrade", "MineralIndustry", "FerrousMetals", "CateringTourism",
                                 "Steel", "Leisure", "ConstructionFurnishings", "ElectricalEquipment",
                                 "Defense", "Automobiles", "Communication", "Media", "NonBankFinancials",
                                 "TransportationEquipment", "CommunicationServices"]  # 1国家+10个风格+32个行业
        self.sequence_factors1 = ["Country", "Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                           "Midcapitalization", "Residualvolatility", "Earningsyield",
                                           "FerrousMetals", "CateringTourism", "ConstructionMaterials",
                                           "Machinery", "TransportationEquipment", "CommunicationEquipment",
                                           "CommunicationServices", "Financials", "RealEstate",
                                           "IndustrialConglomerates",
                                           "MedicalHealth", "NonferrousMetals", "Chemicals", "HouseholdAppliances",
                                           "FoodBeverage", "Electronics", "Transportation", "LightManufacturing",
                                           "Utilities", "Textiles", "Agriculture", "CommercialTrade",
                                           "MineralIndustry"]  # 2014年之前: 1国家+10个风格+23个行业
        self.sequence_factors2 = ["Country", "Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                           "Midcapitalization", "Residualvolatility", "Earningsyield",
                                           "Steel", "Leisure", "ConstructionMaterials", "ConstructionFurnishings",
                                           "Machinery", "ElectricalEquipment", "Defense", "Automobiles",
                                           "CommunicationEquipment", "Communication", "Media", "Financials",
                                           "NonBankFinancials", "RealEstate", "IndustrialConglomerates",
                                           "MedicalHealth", "NonferrousMetals", "Chemicals", "HouseholdAppliances",
                                           "FoodBeverage", "Electronics", "Transportation", "LightManufacturing",
                                           "Utilities", "Textiles", "Agriculture", "CommercialTrade",
                                           "MineralIndustry"]  # 2014年之后: 1国家+10个风格+28个行业


    def Factor_Return_Covariance(self, Factor_Return, params, trading_day):
        """
        步骤与Factor_Return_Covariance_plot_bias一致，但不输出实证结果，直接输出共同风险矩阵用于储存
        """
        # 参数设置
        T = len(Factor_Return)
        factor_corr_half_life = params["factor_corr_half_life"]
        param_corr_half_life = params["param_corr_half_life"]
        newey_west_corr_lags = params["newey_west_corr_lags"]
        factor_vol_half_life = params["factor_vol_half_life"]
        param_vol_half_life = params["param_vol_half_life"]
        newey_west_vol_lags = params["newey_west_vol_lags"]
        VRA_half_life = params["VRA_half_life"]
        param_VRA_half_life = params["param_VRA_half_life"]
        weight_half_life_vra = pd.Series(self.cal_half_life(VRA_half_life, param_VRA_half_life))
        weight_half_life_corr = pd.Series(self.cal_half_life(factor_corr_half_life, param_corr_half_life))
        weight_half_life_vol = pd.Series(self.cal_half_life(factor_vol_half_life, param_vol_half_life))

        # 1、进行半衰期指数加权调整和newey-west调整
        Factor_Return_corr = Factor_Return.iloc[-factor_corr_half_life:, :]
        # Factor_return_for_NW_corr = Factor_Return.copy()
        # Corr_NW = self.NW_corr2(Factor_return_for_NW_corr, weight_half_life_corr, newey_west_corr_lags)
        # Factor_return_for_NW_vol = Factor_Return.iloc[factor_corr_half_life - factor_vol_half_life: factor_corr_half_life]
        # Factor_return_for_NW_vol.reset_index(drop = True, inplace = True)
        # Vol_NW = self.NW_vol(Factor_return_for_NW_vol, weight_half_life_vol, newey_west_vol_lags)
        # Cov_NW = self.covariance_from_vol_corr2(Corr_NW, Vol_NW) # Cov_NW:F_0
        # Cov_NW = self.NW_Cov_shichun(Factor_Return_corr, weight_half_life_corr, newey_west_corr_lags)  # Cov_NW:F_0
        Cov_NW = self.NW_Cov(Factor_Return_corr, weight_half_life_corr, newey_west_corr_lags)  # Cov_NW:F_0
        Cov_NW *= 21

        # 2、Eigenfactor Risk Adjustment
        EigVals, EigVecs = linalg.eig(Cov_NW) # EigVecs: U_0
        EigFacCov = np.diag((linalg.inv(EigVecs).dot(Cov_NW)).dot(EigVecs)) # EigFacCov: D_0 = inv(U_0) * F_0 * U_0

        # Monte Carlo simulation
        k = len(EigFacCov)
        Times_sim = 30  # 蒙特卡洛模拟次数M
        EigFacVol_sim = np.zeros((Times_sim, k))# np.zeros((Times_sim, len(EigFacCov)))
        EigFacVol_sim_bar = np.zeros((Times_sim, k))# np.zeros((Times_sim, len(EigFacCov)))

        for m in range(Times_sim):
            # EigFacRet_sim: b_m 模拟的eigenfactor的收益率
            EigFacRet_sim = np.zeros((k, factor_corr_half_life)) # np.zeros((len(EigFacCov), factor_corr_half_life))
            for n in range(k):
                if EigFacCov[n] > 0:
                    EigFacRet_sim[n] = np.random.normal(0, np.sqrt(EigFacCov[n]), factor_corr_half_life)  # 仿真的特征根因子收益率
                else:
                    EigFacRet_sim[n] = np.random.normal(0, np.sqrt(EigFacCov[n-1]), factor_corr_half_life)
                    print("<0")
            # FacRet_sim_for_corr: f_m = U_0 * b_m 模拟的原因子的收益率
            FacRet_sim_for_corr = (EigVecs.dot(EigFacRet_sim)).T
            FacRet_sim_for_corr = pd.DataFrame(FacRet_sim_for_corr)

            """
            # 分开计算corr和volatility，再合成covariance
            # corr_sim = Corr(f_m, f_m) 模拟的相关系数矩阵
            corr_sim = self.NW_corr2(FacRet_sim_for_corr, weight_half_life_corr, newey_west_corr_lags)
            # FacRet_sim_for_vol: 同为f_m，长度不一致，为了生成模拟的波动率对角矩阵
            FacRet_sim_for_vol = FacRet_sim_for_corr.iloc[0:factor_vol_half_life]
            FacRet_sim_for_vol.reset_index(drop = True, inplace = True)
            # vol_sim: 模拟的波动率对角矩阵
            vol_sim = self.NW_vol(FacRet_sim_for_vol, weight_half_life_vol, newey_west_vol_lags)
            # cov_sim: F_m = Cov(f_m, f_m) 模拟的因子收益率协方差矩阵
            cov_sim = self.covariance_from_vol_corr2(corr_sim, vol_sim)
            """

            # 直接计算covariance   cov_sim: F_m = Cov(f_m, f_m) 模拟的因子收益率协方差矩阵
            cov_sim = self.NW_Cov(FacRet_sim_for_corr, weight_half_life_corr, newey_west_corr_lags)

            # EigVecs_sim: 模拟因子收益率协方差矩阵F_m的特征向量矩阵：U_m
            EigVals_sim, EigVecs_sim = linalg.eig(cov_sim)
            # EigFacVol_sim: 模拟eigenfactor协方差矩阵 D_m = inv(U_m) * F_m * U_m
            EigFacVol_sim[m] = np.diag((linalg.inv(EigVecs_sim).dot(cov_sim)).dot(EigVecs_sim))
            # EigFacVol_sim_bar: 模拟eigenfactor协方差矩阵的真实值 D_m~ = inv(U_m) * F_0 * U_m
            EigFacVol_sim_bar[m] = np.diag((linalg.inv(EigVecs_sim).dot(Cov_NW)).dot(EigVecs_sim))

        # EigFacBias: v(k)
        EigFacBias = np.sqrt((EigFacVol_sim_bar / EigFacVol_sim).mean(axis=0))

        # 线性拟合
        y = pd.Series(EigFacBias)[:27] # 仅取前27个eigenfactor
        y = np.array(y)
        x = np.array([i for i in range(27)])  # x
        coef = np.polyfit(x, y, 2)  # np.polyfit(x, y, degree)
        p2 = np.poly1d(coef)
        # EigFacBias_fit: v_p(k)
        EigFacBias_fit = np.array([p2(i) for i in range(len(EigFacBias))])
        # EigFacBias_scaled: v_s(k) = a(v_p(k)-1) + 1, a = 1.4
        a = 1.4
        EigFacBias_scaled = a * (EigFacBias_fit - 1) + 1
        # EigFacCov_bar: D_0~ = v^2 * D_0
        EigFacCov_bar = np.diag(EigFacBias_scaled ** 2).dot(np.diag(EigFacCov))
        # Cov_EigFacAdj: 经过eigenfactor调整最终得到的协方差矩阵 F_0~ = U_0 * D_0~ * inv(U_0)
        Cov_EigFacAdj = (EigVecs.dot(EigFacCov_bar)).dot(np.linalg.inv(EigVecs))
        # 画图判断是否bias已经被消除，这也是确定a的方法

        # 3、Volatility Regime Adjustment
        BF = []
        # CSV = []
        for vra in range(VRA_half_life):
            # FactorRetforVRA = Factor_Return.iloc[
            #                   factor_corr_half_life - factor_vol_half_life - VRA_half_life + vra: factor_corr_half_life - VRA_half_life + vra]  # 长度为half_life_vol的长度，目的是计算vol
            FactorRetforVRA = Factor_Return.iloc[T - factor_vol_half_life - VRA_half_life + vra: T - VRA_half_life + vra]
            #vol_k = self.NW_vol(FactorRetforVRA, weight_half_life_vol, newey_west_vol_lags)
            vol_k = self.NW_vol(FactorRetforVRA, weight_half_life_vol, 0) # 不进行newey-west调整，但进行半衰期加权
            vol_k = np.diag(vol_k)
            facret_k = Factor_Return.iloc[T - VRA_half_life + vra]  # 因子收益率，比计算vol所用的时间+1期
            BF_each_period = math.sqrt(((facret_k / vol_k) ** 2).mean())
            BF.append(BF_each_period)
            # CSV_each_period = math.sqrt((facret_k ** 2).mean())*100
            # CSV.append(CSV_each_period)
        BF_square = np.array(BF) ** 2
        lambda_square = (weight_half_life_vra * BF_square).sum()
        # CSV = (weight_half_life_vra * CSV).sum()
        # 每期算出lambda_square和CSV，画出曲线，月度计算即可
        cov_new = Cov_EigFacAdj * lambda_square * (10 ** 6) # 扩大10**6次方，在后续组合优化中处理

        # 排序后储存进数据库
        factors_exist = Factor_Return.columns.tolist() # 已有的行业
        covariance = pd.DataFrame(cov_new, columns=factors_exist)
        factors_left = list(set(self.sequence_factors) - set(factors_exist))  # 缺少的行业
        covariance = pd.concat([covariance, pd.DataFrame(np.nan, columns=factors_left, index=covariance.index)],axis=1)
        covariance = pd.concat([covariance, pd.DataFrame(np.nan, columns=covariance.columns, index=factors_left)], axis=0)
        covariance.index = covariance.columns.tolist()
        covariance = covariance[self.sequence_factors]
        covariance = covariance.loc[self.sequence_factors]
        covariance.index = range(len(covariance))
        covariance.insert(loc = 0, column = 'Factors', value = self.sequence_factors)
        covariance.insert(loc = 0, column = 'Trading_Day', value = self.convert_string_to_datetime(trading_day))
        return covariance
    def Factor_Return_Covariance_plot_bias(self, Factor_Return_plus, params):
        """
        步骤与Factor_Return_Covariance一致，但用于输出实证研究结果
        """
        Factor_Return_last_day = Factor_Return_plus.iloc[-21:, :].sum() # 月度因子收益率->真实值
        Factor_Return = Factor_Return_plus.iloc[:-21, :]
        T = len(Factor_Return)
        # 参数设置
        factor_corr_half_life = params["factor_corr_half_life"]
        param_corr_half_life = params["param_corr_half_life"]
        newey_west_corr_lags = params["newey_west_corr_lags"]
        factor_vol_half_life = params["factor_vol_half_life"]
        param_vol_half_life = params["param_vol_half_life"]
        newey_west_vol_lags = params["newey_west_vol_lags"]
        VRA_half_life = params["VRA_half_life"]
        param_VRA_half_life = params["param_VRA_half_life"]
        weight_half_life_vra = pd.Series(self.cal_half_life(VRA_half_life, param_VRA_half_life))
        weight_half_life_corr = pd.Series(self.cal_half_life(factor_corr_half_life, param_corr_half_life))
        weight_half_life_vol = pd.Series(self.cal_half_life(factor_vol_half_life, param_vol_half_life))

        # 1、进行半衰期指数加权调整和newey-west调整
        Factor_Return_corr = Factor_Return.iloc[-factor_corr_half_life:, :]
        # Factor_return_for_NW_corr = Factor_Return.copy()
        # Corr_NW = self.NW_corr2(Factor_return_for_NW_corr, weight_half_life_corr, newey_west_corr_lags)
        # Factor_return_for_NW_vol = Factor_Return.iloc[factor_corr_half_life - factor_vol_half_life: factor_corr_half_life]
        # Factor_return_for_NW_vol.reset_index(drop = True, inplace = True)
        # Vol_NW = self.NW_vol(Factor_return_for_NW_vol, weight_half_life_vol, newey_west_vol_lags)
        # Cov_NW = self.covariance_from_vol_corr2(Corr_NW, Vol_NW) # Cov_NW:F_0
        # Cov_NW = self.NW_Cov_shichun(Factor_Return_corr, weight_half_life_corr, newey_west_corr_lags)  # Cov_NW:F_0
        Cov_NW = self.NW_Cov(Factor_Return_corr, weight_half_life_corr, newey_west_corr_lags)  # Cov_NW:F_0
        print(np.all(np.linalg.eigvals(Cov_NW) >= 0))
        Cov_NW *= 21
        factor_standard_return_opt_port_before_EigFacAdj = None # self.cal_bias_optimized_portfolio(Cov_NW)

        # 2、Eigenfactor Risk Adjustment
        EigVals, EigVecs = linalg.eig(Cov_NW)  # EigVecs: U_0
        EigFacCov = np.diag((linalg.inv(EigVecs).dot(Cov_NW)).dot(EigVecs))  # EigFacCov: D_0 = inv(U_0) * F_0 * U_0
        Factor_Return_last_day_before_EigFacAdj = Factor_Return_last_day.dot(EigVecs)
        factor_standard_return_before_EigFacAdj = Factor_Return_last_day_before_EigFacAdj / np.sqrt(EigFacCov)

        # Monte Carlo simulation
        k = len(EigFacCov)
        Times_sim = 30  # 蒙特卡洛模拟次数M
        EigFacVol_sim = np.zeros((Times_sim, k))  # np.zeros((Times_sim, len(EigFacCov)))
        EigFacVol_sim_bar = np.zeros((Times_sim, k))  # np.zeros((Times_sim, len(EigFacCov)))

        for m in range(Times_sim):
            # EigFacRet_sim: b_m 模拟的eigenfactor的收益率
            EigFacRet_sim = np.zeros((k, factor_corr_half_life))  # np.zeros((len(EigFacCov), factor_corr_half_life))
            for n in range(k):
                if EigFacCov[n] > 0:
                    EigFacRet_sim[n] = np.random.normal(0, np.sqrt(EigFacCov[n]), factor_corr_half_life)  # 仿真的特征根因子收益率
                else:
                    #EigFacRet_sim[n] = np.random.normal(0, 0, factor_corr_half_life)
                    EigFacRet_sim[n] = np.random.normal(0, np.sqrt(EigFacCov[n-1]), factor_corr_half_life)
                    print("<0")
            # FacRet_sim_for_corr: f_m = U_0 * b_m 模拟的原因子的收益率
            FacRet_sim_for_corr = (EigVecs.dot(EigFacRet_sim)).T
            FacRet_sim_for_corr = pd.DataFrame(FacRet_sim_for_corr)

            """
            # 分开计算corr和volatility，再合成covariance
            # corr_sim = Corr(f_m, f_m) 模拟的相关系数矩阵
            corr_sim = self.NW_corr2(FacRet_sim_for_corr, weight_half_life_corr, newey_west_corr_lags)
            # FacRet_sim_for_vol: 同为f_m，长度不一致，为了生成模拟的波动率对角矩阵
            FacRet_sim_for_vol = FacRet_sim_for_corr.iloc[0:factor_vol_half_life]
            FacRet_sim_for_vol.reset_index(drop = True, inplace = True)
            # vol_sim: 模拟的波动率对角矩阵
            vol_sim = self.NW_vol(FacRet_sim_for_vol, weight_half_life_vol, newey_west_vol_lags)
            # cov_sim: F_m = Cov(f_m, f_m) 模拟的因子收益率协方差矩阵
            cov_sim = self.covariance_from_vol_corr2(corr_sim, vol_sim)
            """

            # 直接计算covariance   cov_sim: F_m = Cov(f_m, f_m) 模拟的因子收益率协方差矩阵
            cov_sim = self.NW_Cov(FacRet_sim_for_corr, weight_half_life_corr, newey_west_corr_lags)

            # EigVecs_sim: 模拟因子收益率协方差矩阵F_m的特征向量矩阵：U_m
            EigVals_sim, EigVecs_sim = linalg.eig(cov_sim)
            # EigFacVol_sim: 模拟eigenfactor协方差矩阵 D_m = inv(U_m) * F_m * U_m
            EigFacVol_sim[m] = np.diag((linalg.inv(EigVecs_sim).dot(cov_sim)).dot(EigVecs_sim))
            # EigFacVol_sim_bar: 模拟eigenfactor协方差矩阵的真实值 D_m~ = inv(U_m) * F_0 * U_m
            EigFacVol_sim_bar[m] = np.diag((linalg.inv(EigVecs_sim).dot(Cov_NW)).dot(EigVecs_sim))

        # EigFacBias: v(k)
        EigFacBias = np.sqrt((EigFacVol_sim_bar / EigFacVol_sim).mean(axis=0))
        # 画出v(k) --> 最好计算好几期的再画均值，或者画出均值、1pct值、99pct值，显示其稳定性
        # self.plot_simulated_volatility_bias(EigFacBias)
        # 画出线性拟合和二次拟合的图做比较，仅一个截面
        # self.plot_simulated_volatility_bias_fit(EigFacBias)

        # 线性拟合
        y = pd.Series(EigFacBias)[:27]  # 仅取前27个eigenfactor
        y = np.array(y)
        x = np.array([i for i in range(27)])  # x
        coef = np.polyfit(x, y, 2)  # np.polyfit(x, y, degree)
        p2 = np.poly1d(coef)
        # EigFacBias_fit: v_p(k)
        EigFacBias_fit = np.array([p2(i) for i in range(len(EigFacBias))])
        # EigFacBias_scaled: v_s(k) = a(v_p(k)-1) + 1, a = 1.4
        a = 1.4 # 画图判断是否bias已经被消除，这也是确定a的方法
        EigFacBias_scaled = a * (EigFacBias_fit - 1) + 1
        # EigFacCov_bar: D_0~ = v^2 * D_0
        EigFacCov_bar = np.diag(EigFacBias_scaled ** 2).dot(np.diag(EigFacCov))
        # Cov_EigFacAdj: 经过eigenfactor调整最终得到的协方差矩阵 F_0~ = U_0 * D_0~ * inv(U_0)
        Cov_EigFacAdj = (EigVecs.dot(EigFacCov_bar)).dot(np.linalg.inv(EigVecs))
        Factor_Return_last_day_EigFacAdj = Factor_Return_last_day_before_EigFacAdj.copy()
        factor_standard_return_EigFacAdj = Factor_Return_last_day_EigFacAdj / np.sqrt(np.diag(EigFacCov_bar))
        factor_standard_return_opt_port_EigFacAdj = None # self.cal_bias_optimized_portfolio(Cov_EigFacAdj)

        # 3、Volatility Regime Adjustment
        BF = []
        CSV = []
        for vra in range(VRA_half_life):
            # FactorRetforVRA = Factor_Return.iloc[
            #                   factor_corr_half_life - factor_vol_half_life - VRA_half_life + vra: factor_corr_half_life - VRA_half_life + vra]  # 长度为half_life_vol的长度，目的是计算vol
            FactorRetforVRA = Factor_Return.iloc[T - factor_vol_half_life - VRA_half_life + vra: T - VRA_half_life + vra]
            #vol_k = self.NW_vol(FactorRetforVRA, weight_half_life_vol, newey_west_vol_lags)
            vol_k = self.NW_vol(FactorRetforVRA, weight_half_life_vol, 0) # 不进行newey-west调整，但进行半衰期加权
            vol_k = np.diag(vol_k)
            facret_k = Factor_Return.iloc[T - VRA_half_life + vra]  # 因子收益率，比计算vol所用的时间+1期
            BF_each_period = math.sqrt(((facret_k / vol_k) ** 2).mean())
            BF.append(BF_each_period)
            CSV_each_period = math.sqrt((facret_k ** 2).mean())*100
            CSV.append(CSV_each_period)
        BF_square = np.array(BF) ** 2
        multiplier_lambda = np.sqrt((weight_half_life_vra * BF_square).sum())
        CSV = (weight_half_life_vra * CSV).sum()
        # weight_half_life_vra_CSV = pd.Series(self.cal_half_life(42, 42))
        # CSV = (weight_half_life_vra_CSV * CSV[-42:]).sum()
        vol_k_adj = multiplier_lambda*vol_k # 最后一期的调整vol
        BF_each_period_adj = math.sqrt(((facret_k / vol_k_adj) ** 2).mean()) # 最后一期的调整vol bias
        return factor_standard_return_before_EigFacAdj, factor_standard_return_EigFacAdj,\
               factor_standard_return_opt_port_before_EigFacAdj, factor_standard_return_opt_port_EigFacAdj, \
               EigFacBias, multiplier_lambda, CSV, BF_each_period, BF_each_period_adj


    def output_plot_bias(self,):
        """
        :param Factor_return: 因子收益率，长度为factor_corr_half_life
        :return covariance: 当期的因子收益率协方差矩阵
        """
        # 参数设置
        covariance_params, specific_risk_params = self.params()
        # 获取相关数据
        num_days_need_factor_return = np.max((covariance_params["factor_corr_half_life"], covariance_params["factor_vol_half_life"]+covariance_params["VRA_half_life"])) + 21
        trading_day_df = self.get_trading_days_monthend()  # 月末交易日
        trading_day_series = trading_day_df["TradingDate"]
        # 计算bias
        b_before_adj_list = []
        b_adj_list = []
        b_opt_port_before_adj_list = []
        b_opt_port_adj_list = []
        EigFacBias = []
        multiplier_lambda = []
        CSV = []
        BF = []
        BF_adj = []
        for i in range(len(trading_day_df)):
            date_datetime = trading_day_series[i]
            date = self.convert_datetime_to_string(date_datetime)
            print(date)
            Factor_Return = self.get_data_factor_return(num_days_need_factor_return, date)
            b_before_adj_date, b_adj_date, b_opt_port_before_adj_date, b_opt_port_adj_date, EigFacBias_date, \
            multiplier_lambda_date, CSV_date,  BF_date, BF_adj_date\
                             = self.Factor_Return_Covariance_plot_bias(Factor_Return, covariance_params)
            b_before_adj_list.append(b_before_adj_date)
            b_adj_list.append(b_adj_date)
            b_opt_port_before_adj_list.append(b_opt_port_before_adj_date)
            b_opt_port_adj_list.append(b_opt_port_adj_date)
            EigFacBias.append(EigFacBias_date)
            multiplier_lambda.append(multiplier_lambda_date)
            CSV.append(CSV_date)
            BF.append(BF_date)
            BF_adj.append(BF_adj_date)
        b_before_adj_df = pd.DataFrame(b_before_adj_list)
        b_adj_df = pd.DataFrame(b_adj_list)
        b_opt_port_before_adj_df = pd.DataFrame(b_opt_port_before_adj_list)
        b_opt_port_adj_df = pd.DataFrame(b_opt_port_adj_list)
        EigFacBias_df = pd.DataFrame(EigFacBias)
        CSV_df = pd.DataFrame(CSV)
        multiplier_lambda_df = pd.DataFrame(multiplier_lambda)
        BF_df = pd.DataFrame(BF)
        BF_adj_df = pd.DataFrame(BF_adj)
        self.plot_bias_ts(b_before_adj_df)
        self.plot_bias_ts(b_adj_df)
        # self.plot_bias_ts(b_opt_port_before_adj_df)
        # self.plot_bias_ts(b_opt_port_adj_df)
        self.plot_lambda_csv(trading_day_series, CSV_df, multiplier_lambda_df)
        self.plot_regime_bias(trading_day_series, BF_df, BF_adj_df)
        self.plot_simulated_volatility_bias_ts(EigFacBias_df)
    def plot_bias_ts(self, factor_standard_return):
        import matplotlib.pyplot as plt
        B = factor_standard_return.std(skipna = True)
        plt.figure('fig', figsize=(9, 4))
        plt.scatter(B.index, B.values)
        T = len(factor_standard_return)
        upperline = 1-np.sqrt(2/T)
        lowerline = 1+np.sqrt(2/T)
        plt.axhline(y=upperline, color='black', linewidth=1, linestyle="--")
        plt.axhline(y=lowerline, color='black', linewidth=1, linestyle="--")
        plt.ylim((0.5, 2.5))
        plt.xlabel("Eigen Factor Number")
        plt.ylabel("Bias Statistics")
        plt.title("Eigenfactor Bias Statistics")
        plt.show()
    def plot_simulated_volatility_bias(self, EigFacBias):
        import matplotlib.pyplot as plt
        plt.figure('fig', figsize=(9, 4))
        plt.scatter(range(1, len(EigFacBias) + 1), EigFacBias)
        plt.plot(range(1, len(EigFacBias) + 1), EigFacBias)
        plt.xlabel("Eigenfactor Number")
        plt.ylabel("Simulated Volatility Bias")
        plt.axhline(y=1.0, color='black', linewidth=1, linestyle="--")
        plt.title("Simulated Volatility Bias")
        plt.show()
    def plot_simulated_volatility_bias_fit(self, EigFacBias):
        """
        仅画一个截面的拟合，比较线性拟合和二次拟合
        """
        import matplotlib.pyplot as plt
        plt.rcParams['font.sans-serif'] = ['SimHei']
        plt.rcParams['axes.unicode_minus'] = False
        y = pd.Series(EigFacBias)[:27]
        y = np.array(y)
        x = np.array([i for i in range(27)])
        coef1 = np.polyfit(x, y, 1)
        p1 = np.poly1d(coef1)
        coef2 = np.polyfit(x, y, 2)
        p2 = np.poly1d(coef2)
        xp = np.linspace(0, 30)
        plt.figure('fig', figsize=(6, 6))
        line1 = plt.plot(x, y, '.', label="Simulated Bias")
        line2 = plt.plot(xp, p1(xp), '-', label="Linear Fit")
        line3 = plt.plot(xp, p2(xp), '--', label="Parabola Fit")
        plt.legend(loc=2)
        plt.xlabel("Eigenfactor Number")
        plt.ylabel("Simulated Volatility Bias")
        plt.title("Simulated Volatility Bias Fit")
        plt.show()
    def plot_lambda_csv(self, trading_day_series, CSV_df, lambda_square_df):
        import matplotlib.pyplot as plt
        plt.figure('fig', figsize=(9, 5))
        plt.plot(trading_day_series, CSV_df, label="Factor CSV", color="gray", linewidth=3, linestyle="--")
        plt.plot(trading_day_series, lambda_square_df, label="Factor Volatility Multiplier", color="darkblue", linewidth=3)
        plt.ylabel("Factor Volatility Multiplier & Factor CSV(%)")
        plt.xlabel("Time")
        plt.axhline(y=1.0, color='black', linewidth=1, linestyle="--")
        plt.legend(loc=1)
        plt.title("Factor Volatility Multiplier & Factor CSV(%)")
        plt.show()
    def plot_simulated_volatility_bias_ts(self, EigFacBias_df):
        import matplotlib.pyplot as plt
        plt.figure('fig', figsize=(12, 12))
        for idx in EigFacBias_df.index:
            val = EigFacBias_df.loc[idx]
            k = len(val)
            plt.scatter(range(1, k + 1), val)
        plt.plot(range(1, k + 1), EigFacBias_df.mean(), label="mean", linewidth=3.0, color="darkblue")
        plt.axhline(y=1.0, color='black', linewidth=1, linestyle="--")
        plt.xlabel("Eigenfactor Number")
        plt.ylabel("Simulated Volatility Bias")
        plt.title("Simulated Volatility Bias")
        plt.show()
    def plot_regime_bias(self, trading_day_series, BF_df, BF_adj_df):
        import matplotlib.pyplot as plt
        plt.figure('fig', figsize=(9, 5))
        plt.plot(trading_day_series, BF_df.rolling(12).mean(), label="Unadjusted", color="grey", linestyle="--",
                 linewidth=3)
        plt.plot(trading_day_series, BF_adj_df.rolling(12).mean(), label="With Volatility Regime Adjustment",
                 color="darkblue", linewidth=3)
        plt.xlabel("Time")
        plt.ylabel("Mean Bias Statistic")
        plt.axhline(y=1.0, color='black', linewidth=1, linestyle="--")
        plt.title("Mean Bias Statistic")
        plt.legend(loc=0)
        plt.show()

    def cal_half_life(self, num_days, param_half_life):
        """
        半衰期权重
        """
        delta = 0.5 ** (1 / param_half_life)
        weight_list = [delta ** (num_days - k) for k in range(1, num_days + 1)]
        weight_list2 = list(weight_list / np.sum(weight_list))
        return weight_list2
    @numba.jit
    def demean(self, df, half_life_weight):
        """
        去均值
        """
        df_mean = (df.multiply(half_life_weight,0)).sum(axis = 0)
        df_demean = df - df_mean
        df_demean.reset_index(drop=True, inplace = True)
        return df_demean
    @numba.jit
    def NW_corr2(self, df, half_life_weight, newey_west_corr_lags):
        """
        !!!目前未使用该函数，协方差的调整是正确的，波动率的调整有误!!!
        计算newey-west调整相关系数
        :param df: 全部因子收益率时间序列: T by k
        :param half_life_weight: 半衰期权重: T by 1
        :param newey_west_corr_lags: 调整相关系数时的参数m
        :return: 经过调整的相关系数矩阵: k by k
        """
        df_demean1 = self.demean(df, half_life_weight)
        df_demean2 = self.demean(df, [1/len(df)]*len(df))
        corr = self.half_life_corr(df_demean1, half_life_weight, 0)
        k = len(corr)
        for m in range(1, newey_west_corr_lags + 1):
            weight_lag_m = 1 - m / (1 + newey_west_corr_lags)
            corr_m = self.half_life_corr(df_demean2, half_life_weight, m)
            corr += weight_lag_m * (corr_m + corr_m.T)
        return corr
    @numba.jit
    def half_life_corr(self, df, half_life_weight, m):
        """
        计算半衰期指数加权的相关系数，m为滞后调整项
        """
        factor_name_list = df.columns.tolist()
        k = len(factor_name_list)
        df_corr = pd.DataFrame(0, index=factor_name_list, columns=factor_name_list)
        # 计算上半个矩阵，不包括对角线
        for i in range(k):#range(k-1):
            factor_x = factor_name_list[i]
            ts_factor_x = df[factor_x]
            for j in range(i,k):#range(i+1, k):
                factor_y = factor_name_list[j]
                ts_factor_y = df[factor_y]
                corr = self.half_life_corr_two_factor(ts_factor_x, ts_factor_y, half_life_weight, m)
                df_corr[factor_x].loc[factor_y] = corr
        if m == 0:
            df_corr += df_corr.T # 下半个矩阵复制上半个矩阵
            df_corr.values[[range(k)] * 2] = 1
        else: # 计算下半个矩阵
            for i in range(1, k):
                factor_x = factor_name_list[i]
                ts_factor_x = df[factor_x]
                for j in range(i):
                    factor_y = factor_name_list[j]
                    ts_factor_y = df[factor_y]
                    corr = self.half_life_corr_two_factor(ts_factor_x, ts_factor_y, half_life_weight, m)
                    df_corr[factor_x].loc[factor_y] = corr
        return df_corr
    @numba.jit
    def half_life_corr_two_factor(self, ts_factor_x, ts_factor_y, half_life_weight, m):
        """
        计算两个因子之间的半衰期指数加权的相关系数，m为滞后调整项
        """
        T = len(ts_factor_x)
        # if m == 0:
        #     vol_factor_x = self.half_life_vol_single_factor(ts_factor_x, half_life_weight, 0)
        #     vol_factor_y = self.half_life_vol_single_factor(ts_factor_y, half_life_weight, 0)
        #     numerator = half_life_weight.T.dot(ts_factor_x * ts_factor_y)
        #     denominator = vol_factor_x * vol_factor_y
        # else:
        #     vol_factor_x = self.half_life_vol_single_factor(ts_factor_x, half_life_weight, 0)
        #     vol_factor_y = self.half_life_vol_single_factor(ts_factor_y, half_life_weight, m)
        #     ts_factor_x = ts_factor_x[:T - m].reset_index(drop=True)
        #     ts_factor_y = ts_factor_y[m:].reset_index(drop=True)
        #     numerator = (ts_factor_x * ts_factor_y).sum()/(T-1)
        #     denominator = vol_factor_x * vol_factor_y
        if m == 0:
            vol_factor_x = self.half_life_vol_single_factor(ts_factor_x, half_life_weight, 0)
            vol_factor_y = self.half_life_vol_single_factor(ts_factor_y, half_life_weight, 0)
            numerator = half_life_weight.T.dot(ts_factor_x * ts_factor_y)
        else:
            half_life_weight = pd.Series([])
            vol_factor_x = self.half_life_vol_single_factor(ts_factor_x, half_life_weight, 0)
            vol_factor_y = self.half_life_vol_single_factor(ts_factor_y, half_life_weight, m)
            ts_factor_x = ts_factor_x[:T - m].reset_index(drop=True)
            ts_factor_y = ts_factor_y[m:].reset_index(drop=True)
            numerator = (ts_factor_x * ts_factor_y).sum()/(T-1)
        denominator = vol_factor_x * vol_factor_y
        corr = numerator/denominator
        return corr
    @numba.jit
    def NW_vol(self, df, half_life_weight, newey_west_vol_lags):
        """
        !!!目前未使用该函数，协方差的调整是正确的，波动率的调整有误!!!
        !!!当newey_west_vol_lags=0时，可以用于计算不经newey-west调整但经过半衰期加权的波动率!!!
        计算newey-west调整的波动率
        :param df: 全部因子收益率时间序列: T by k
        :param half_life_weight: 半衰期权重: T by 1
        :param newey_west_vol_lags: 调整波动率时的参数m
        :return: 经过调整的波动率对角矩阵
        """
        df_demean1 = self.demean(df, half_life_weight)
        df_demean2 = self.demean(df, [1 / len(df)] * len(df))
        vol = self.half_life_vol(df_demean1, half_life_weight, 0)
        for m in range(1, newey_west_vol_lags + 1):
            weight_lag_m = 1 - m / (1 + newey_west_vol_lags)
            vol_m = self.half_life_vol(df_demean2, pd.Series([]), m)
            vol += weight_lag_m*vol_m
        return vol
    @numba.jit
    def half_life_vol(self, df, half_life_weight, m):
        """
        计算半衰期指数加权的波动率，m为滞后调整项
        """
        factor_name_list = df.columns.tolist()
        k = len(factor_name_list)
        df_volatility = pd.DataFrame(0, index = factor_name_list, columns = factor_name_list)
        for factor_name in factor_name_list:
            ts_factor_name = df[factor_name]
            vol = self.half_life_vol_single_factor(ts_factor_name, half_life_weight, m)
            df_volatility[factor_name].loc[factor_name] = vol
        return df_volatility
    @numba.jit
    def half_life_vol_single_factor(self, ts_factor_name, half_life_weight, m):
        """
        计算半衰期指数加权的单个因子的波动率，m为滞后调整项
        """
        T = len(ts_factor_name)
        # if m == 0:
        #     var = half_life_weight.T.dot(ts_factor_name ** 2)
        # else:
        #     ts_factor_name_1 = ts_factor_name[:T - m].reset_index(drop=True)
        #     ts_factor_name_2 = ts_factor_name[m:].reset_index(drop=True)
        #     half_life_weight_1 = half_life_weight[m:].reset_index(drop=True)
        #     var = half_life_weight_1.T.dot(ts_factor_name_2 ** 2)
        if half_life_weight.empty:
            ts_factor_name_2 = ts_factor_name[m:].reset_index(drop=True)
            var = (ts_factor_name_2 ** 2).sum() / (T - 1)
        else:
            ts_factor_name = ts_factor_name.reset_index(drop = True)
            var = half_life_weight.T.dot(ts_factor_name ** 2)
        vol = np.sqrt(var)
        return vol
    @numba.jit
    def covariance_from_vol_corr2(self, corr, vol):
        """
        Covariance_xy = Vol_x * Corr_xy * Vol_y
        将波动率和相关系数相乘，得到协方差矩阵
        其中波动率和相关系数可以是经过newey-west调整后的值，也可以是不经过调整的值，如果corr为1，即自身的相关系数，此函数算出的则为方差
        :param corr: 相关系数矩阵
        :param vol: 波动率对角矩阵
        :return: 协方差矩阵或者方差
        """
        cov = vol.dot(corr).dot(vol)
        return cov
    @numba.jit
    def gamma_func(self, df, m):
        T = len(df)
        tmp = np.zeros([len(df.columns), len(df.columns)])
        for t in range(0, T - m):
            tmp += np.outer(df.ix[t, :], df.ix[t + m, :])
        tmp /= (T - 1)
        return pd.DataFrame(tmp, index=df.columns, columns=df.columns)
    def NW_Cov_shichun(self, df, weight_half_life_cov, newey_west_cov_lags):
        """
        石川的计算方法
        """
        df_mean1 = self.demean(df, weight_half_life_cov)
        df_mean2 = self.demean(df, [1/len(df)]*len(df))
        factor_name_list = df_mean1.columns.tolist()
        k = len(factor_name_list)
        g0 = pd.DataFrame(0, index=factor_name_list, columns=factor_name_list)
        # 计算上半个矩阵，不包括对角线
        for i in  range(k - 1):
            factor_x = factor_name_list[i]
            ts_factor_x = df_mean1[factor_x].reset_index(drop=True)
            for j in range(i + 1, k):
                factor_y = factor_name_list[j]
                ts_factor_y = df_mean1[factor_y].reset_index(drop=True)
                cov = weight_half_life_cov.T.dot(ts_factor_x * ts_factor_y)
                g0[factor_x].loc[factor_y] = cov
        g0 += g0.T  # 下半个矩阵复制上半个矩阵
        g0.values[[range(k)] * 2] = [weight_half_life_cov.T.dot(df_mean1[factor_name_list[q]].reset_index(drop=True) * df_mean1[factor_name_list[q]].reset_index(drop=True)) for q in range(k)]

        #g0 = df.cov()
        for m in range(0, newey_west_cov_lags):
            m += 1
            gi = self.gamma_func(df_mean2, m)
            w = 1 - m / (1 + newey_west_cov_lags)
            g0 += w * (gi + gi.T)
        return g0
    @numba.jit
    def gamma_func_half_life(self, df, m, weight_half_life):
        # 当m==0时，即为没有滞后阶数的协方差
        T = len(df)
        tmp = np.zeros([len(df.columns), len(df.columns)])
        for t in range(0, T - m):
            tmp += np.outer(df.ix[t, :], df.ix[t + m, :])*weight_half_life[t]
        # tmp /= (T - 1) # 等权重
        return pd.DataFrame(tmp, index=df.columns, columns=df.columns)
    @numba.jit
    def NW_Cov(self, df, weight_half_life_cov, newey_west_cov_lags):
        """
        目前使用的计算方法，直接计算协方差而非拆分成波动率和相关系数矩阵
        """
        df = self.demean(df, weight_half_life_cov)
        g0 = self.gamma_func_half_life(df, 0, weight_half_life_cov)
        for m in range(0, newey_west_cov_lags):
            m += 1
            gi = self.gamma_func_half_life(df, m, weight_half_life_cov)
            w = 1 - m / (1 + newey_west_cov_lags)
            g0 += w * (gi + gi.T)
        return g0


    def params(self):
        covariance_params = pd.Series([252,90,5,252,90,2,42,42],
        index = [ "factor_vol_half_life","param_vol_half_life","newey_west_vol_lags","factor_corr_half_life", "param_corr_half_life", "newey_west_corr_lags",  "VRA_half_life", "param_VRA_half_life"])
        specific_risk_params = pd.Series([252,90,5,1,42,42],
        index = ["factor_vol_half_life", "param_vol_half_life", "newey_west_vol_lags", "bayesian_shrinkage_q", "VRA_half_life", "param_VRA_half_life"])
        return covariance_params, specific_risk_params
    def output(self, date):
        # 参数设置
        covariance_params, specific_risk_params = self.params()
        # 获取相关数据
        num_days_need_factor_return = np.max((covariance_params["factor_corr_half_life"], covariance_params["factor_vol_half_life"] + covariance_params["VRA_half_life"]))
        Factor_Return = self.get_data_factor_return(num_days_need_factor_return, date)
        # 计算一个截面的协方差矩阵
        Covariance = self.Factor_Return_Covariance(Factor_Return, covariance_params, date)
        return Covariance
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str
    def get_trading_days_days(self):
        """
        获取日度交易日，该时间与SaveDB中的日期一致
        """
        start_date = self.start_date
        end_date = self.end_date
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate  from QT_TradingDayNew where SecuMarket = 83 and IfTradingDay  = 1 and
                                        TradingDate >= DATE('{start_date}') and TradingDate <= DATE('{end_date}') order by 
                                        TradingDate;""".format(start_date=start_date, end_date=end_date)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.index = range(len(trading_day_df))
        return trading_day_df
    def get_trading_day(self, start, end):
        """
        获取交易日
        :param start: 开始时间string
        :param end: 结束时间string
        :return: 交易日列表，日期为pd.Timestamp
        """
        if start == None:
            start_datetime = self.convert_string_to_datetime(end) - pd.DateOffset(years=1)
            start = self.convert_datetime_to_string(start_datetime)
        if end == None:
            end_datetime = self.convert_string_to_datetime(start) + pd.DateOffset(years=1)
            end = self.convert_datetime_to_string(end_datetime)
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate  from QT_TradingDayNew where SecuMarket = 83 and IfTradingDay  = 1 and
                            TradingDate >= DATE('{start}') and TradingDate <= DATE('{end}') order by TradingDate;""".format(
            start=start, end=end)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.rename(columns = {"TradingDate": "TradingDay"}, inplace = True)
        trading_day_list = sorted(trading_day_df["TradingDay"].tolist())
        return trading_day_list
    def get_data_factor_return(self, num_days_need_factor_return, trading_day):
        num_day_need_max = int(num_days_need_factor_return)
        end_date = trading_day
        if num_day_need_max < 250:
            trading_day_df = self.get_trading_day(start = None, end = trading_day)
        else:
            temp_datetime = self.convert_string_to_datetime(trading_day) - pd.DateOffset(days=2 * num_day_need_max)
            temp_date = self.convert_datetime_to_string(temp_datetime)
            trading_day_df = self.get_trading_day(start=temp_date, end=trading_day)
        start_date_datetime_factor_return = trading_day_df[-num_days_need_factor_return]
        start_date_factor_return = self.convert_datetime_to_string(start_date_datetime_factor_return)
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="db_etf", charset="utf8mb4")
        engine = create_engine(engine_str)
        if self.sample == "HS300":
            table_name_factor_return = "A_Barra_Factor_Return_HS300"
        elif self.sample == "ZZ500":
            table_name_factor_return = "A_Barra_Factor_Return_ZZ500"
        # 因子收益率时间序列
        sql_string_factor_return = "select * from " + table_name_factor_return + " where Trading_Day >= " \
                                "Date('{start_date_factor_return}') and Trading_Day <= Date('{end_date}')" \
                                " order by Trading_Day;".format(
                                end_date=end_date, start_date_factor_return=start_date_factor_return)
        sql_stmt_factor_return = text(sql_string_factor_return)
        Barra_Factor_Return= pd.read_sql(sql_stmt_factor_return, engine)
        Barra_Factor_Return.drop(["Create_Time", "Trading_Day"], axis=1, inplace=True)

        # 剔除行业因子收益率缺失严重的因子（缺失数量大于1/3），同时在因子暴露度中也剔除相应的因子，放在后面处理
        # 或者以平均值填充缺失的收益率，第一个时间截面缺失的填充为零
        if int(trading_day[:4]) >= 2014:
            Barra_Factor_Return = Barra_Factor_Return[self.sequence_factors2]
        else:
            Barra_Factor_Return = Barra_Factor_Return[self.sequence_factors1]
        null_count = Barra_Factor_Return.isnull().sum()
        factor_delete_count = null_count.index[null_count > num_days_need_factor_return / 3].tolist()
        factor_keep_count = list(set(null_count.index[null_count > 0].tolist())-set(factor_delete_count))
        Barra_Factor_Return.drop(factor_delete_count, axis=1, inplace=True)
        for factor_keep in factor_keep_count:
            index_fillna = Barra_Factor_Return.index[Barra_Factor_Return[factor_keep].isnull()]
            for idx in index_fillna:
                factor_return_before = Barra_Factor_Return[factor_keep][:idx].dropna()
                Barra_Factor_Return[factor_keep][idx] = factor_return_before.mean()
        Barra_Factor_Return = Barra_Factor_Return.fillna(0)
        print(factor_delete_count)
        return Barra_Factor_Return
    def get_data_exposure(self, trading_day):
        end_date = trading_day
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="db_etf", charset="utf8mb4")
        engine = create_engine(engine_str)
        if self.sample == "HS300":
            table_name_exposure = "A_Barra_Exposure_HS300"
        elif self.sample == "ZZ500":
            table_name_exposure = "A_Barra_Exposure_ZZ500"
        # end_date时间截面个股暴露度
        sql_string_factor_exposure = "select * from " + table_name_exposure + " where Trading_Day = " \
                                     "Date('{end_date}') order by SecuCode;".format(
                                     end_date=end_date)
        sql_stmt_factor_exposure = text(sql_string_factor_exposure)
        Barra_Exposure = pd.read_sql(sql_stmt_factor_exposure, engine)
        Barra_Exposure.drop(["Create_Time", "Trading_Day"], axis=1, inplace=True)
        return Barra_Exposure
    def get_data_residual_return(self, num_days_need_residual_return, trading_day):
        num_day_need_max = int(num_days_need_residual_return)
        end_date = trading_day
        if num_day_need_max < 250:
            trading_day_df = self.get_trading_day(start=None, end=trading_day)
        else:
            temp_datetime = self.convert_string_to_datetime(trading_day) - pd.DateOffset(days=2 * num_day_need_max)
            temp_date = self.convert_datetime_to_string(temp_datetime)
            trading_day_df = self.get_trading_day(start=temp_date, end=trading_day)
        start_date_datetime_residual_return = trading_day_df[-num_days_need_residual_return]
        start_date_residual_return = self.convert_datetime_to_string(start_date_datetime_residual_return)
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="db_etf", charset="utf8mb4")
        engine = create_engine(engine_str)
        if self.sample == "HS300":
            table_name_residual_return = "A_Barra_Residual_Return_HS300"
        elif self.sample == "ZZ500":
            table_name_residual_return = "A_Barra_Residual_Return_ZZ500"

        # 个股残差收益率时间序列
        sql_string_residual_return = "select * from " + table_name_residual_return + " where Trading_Day >= " \
                                     "Date('{start_date_residual_return}') and Trading_Day <= Date('{end_date}') order by " \
                                     "Trading_Day, SecuCode;".format(
                                     end_date=end_date, start_date_residual_return=start_date_residual_return)
        sql_stmt_residual_return = text(sql_string_residual_return)
        Barra_Residual_Return = pd.read_sql(sql_stmt_residual_return, engine)
        Barra_Residual_Return.drop(["Create_Time"], axis=1, inplace=True)
        Barra_Residual_Return_pivot = Barra_Residual_Return.pivot(index="Trading_Day", columns="SecuCode",
                                                                  values="Residual_return")
        return Barra_Residual_Return_pivot
    def get_trading_days_monthend(self):
        """
        获取月末交易日，该时间与SaveDB中的日期一致
        """
        start_date = self.start_date
        end_date = self.end_date
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate from QT_TradingDayNew where SecuMarket = 83 and IfMonthEnd  = 1 and
                                        TradingDate >= DATE('{start_date}') and TradingDate <= DATE('{end_date}') order by  
                                        TradingDate;""".format(start_date=start_date, end_date=end_date)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.index = range(len(trading_day_df))
        return trading_day_df
    def cal_bias_optimized_portfolio(self, cov):
        """
        计算最优化组合的偏差
        该函数未实现
        """
        k = len(cov)
        num_port = 100
        bias_optimized_port = []
        for j in range(num_port):
            alpha_j = np.random.normal(0, 1, k)
            w = (linalg.inv(cov).dot(alpha_j))/(alpha_j.T.dot(linalg.inv(cov)).dot(alpha_j))
            ret = w.T.dot(alpha_j)
            risk = w.T.dot(cov).dot(w)
            factor_standard_return = ret/risk
            bias_optimized_port.append(factor_standard_return)
        return bias_optimized_port
    def cal_specific_risk(self):
        """
        从RiskModel_SpecificRisk中计算异质风险矩阵
        """
        obj = RiskModel_SpecificRisk()
        Specific_Risk = obj.output()
        return Specific_Risk





if __name__ == "__main__":
    start_date = "2011-04-01"
    end_date = "2018-12-31"
    sample = "HS300"
    obj = RiskModel_CommonRisk(start_date, end_date, sample)
    # 当用于实证研究，输出研报内容时
    obj.output_plot_bias()
    # 无需实证研究结果，直接运算出结果用于存储
    # obj.output()