# -*- coding: utf-8 -*-
"""
@author: Huang, Jingyang
@license: Copyright Reserved by Futu
@contact: jgyghuang@qq.com
@site: http://www.futu5.com
@software: PyCharm
@file: RiskModel.py
@time: 2019/05/01
"""


import pandas as pd
import numpy as np
from numpy import linalg
import math
import datetime
from sqlalchemy import create_engine, text
from sklearn import linear_model
import json
import pymysql
import numba
import statsmodels.api as sm
from SaveDB import SaveDB

class RiskModel_SpecificRisk():
    # 当前只支持截面计算
    def __init__(self, start_date, end_date, sample):
        self.start_date = start_date
        self.end_date = end_date
        self.sample = sample
        self.style_sequence_factors = ["Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                       "Midcapitalization", "Residualvolatility", "Earningsyield"]  # 10个风格
        self.sequence_factors = ["Country", "Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                 "Midcapitalization", "Residualvolatility", "Earningsyield",
                                 "ConstructionMaterials", "Machinery", "CommunicationEquipment", "Financials",
                                 "RealEstate", "IndustrialConglomerates", "MedicalHealth", "NonferrousMetals",
                                 "Chemicals", "HouseholdAppliances", "FoodBeverage", "Electronics",
                                 "Transportation", "LightManufacturing", "Utilities", "Textiles", "Agriculture",
                                 "CommercialTrade", "MineralIndustry", "FerrousMetals", "CateringTourism",
                                 "Steel", "Leisure", "ConstructionFurnishings", "ElectricalEquipment",
                                 "Defense", "Automobiles", "Communication", "Media", "NonBankFinancials",
                                 "TransportationEquipment", "CommunicationServices"]  # 1国家+10个风格+32个行业
        self.sequence_factors1 = ["Country", "Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                           "Midcapitalization", "Residualvolatility", "Earningsyield",
                                           "FerrousMetals", "CateringTourism", "ConstructionMaterials",
                                           "Machinery", "TransportationEquipment", "CommunicationEquipment",
                                           "CommunicationServices", "Financials", "RealEstate",
                                           "IndustrialConglomerates",
                                           "MedicalHealth", "NonferrousMetals", "Chemicals", "HouseholdAppliances",
                                           "FoodBeverage", "Electronics", "Transportation", "LightManufacturing",
                                           "Utilities", "Textiles", "Agriculture", "CommercialTrade",
                                           "MineralIndustry"]  # 2014年之前: 1国家+10个风格+23个行业
        self.sequence_factors2 = ["Country", "Size", "Beta", "Momentum", "Btop", "Growth", "Liquidity", "Leverage",
                                           "Midcapitalization", "Residualvolatility", "Earningsyield",
                                           "Steel", "Leisure", "ConstructionMaterials", "ConstructionFurnishings",
                                           "Machinery", "ElectricalEquipment", "Defense", "Automobiles",
                                           "CommunicationEquipment", "Communication", "Media", "Financials",
                                           "NonBankFinancials", "RealEstate", "IndustrialConglomerates",
                                           "MedicalHealth", "NonferrousMetals", "Chemicals", "HouseholdAppliances",
                                           "FoodBeverage", "Electronics", "Transportation", "LightManufacturing",
                                           "Utilities", "Textiles", "Agriculture", "CommercialTrade",
                                           "MineralIndustry"]  # 2014年之后: 1国家+10个风格+28个行业

    def Stock_Specific_Risk_Before_Regime_save(self):
        # 参数设置
        covariance_params, specific_risk_params = self.params()
        # 获取相关数据
        num_days_need_residual_return = specific_risk_params["factor_vol_half_life"] + 1
        trading_day_monthend_df = self.get_trading_days_monthend(None, None) # 月交易日
        # 计算bias
        pct_gamma_ts = []
        bias_group_before_bayesian = pd.DataFrame()
        bias_group_after_bayesian = pd.DataFrame()
        B_ts = []
        CSV_ts = []
        Trading_Day = []
        for i in range(len(trading_day_monthend_df) - 1):
            monthend1 = self.convert_datetime_to_string(trading_day_monthend_df["TradingDate"][i])
            monthend2 = self.convert_datetime_to_string(trading_day_monthend_df["TradingDate"][i + 1])
            days_btw = self.get_trading_days_days(monthend1, monthend2)
            b_before_bayesian_ts = pd.DataFrame()
            b_after_bayesian_ts = pd.DataFrame()
            for j in range(1, len(days_btw)):
                if j == 1:
                    month_start = True
                else:
                    month_start = False
                date_datetime = days_btw["TradingDate"][j]
                date = self.convert_datetime_to_string(date_datetime)
                Residual_Return_plus = self.get_data_residual_return(num_days_need_residual_return, date)
                date_day_ago = Residual_Return_plus.index[-1-1]
                print(date_day_ago)
                Exposure = self.get_data_exposure(self.convert_datetime_to_string(date_day_ago))
                pct_gamma, b_before_bayesian, b_after_bayesian, B_t, CSV_t, Var_bayesian = \
                self.Stock_Specific_Risk_Before_Regime(Residual_Return_plus, Exposure, specific_risk_params, date_day_ago, month_start)
                pct_gamma_ts.append(pct_gamma)
                b_before_bayesian_ts = b_before_bayesian_ts.merge(b_before_bayesian, right_index = True, left_index = True, how = "outer")
                b_after_bayesian_ts = b_after_bayesian_ts.merge(b_after_bayesian,  right_index = True, left_index = True, how = "outer")
                B_ts.append(B_t)
                CSV_ts.append(CSV_t)
                Trading_Day.append(date_day_ago)
                if self.sample == "HS300":
                    table_name = "A_Barra_Specific_Risk_HS300_before_regime"
                elif self.sample == "ZZ500":
                    table_name = "A_Barra_Specific_Risk_ZZ500_before_regime"
                savedb = SaveDB()
                savedb.save_data_to_db(table_name, Var_bayesian)
            bias_month_before_bayesian = b_before_bayesian_ts.drop(["layer"], axis = 1).std(axis = 1).to_frame(name = date_day_ago)
            bias_month_before_bayesian = bias_month_before_bayesian.merge(b_before_bayesian_ts["layer"].to_frame(name = "layer"), right_index = True, left_index = True, how = "outer")
            bias_month_before_bayesian = bias_month_before_bayesian.groupby(["layer"]).mean()
            bias_group_before_bayesian = bias_group_before_bayesian.merge(bias_month_before_bayesian, right_index = True,
                                                                          left_index = True, how = 'outer')
            bias_month_after_bayesian = b_after_bayesian_ts.drop(["layer"], axis = 1).std(axis = 1).to_frame(name=date_day_ago)
            bias_month_after_bayesian = bias_month_after_bayesian.merge(b_after_bayesian_ts["layer"].to_frame(name="layer"), right_index = True, left_index = True, how = "outer")
            bias_month_after_bayesian = bias_month_after_bayesian.groupby(["layer"]).mean()
            bias_group_after_bayesian = bias_group_after_bayesian.merge(bias_month_after_bayesian, right_index=True,
                                                                          left_index=True, how='outer')
        self.plot_bias_bayesian(bias_group_before_bayesian.mean(axis = 1), bias_group_after_bayesian.mean(axis=1))
        pct_gamma_df = pd.DataFrame(pct_gamma_ts, columns = ["pct_gamma"])
        pct_gamma_df = pct_gamma_df.assign(Trading_Day=Trading_Day)
        B_df = pd.DataFrame(B_ts, columns = ["B"])
        B_df.index = Trading_Day
        CSV_df = pd.DataFrame(CSV_ts, columns = ["CSV"])
        CSV_df.index = Trading_Day
        # pct_gamma_df.to_csv(".\\pct_gamma.csv")
        # B_df.to_csv(".\\B.csv")
        # CSV_df.to_csv(".\\CSV.csv")
        lambda_weight_half_life = self.cal_half_life(specific_risk_params["VRA_half_life"].astype(int), specific_risk_params["param_VRA_half_life"].astype(int))
        lambda_ts = B_df.rolling(252).apply(lambda x: np.sqrt((x ** 2 * np.array(lambda_weight_half_life)).sum()))
        csv_ts = CSV_df.rolling(252).apply(lambda x: (x * np.array(lambda_weight_half_life)).sum()) * 100
        lambda_ts.to_csv(".\\lambda_ts.csv")
        self.plot_lambda_csv(lambda_ts, csv_ts)
    def Stock_Specific_Risk_After_Regime_save(self):
        # 参数设置
        covariance_params, specific_risk_params = self.params()
        # 获取相关数据
        num_days_need_residual_return = specific_risk_params["factor_vol_half_life"] + 1
        trading_day_days_df = self.get_trading_days_days(None, None) # 日交易日
        # 计算bias
        B_after_bayesian_ts = []
        B_after_regime_ts = []
        Trading_Day = []
        lambda_ts = pd.read_csv(".\\lambda_ts.csv")
        lambda_ts = lambda_ts.set_index(["Unnamed: 0"])
        lambda_ts.index = pd.to_datetime(lambda_ts.index)
        for i in range(len(trading_day_days_df)):
            date_datetime = trading_day_days_df["TradingDate"][i]
            date = self.convert_datetime_to_string(date_datetime)
            Residual_Return_plus = self.get_data_residual_return(num_days_need_residual_return, date)
            date_day_ago = Residual_Return_plus.index[-1-1]
            print(date_day_ago)
            lambda_date = lambda_ts["B"].loc[date_day_ago]
            Exposure = self.get_data_exposure(self.convert_datetime_to_string(date_day_ago))
            B_after_bayesian_t, B_after_regime_t, Var_regime = \
            self.Stock_Specific_Risk_After_Regime(Residual_Return_plus, Exposure, specific_risk_params, date_day_ago, lambda_date)
            B_after_bayesian_ts.append(B_after_bayesian_t)
            B_after_regime_ts.append(B_after_regime_t)
            Trading_Day.append(date_day_ago)
            if self.sample == "HS300":
                table_name = "A_Barra_Specific_Risk_HS300"
            elif self.sample == "ZZ500":
                table_name = "A_Barra_Specific_Risk_ZZ500"
            savedb = SaveDB()
            savedb.save_data_to_db(table_name, Var_regime)

        B_after_bayesian_df = pd.DataFrame(B_after_bayesian_ts, columns = ["bias_after_bayesian"])
        B_after_bayesian_df.index = Trading_Day
        B_after_bayesian_df = B_after_bayesian_df.rolling(252).mean().dropna()
        B_after_regime_df = pd.DataFrame(B_after_regime_ts, columns=["bias_after_regime"])
        B_after_regime_df.index = Trading_Day
        B_after_regime_df = B_after_regime_df.rolling(252).mean().dropna()
        self.plot_bias_regime(B_after_bayesian_df, B_after_regime_df)



    def Stock_Specific_Risk_Before_Regime(self, Residual_Return_plus, Exposure, params, date_day_ago, month_start):
        """
        :param Residual_Return_plus: 个股残差收益率，长度为 specific_risk_params["factor_vol_half_life"] + specific_risk_params[
            "VRA_half_life"] + 1，最后1天为实现个股月度残差收益率
        :param Exposure: 1天之前的暴露度
        :param params: 计算异质风险相关的参数
        :param date_day_ago: 1天之前的日期，datetime
        """
        # 设置参数
        factor_vol_half_life = params["factor_vol_half_life"].astype(int)
        param_vol_half_life = params["param_vol_half_life"].astype(int)
        newey_west_vol_lags = params["newey_west_vol_lags"].astype(int)
        VRA_half_life = params["VRA_half_life"].astype(int)
        param_VRA_half_life = params["param_VRA_half_life"].astype(int)
        bayesian_shrinkage_q = params["bayesian_shrinkage_q"]

        weight_half_life_vol = pd.Series(self.cal_half_life(factor_vol_half_life, param_vol_half_life))


        # date截面的股票
        stock_list_cs = Residual_Return_plus.columns[Residual_Return_plus.loc[date_day_ago].notnull()].tolist()
        Residual_Return_last_day = Residual_Return_plus[stock_list_cs].iloc[-1:].sum() # 月度个股残差收益率->真实值
        Residual_Return = Residual_Return_plus.iloc[:-1, :]
        Residual_Return_vol = Residual_Return[stock_list_cs].iloc[-factor_vol_half_life:] # 仅保留最近时间截面的股票
        """ 1、进行半衰期指数加权调整和newey-west调整 """
        Var_NW = self.NW_Var(Residual_Return_vol, weight_half_life_vol, newey_west_vol_lags)
        # Var_NW *= 21

        """ 2、进行结构化调整 """
        Q1 = Residual_Return_vol.quantile(0.25)
        Q3 = Residual_Return_vol.quantile(0.75)
        Var_tilt = (1 / 1.35) * (Q3 - Q1)
        Var_equal = []
        for stock_n in Residual_Return_vol.columns:
            residual_return_stock = Residual_Return_vol[stock_n]
            residual_return_stock_drop_outlier = residual_return_stock[(residual_return_stock >= 10*Q1[stock_n]) & (residual_return_stock <= 10*Q3[stock_n])]
            Var_equal.append(residual_return_stock_drop_outlier.var())
        Var_equal = pd.Series(Var_equal, index = Var_tilt.index)
        Z = np.abs((Var_equal - Var_tilt)/Var_tilt)

        Z_exp = np.exp(1-Z)
        index1 = Z_exp.index[Z_exp > 0]
        index1_complement = set(Z_exp.index) - set(index1)
        Z_exp[index1_complement] = 0
        index2 = Z_exp.index[Z_exp < 1]
        index2_complement = set(Z_exp.index) - set(index2)
        Z_exp[index2_complement] = 1
        gamma = Z_exp * min(1, max(0, (factor_vol_half_life - 60) / 120))
        index_gamma = gamma.index[gamma == 1] # gamma=1的股票
        index_gamma_complement = set(gamma.index) - set(index_gamma)
        pct_gamma = len(index_gamma)/len(Z_exp)
        ### 求出全部时间的pct_gamma的占比后，画图

        # gamma==1的股票为样本，求解回归系数
        E0 = 1.05
        Var_str = self.STR_regression(Exposure, Var_NW, index_gamma, E0)
        Var_hat = Var_NW.copy()
        for item in index_gamma_complement:
            Var_hat[item].loc[item] = Var_str[item].loc[item] # 只有gamma=0的股票需要调整为结构化方差

        """ 3、贝叶斯收缩 """
        # 贝叶斯调整之前的标准化个股残差收益率
        b_before_bayesian = Residual_Return_last_day/np.sqrt(np.diag(Var_hat))
        b_before_bayesian = pd.DataFrame(b_before_bayesian, columns=[date_day_ago])
        if month_start: # 意味着月初，需要根据vol分组
            Var_hat_layer = self.LayerFunction(pd.DataFrame(np.diag(Var_hat), columns = ["Var_hat"], index = stock_list_cs), "Var_hat", 10)
            layer_stock_df = Var_hat_layer["layer"].to_frame(name = "layer")
            b_before_bayesian = b_before_bayesian.merge(layer_stock_df, right_index=True, left_index=True, how = "outer")
        ### 求出全部时间的b_before_bayesain，再求出bias，画出不同市值区间下的bias

        mv = Exposure[["MV", "SecuCode"]]
        mv = mv.set_index(["SecuCode"])
        mv = mv.loc[stock_list_cs]
        mv_weight = mv/mv.sum()
        mv_layer = self.LayerFunction(mv, "MV", 10) # 分为10层
        layer = mv_layer["layer"].unique()
        Var_bayesian = pd.Series(index = stock_list_cs)
        for layer_i in layer:
            mv_layer_i = mv_layer["MV"][mv_layer["layer"] == layer_i]
            stock_list_layer_i = mv_layer_i.index
            N_layer_i = len(stock_list_layer_i)
            var_hat_layer_i = np.diag(Var_hat[stock_list_layer_i].loc[stock_list_layer_i])
            mv_weight_layer_i = mv_layer_i/mv_layer_i.sum()
            var_bar_layer_i = (var_hat_layer_i * mv_weight_layer_i).sum()
            delta_layer_i = ((var_hat_layer_i - var_bar_layer_i)**2).sum()/N_layer_i
            v_layer_i = bayesian_shrinkage_q*abs(var_hat_layer_i - var_bar_layer_i)/(delta_layer_i +
                        bayesian_shrinkage_q*abs(var_hat_layer_i - var_bar_layer_i))
            var_bayes_layer_i = v_layer_i*var_bar_layer_i + (1-v_layer_i)*var_hat_layer_i
            Var_bayesian[stock_list_layer_i] = var_bayes_layer_i

        # 贝叶斯调整之后的标准化个股残差收益率
        b_after_bayesian = Residual_Return_last_day / np.sqrt(Var_bayesian)
        b_after_bayesian = pd.DataFrame(b_after_bayesian, columns=[date_day_ago])
        if month_start:  # 意味着月初，需要根据vol分组
            Var_bayesian_layer = self.LayerFunction(pd.DataFrame(Var_bayesian, columns=["Var_bayesian"], index=stock_list_cs),
                                               "Var_bayesian", 10)
            layer_stock_df = Var_bayesian_layer["layer"].to_frame(name="layer")
            b_after_bayesian = b_after_bayesian.merge(layer_stock_df, right_index=True, left_index=True, how="outer")
        ### 求出全部时间的b_after_bayesain，再求出bias，画出不同市值区间下的bias

        """ 4、regime调整 """
        B_t = np.sqrt((b_after_bayesian[date_day_ago]**2*mv_weight["MV"]).sum())
        CSV_t = np.sqrt((Residual_Return_last_day**2*mv_weight["MV"]).sum())
        ### B_t/CSV_t均为一期的值，每日循环后半衰期指数加权算lambda和CSV

        # 调整格式
        Var_bayesian = pd.DataFrame(Var_bayesian, columns = ["Specific_Risk"])
        Var_bayesian = Var_bayesian.assign(SecuCode = stock_list_cs)
        Var_bayesian = Var_bayesian.assign(Trading_Day = date_day_ago)
        Var_bayesian.index = range(len(Var_bayesian))

        return pct_gamma, b_before_bayesian, b_after_bayesian, B_t, CSV_t, Var_bayesian
    def Stock_Specific_Risk_After_Regime(self, Residual_Return_plus, Exposure, params, date_day_ago, lambda_date):
        """
        :param Residual_Return_plus: 个股残差收益率，长度为 specific_risk_params["factor_vol_half_life"] + specific_risk_params[
            "VRA_half_life"] + 1，最后1天为实现个股月度残差收益率
        :param Exposure: 1天之前的暴露度
        :param params: 计算异质风险相关的参数
        :param date_day_ago: 1天之前的日期，datetime
        """
        # 设置参数
        factor_vol_half_life = params["factor_vol_half_life"].astype(int)
        param_vol_half_life = params["param_vol_half_life"].astype(int)
        newey_west_vol_lags = params["newey_west_vol_lags"].astype(int)
        VRA_half_life = params["VRA_half_life"].astype(int)
        param_VRA_half_life = params["param_VRA_half_life"].astype(int)
        bayesian_shrinkage_q = params["bayesian_shrinkage_q"]
        weight_half_life_vol = pd.Series(self.cal_half_life(factor_vol_half_life, param_vol_half_life))


        # date截面的股票
        stock_list_cs = Residual_Return_plus.columns[Residual_Return_plus.loc[date_day_ago].notnull()].tolist()
        Residual_Return_last_day = Residual_Return_plus[stock_list_cs].iloc[-1:].sum() # 月度个股残差收益率->真实值
        Residual_Return = Residual_Return_plus.iloc[:-1, :]
        Residual_Return_vol = Residual_Return[stock_list_cs].iloc[-factor_vol_half_life:] # 仅保留最近时间截面的股票
        """ 1、进行半衰期指数加权调整和newey-west调整 """
        Var_NW = self.NW_Var(Residual_Return_vol, weight_half_life_vol, newey_west_vol_lags)
        # Var_NW *= 21

        """ 2、进行结构化调整 """
        Q1 = Residual_Return_vol.quantile(0.25)
        Q3 = Residual_Return_vol.quantile(0.75)
        Var_tilt = (1 / 1.35) * (Q3 - Q1)
        Var_equal = []
        for stock_n in Residual_Return_vol.columns:
            residual_return_stock = Residual_Return_vol[stock_n]
            residual_return_stock_drop_outlier = residual_return_stock[(residual_return_stock >= 10*Q1[stock_n]) & (residual_return_stock <= 10*Q3[stock_n])]
            Var_equal.append(residual_return_stock_drop_outlier.var())
        Var_equal = pd.Series(Var_equal, index = Var_tilt.index)
        Z = np.abs((Var_equal - Var_tilt)/Var_tilt)

        Z_exp = np.exp(1-Z)
        index1 = Z_exp.index[Z_exp > 0]
        index1_complement = set(Z_exp.index) - set(index1)
        Z_exp[index1_complement] = 0
        index2 = Z_exp.index[Z_exp < 1]
        index2_complement = set(Z_exp.index) - set(index2)
        Z_exp[index2_complement] = 1
        gamma = Z_exp * min(1, max(0, (factor_vol_half_life - 60) / 120))
        index_gamma = gamma.index[gamma == 1] # gamma=1的股票
        index_gamma_complement = set(gamma.index) - set(index_gamma)
        ### 求出全部时间的pct_gamma的占比后，画图

        # gamma==1的股票为样本，求解回归系数
        E0 = 1.05
        Var_str = self.STR_regression(Exposure, Var_NW, index_gamma, E0)
        Var_hat = Var_NW.copy()
        for item in index_gamma_complement:
            Var_hat[item].loc[item] = Var_str[item].loc[item] # 只有gamma=0的股票需要调整为结构化方差

        """ 3、贝叶斯收缩 """
        mv = Exposure[["MV", "SecuCode"]]
        mv = mv.set_index(["SecuCode"])
        mv = mv.loc[stock_list_cs]
        mv_weight = mv/mv.sum()
        mv_layer = self.LayerFunction(mv, "MV", 10) # 分为10层
        layer = mv_layer["layer"].unique()
        Var_bayesian = pd.Series(index = stock_list_cs)
        for layer_i in layer:
            mv_layer_i = mv_layer["MV"][mv_layer["layer"] == layer_i]
            stock_list_layer_i = mv_layer_i.index
            N_layer_i = len(stock_list_layer_i)
            var_hat_layer_i = np.diag(Var_hat[stock_list_layer_i].loc[stock_list_layer_i])
            mv_weight_layer_i = mv_layer_i/mv_layer_i.sum()
            var_bar_layer_i = (var_hat_layer_i * mv_weight_layer_i).sum()
            delta_layer_i = ((var_hat_layer_i - var_bar_layer_i)**2).sum()/N_layer_i
            v_layer_i = bayesian_shrinkage_q*abs(var_hat_layer_i - var_bar_layer_i)/(delta_layer_i +
                        bayesian_shrinkage_q*abs(var_hat_layer_i - var_bar_layer_i))
            var_bayes_layer_i = v_layer_i*var_bar_layer_i + (1-v_layer_i)*var_hat_layer_i
            Var_bayesian[stock_list_layer_i] = var_bayes_layer_i


        """ 4、regime调整 """
        Var_regime = Var_bayesian * lambda_date
        b_after_bayesian = Residual_Return_last_day / np.sqrt(Var_bayesian)
        b_after_regime = Residual_Return_last_day / np.sqrt(Var_regime)
        B_after_bayesian_t = np.sqrt((b_after_bayesian**2*mv_weight["MV"]).sum())
        B_after_regime_t = np.sqrt((b_after_regime**2*mv_weight["MV"]).sum())

        # 调整格式
        Var_regime = pd.DataFrame(Var_regime, columns=["Specific_Risk"])
        Var_regime = Var_regime.assign(SecuCode=stock_list_cs)
        Var_regime = Var_regime.assign(Trading_Day=date_day_ago)
        Var_regime.index = range(len(Var_regime))
        return B_after_bayesian_t, B_after_regime_t, Var_regime






    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str
    def cal_half_life(self, num_days, param_half_life):
        """
        半衰期权重
        """
        delta = 0.5 ** (1 / param_half_life)
        weight_list = [delta ** (num_days - k) for k in range(1, num_days + 1)]
        weight_list2 = list(weight_list / np.sum(weight_list))
        return weight_list2
    # @numba.jit
    def demean(self, df, half_life_weight):
        """
        去均值
        """
        df_mean = (df.multiply(half_life_weight,0)).sum(axis = 0)
        df_demean = df - df_mean
        df_demean.reset_index(drop=True, inplace = True)
        return df_demean
    #@numba.jit
    def gamma_func_half_life(self, df, m, weight_half_life):
        # 当m==0时，即为没有滞后阶数的协方差
        T = len(df)
        tmp = np.zeros([len(df.columns)])
        for t in range(0, T - m):
            tmp_add = df.ix[t, :].fillna(0) * df.ix[t + m, :].fillna(0) * weight_half_life[t] # 残差收益为nan的填充为0
            tmp += tmp_add
        # tmp /= (T - 1) # 等权重
        tmp = np.diag(tmp)
        return pd.DataFrame(tmp, index=df.columns, columns=df.columns)
    #@numba.jit
    def NW_Var(self, df, weight_half_life_cov, newey_west_cov_lags):
        """
        目前使用的计算方法，直接计算协方差而非拆分成波动率和相关系数矩阵
        """
        df = self.demean(df, weight_half_life_cov)
        g0 = self.gamma_func_half_life(df, 0, weight_half_life_cov)
        for m in range(0, newey_west_cov_lags):
            m += 1
            gi = self.gamma_func_half_life(df, m, weight_half_life_cov)
            w = 1 - m / (1 + newey_west_cov_lags)
            g0 += w * (gi + gi.T)
        return g0
    def STR_regression(self, df_exposure, Var, index_gamma, E0):
        """
        结构化调整中回归
        """
        # 用gamma=1的股票回归求出系数params_b
        df_exposure = df_exposure.set_index(["SecuCode"])
        mv_total = df_exposure["MV"].sum()
        df_industry = pd.get_dummies(df_exposure["Industry"])
        industry_sequence_factors = df_industry.columns.tolist()
        weight_industry_list = []
        for industry in industry_sequence_factors:
            secucode_industry = df_industry.index[df_industry[industry] == 1]
            mv_industry = df_exposure["MV"].loc[secucode_industry].sum()
            weight_industry = mv_industry / mv_total
            weight_industry_list.append(weight_industry)
        c_list = weight_industry_list / weight_industry_list[0] * (-1)
        column_name_0 = industry_sequence_factors[0]
        df_industry_new = pd.DataFrame(index=df_industry.index, columns=industry_sequence_factors[1:])
        for i in range(1, len(industry_sequence_factors)):
            column_name = industry_sequence_factors[i]
            c = c_list[i]
            df_industry_new[column_name] = df_industry[column_name] + c * df_industry[column_name_0]

        df_country_style_industry = pd.merge(df_exposure[["Country"] + self.style_sequence_factors], df_industry_new,
                                             left_index=True, right_index=True, how='left')
        sequence_factors_dropone = df_country_style_industry.columns.tolist()

        X = df_country_style_industry.loc[index_gamma].values.astype(np.float32)
        W = 1 / df_exposure["MV"].loc[index_gamma]
        Y = np.log(np.diagonal(Var[index_gamma].loc[index_gamma]))
        results = sm.WLS(Y, X, W)
        fit = results.fit()
        params_b = pd.DataFrame(fit.params, index=sequence_factors_dropone).T
        #industry_beta_0 = (c_list[1:] * params_b[industry_sequence_factors[1:]].values).sum()
        #params_b.insert(loc=0, column=column_name_0, value=industry_beta_0)

        # 求出全部股票的结构化方差
        Var_str = np.exp((df_country_style_industry.values.astype(np.float)*params_b.values.astype(np.float)).sum(axis=1))*E0
        Var_str = pd.DataFrame(np.diag(Var_str), index = Var.columns, columns = Var.columns)
        return Var_str
    def LayerFunction(self, df, col_name,  L):
        df['layer'] = np.nan
        for l in range(1, L):
            n = round(1 - (l - 1) / L, 1)
            m = round(1 - l / L, 1)
            df['layer'][(df[col_name] > df[col_name].quantile(m, interpolation="midpoint"))
                                      & (df[col_name] <= df[col_name].quantile(n, interpolation="midpoint"))] = l
        df['layer'][df[col_name] <= df[col_name].quantile(1 / L, interpolation="midpoint")] = L
        return df
    def get_data_exposure(self, trading_day):
        end_date = trading_day
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="db_etf", charset="utf8mb4")
        engine = create_engine(engine_str)
        if self.sample == "HS300":
            table_name_exposure = "A_Barra_Exposure_HS300"
        elif self.sample == "ZZ500":
            table_name_exposure = "A_Barra_Exposure_ZZ500"
        # end_date时间截面个股暴露度
        sql_string_factor_exposure = "select * from " + table_name_exposure + " where Trading_Day = " \
                                     "Date('{end_date}') order by SecuCode;".format(
                                     end_date=end_date)
        sql_stmt_factor_exposure = text(sql_string_factor_exposure)
        Barra_Exposure = pd.read_sql(sql_stmt_factor_exposure, engine)
        Barra_Exposure.drop(["Create_Time", "Trading_Day"], axis=1, inplace=True)
        return Barra_Exposure
    def get_data_residual_return(self, num_days_need_residual_return, trading_day):
        num_day_need_max = int(num_days_need_residual_return)
        end_date = trading_day
        if num_day_need_max < 250:
            trading_day_df = self.get_trading_day(start=None, end=trading_day)
        else:
            temp_datetime = self.convert_string_to_datetime(trading_day) - pd.DateOffset(days=2 * num_day_need_max)
            temp_date = self.convert_datetime_to_string(temp_datetime)
            trading_day_df = self.get_trading_day(start=temp_date, end=trading_day)
        start_date_datetime_residual_return = trading_day_df[-num_day_need_max]
        start_date_residual_return = self.convert_datetime_to_string(start_date_datetime_residual_return)
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="db_etf", charset="utf8mb4")
        engine = create_engine(engine_str)
        if self.sample == "HS300":
            table_name_residual_return = "A_Barra_Residual_Return_HS300"
        elif self.sample == "ZZ500":
            table_name_residual_return = "A_Barra_Residual_Return_ZZ500"

        # 个股残差收益率时间序列
        sql_string_residual_return = "select * from " + table_name_residual_return + " where Trading_Day >= " \
                                     "Date('{start_date_residual_return}') and Trading_Day <= Date('{end_date}') order by " \
                                     "Trading_Day, SecuCode;".format(
                                     end_date=end_date, start_date_residual_return=start_date_residual_return)
        sql_stmt_residual_return = text(sql_string_residual_return)
        Barra_Residual_Return = pd.read_sql(sql_stmt_residual_return, engine)
        Barra_Residual_Return.drop(["Create_Time"], axis=1, inplace=True)
        Barra_Residual_Return_pivot = Barra_Residual_Return.pivot(index="Trading_Day", columns="SecuCode",
                                                                  values="Residual_return")
        return Barra_Residual_Return_pivot
    def params(self):
        covariance_params = pd.Series([252, 90, 5, 252, 90, 2, 42, 42],
                                      index=["factor_vol_half_life", "param_vol_half_life", "newey_west_vol_lags",
                                             "factor_corr_half_life", "param_corr_half_life", "newey_west_corr_lags",
                                             "VRA_half_life", "param_VRA_half_life"])
        specific_risk_params = pd.Series([252, 90, 5, 0.1, 252, 42],
                                         index=["factor_vol_half_life", "param_vol_half_life", "newey_west_vol_lags",
                                                "bayesian_shrinkage_q", "VRA_half_life", "param_VRA_half_life"])
        return covariance_params, specific_risk_params
    def get_trading_days_monthend(self, start_date, end_date):
        """
        获取月末交易日，该时间与SaveDB中的日期一致
        """
        if start_date == None:
            start_date = self.start_date
        if end_date == None:
            end_date = self.end_date
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate from QT_TradingDayNew where SecuMarket = 83 and IfMonthEnd  = 1 and
                                        TradingDate >= DATE('{start_date}') and TradingDate <= DATE('{end_date}') order by  
                                        TradingDate;""".format(start_date=start_date, end_date=end_date)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.index = range(len(trading_day_df))
        return trading_day_df
    def get_trading_day(self, start, end):
        """
        获取交易日
        :param start: 开始时间string
        :param end: 结束时间string
        :return: 交易日列表，日期为pd.Timestamp
        """
        if start == None:
            start_datetime = self.convert_string_to_datetime(end) - pd.DateOffset(years=1)
            start = self.convert_datetime_to_string(start_datetime)
        if end == None:
            end_datetime = self.convert_string_to_datetime(start) + pd.DateOffset(years=1)
            end = self.convert_datetime_to_string(end_datetime)
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate  from QT_TradingDayNew where SecuMarket = 83 and IfTradingDay  = 1 and
                            TradingDate >= DATE('{start}') and TradingDate <= DATE('{end}') order by TradingDate;""".format(
            start=start, end=end)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.rename(columns = {"TradingDate": "TradingDay"}, inplace = True)
        trading_day_list = sorted(trading_day_df["TradingDay"].tolist())
        return trading_day_list
    def get_trading_days_days(self, start_date, end_date):
        """
        获取日度交易日，该时间与SaveDB中的日期一致
        """
        if start_date == None:
            start_date = self.start_date
        if end_date == None:
            end_date = self.end_date
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_string = """select TradingDate  from QT_TradingDayNew where SecuMarket = 83 and IfTradingDay  = 1 and
                                        TradingDate >= DATE('{start_date}') and TradingDate <= DATE('{end_date}') order by 
                                        TradingDate;""".format(start_date=start_date, end_date=end_date)
        sql_stmt = text(sql_string)
        engine = create_engine(engine_str)
        trading_day_df = pd.read_sql(sql_stmt, engine)
        trading_day_df.index = range(len(trading_day_df))
        return trading_day_df
    def plot_bias_bayesian(self, bias_before, bias_after):
        import matplotlib.pyplot as plt
        bias_before = bias_before.sort_index(ascending = False)
        bias_after = bias_after.sort_index(ascending=False)
        plt.figure('fig', figsize = (10, 4))
        plt.scatter(range(1, 11), bias_before.values, color="gray")
        plt.scatter(range(1, 11), bias_after.values, color="darkblue")
        plt.plot(range(1, 11), bias_before.values, label="Bias Without Shrinkage", color="gray", linewidth=3, linestyle="--")
        plt.plot(range(1, 11), bias_after.values, label="Bias With Shrinkage", color="darkblue", linewidth=3)
        plt.axhline(y = 1, color='black', linewidth=1, linestyle="--")
        plt.ylim(0.8, 1.6)
        plt.xlabel("Specific Volatility Decile")
        plt.ylabel("Bias Statistics")
        plt.title("Bias Statistics")
        plt.legend(loc=1)
        plt.xticks(np.arange(1, 11))
        plt.show()
    def plot_lambda_csv(self, lambda_ts, csv_ts):
        import matplotlib.pyplot as plt
        plt.figure('fig', figsize=(10, 4))
        plt.plot(lambda_ts.index[252:], lambda_ts[252:], label="Factor Volatility Multiplier", color="darkblue", linewidth=3)
        plt.plot(csv_ts.index[252:], csv_ts[252:],  label="Factor CSV", color="gray", linewidth=3, linestyle="--")
        plt.axhline(y=1, color='black', linewidth=1, linestyle="--")
        plt.ylabel("Factor Volatility Multiplier & Factor CSV(%)")
        plt.xlabel("Time")
        plt.legend(loc=1)
        plt.title("Factor Volatility Multiplier & Factor CSV(%)")
        plt.show()
    def plot_bias_regime(self, bias_before_regime, bias_after_regime):
        import matplotlib.pyplot as plt
        plt.figure('fig', figsize=(10, 4))
        plt.plot(bias_after_regime.index, bias_after_regime.values, label="Bias with Volatility Regime Adjustment", color="darkblue",
                 linewidth=3)
        plt.plot(bias_before_regime.index, bias_before_regime.values, label="Bias without Volatility Regime Adjustment", color="gray",
                 linewidth=3, linestyle="--")
        plt.axhline(y=1, color='black', linewidth=1, linestyle="--")
        plt.ylabel("Mean Bias Statistics")
        plt.xlabel("Time")
        plt.legend(loc=3)
        plt.title("Mean Bias Statistics")
        plt.show()

if __name__ == "__main__":
    start_date1 = "2011-04-01"
    end_date = "2018-12-31"
    sample = "HS300"
    # obj1 = RiskModel_SpecificRisk(start_date1, end_date, sample)
    # save1 = obj1.Stock_Specific_Risk_Before_Regime_save()

    start_date2 = "2012-05-15" # 从2012/05/14起lambda有数
    obj2 = RiskModel_SpecificRisk(start_date2, end_date, sample)
    save2 = obj2.Stock_Specific_Risk_After_Regime_save()
